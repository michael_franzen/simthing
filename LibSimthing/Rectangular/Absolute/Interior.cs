﻿namespace LibSimthing
{
    public class Interior : AbsoluteObjectXYZ
    {
        private InteriorCategory category;
        private string url;

        public InteriorCategory Category
        {
            get
            {
                return category;
            }

            set
            {
                category = value;
            }
        }

        public string Url
        {
            get
            {
                return url;
            }

            set
            {
                url = value;
            }
        }

        public enum InteriorCategory
        {
            _blank,
            accessories,
            hallway,
            bathroom,
            bedroom,
            decoration,
            dining,
            kids,
            kitchen,
            lamps,
            living,
            office,
            outdoor,
            relaxing,
            seating,
            sleeping,
            storage,
            tables,
            closet
        }
    }
}
