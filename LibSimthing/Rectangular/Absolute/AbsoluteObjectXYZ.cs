﻿using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System;
using System.Collections.Generic;

namespace LibSimthing
{
    public abstract class AbsoluteObjectXYZ : IPhysicalObject
    {
        private Point3d position = new Point3d();
        private Vector3d dimensions = new Vector3d();
        private double angle = 0;

        public List<Line> Lines2D()
        {
            List<Line> lines = new List<Line>();

            Point3d[] point_array = new Point3d[]
            {
            this.Position,
            this.Position + new Vector3d(this.Dimensions[0],0,0),
            this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1],0),
            this.Position + new Vector3d(0,this.Dimensions[1],0),
            this.Position
            };

            List<Point3d> points = new List<Point3d>(point_array);
            if (this.Dimensions[0] < 0 || this.Dimensions[1] < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            points = new List<Point3d>(rotation.TransformList(points));

            lines.Add(new Line(points[0], points[1]));
            lines.Add(new Line(points[1], points[2]));
            lines.Add(new Line(points[2], points[3]));
            lines.Add(new Line(points[3], points[4]));
            return lines;
        }

        public List<Line> Lines2DLong()
        {
            List<Line> lines = new List<Line>();

            Point3d[] point_array = new Point3d[]
            {
            this.Position,
            this.Position + new Vector3d(this.Dimensions[0],0,0),
            this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1],0),
            this.Position + new Vector3d(0,this.Dimensions[1],0),
            this.Position
            };

            List<Point3d> points = new List<Point3d>(point_array);
            if (this.Dimensions[0] < 0 || this.Dimensions[1] < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            points = new List<Point3d>(rotation.TransformList(points));

            lines.Add(new Line(points[0], points[1]));
            lines.Add(new Line(points[2], points[3]));
            return lines;
        }

        public Point3d[] Coordinates2D()
        {
            Point3d[] point_array = new Point3d[]
            {
            this.Position,
            this.Position + new Vector3d(this.Dimensions[0],0,0),
            this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1],0),
            this.Position + new Vector3d(0,this.Dimensions[1],0),
            this.Position
            };

            List<Point3d> points = new List<Point3d>(point_array);
            if (this.Dimensions[0] < 0 || this.Dimensions[1] < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            return rotation.TransformList(points);
        }

        public Point3d[] MinMax2D()
        {
            Point3d[] points = new Point3d[]
            {
                this.Position + new Vector3d(0,0,0),
                this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1],0),
            };

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            return rotation.TransformList(points);
        }

        public PolylineCurve Footprint()
        {
            Point3d[] point_array = new Point3d[]
            {
            this.Position,
            this.Position + new Vector3d(this.Dimensions[0],0,0),
            this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1],0),
            this.Position + new Vector3d(0,this.Dimensions[1],0),
            this.Position
            };

            List<Point3d> points = new List<Point3d>(point_array);
            if (this.Dimensions[0] < 0 || this.Dimensions[1] < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            points = new List<Point3d>(rotation.TransformList(points));
            return new Rhino.Geometry.PolylineCurve(points);
        }

        public Polyline Polyline()
        {
            Point3d[] point_array = new Point3d[]
            {
            this.Position,
            this.Position + new Vector3d(this.Dimensions[0],0,0),
            this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1],0),
            this.Position + new Vector3d(0,this.Dimensions[1],0),
            this.Position
            };

            List<Point3d> points = new List<Point3d>(point_array);
            if (this.Dimensions[0] < 0 || this.Dimensions[1] < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            points = new List<Point3d>(rotation.TransformList(points));
            return new Rhino.Geometry.Polyline(points);
        }

        public Line Axis()
        {
            Point3d[] points = new Point3d[]
            {
                this.Position + new Vector3d(0,this.Dimensions[1]/2,0),
                this.Position + new Vector3d(this.Dimensions[0],this.Dimensions[1]/2,0)
            };

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * this.Angle, new Vector3d(0, 0, 1), this.Position);
            points = rotation.TransformList(points);

            return new Line(points[0], points[1]);
        }

        public Line ExtendedAxis(HashSet<Wall> walls)
        {
            Line extendedAxis = this.Axis();
            foreach (Wall w in walls)
            {
                Line axis = new Line(extendedAxis.From, extendedAxis.To);
                axis.Extend(w.Width, w.Width);

                double a, b;
                if (Intersection.LineLine(axis, w.Axis(), out a, out b, 0.001, true))
                {
                    double d1, d2;
                    d1 = extendedAxis.From.DistanceTo(axis.PointAt(a));
                    d2 = extendedAxis.To.DistanceTo(axis.PointAt(a));
                    if (d1 < d2) extendedAxis.Extend(d1, 0);
                    else extendedAxis.Extend(0, d2);
                }
            }

            return extendedAxis;
        }

        public Surface Surface()
        {
            return Rhino.Geometry.Extrusion.Create(this.Footprint(), this.Dimensions[2], true);
        }

        public Mesh[] Mesh(double maxEdge = 0.1)
        {
            MeshingParameters parameters = new MeshingParameters();
            parameters.MaximumEdgeLength = maxEdge;
            return Rhino.Geometry.Mesh.CreateFromBrep(this.Surface().ToBrep(), parameters);
        }

        #region properties
        public double X
        {
            get
            {
                return this.position[0];
            }
            set
            {
                this.position[0] = value;
            }
        }

        public double Y
        {
            get
            {
                return this.position[1];
            }
            set
            {
                this.position[1] = value;
            }
        }

        public double Z
        {
            get
            {
                return this.position[2];
            }
            set
            {
                this.position[2] = value;
            }
        }

        public double Length
        {
            get
            {
                return this.dimensions[0];
            }
            set
            {
                this.dimensions[0] = value;
            }
        }

        public double Width
        {
            get
            {
                return this.dimensions[1];
            }
            set
            {
                this.dimensions[1] = value;
            }
        }

        public double Height
        {
            get
            {
                return this.dimensions[2];
            }
            set
            {
                this.dimensions[2] = value;
            }
        }

        public double Angle
        {
            get
            {
                return angle;
            }

            set
            {
                angle = value;
            }
        }

        public Point3d Position
        {
            get
            {
                return position;
            }

            private set
            {
                position = value;
            }
        }

        public Vector3d Dimensions
        {
            get
            {
                return dimensions;
            }

            private set
            {
                dimensions = value;
            }
        }

        #endregion properties
    }
}
