﻿using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System;
using System.Collections.Generic;

namespace LibSimthing
{
    public abstract class RelativeObjectXZ : IPhysicalObject
    {
        private AbsoluteObjectXYZ origin;
        private Point2d offset = new Point2d();
        private Vector2d dimensions = new Vector2d();

        public List<Line> Lines2D()
        {
            List<Line> lines = new List<Line>();
            Vector3d position3d = new Vector3d(this.origin.Position.X + this.offset.X, this.origin.Position.Y, this.origin.Position.Z + this.offset.Y);
            Vector3d dimensions3d = new Vector3d(this.Dimensions.X, this.origin.Dimensions.Y, 0);

            Point3d[] points_array = new Point3d[]
            {
                new Point3d(0,0,0),
                new Point3d(dimensions3d[0],0,0),
                new Point3d(dimensions3d[0], dimensions3d[1],0),
                new Point3d(0,dimensions3d[1],0),
                new Point3d(0,0,0)
            };

            List<Point3d> points = new List<Point3d>(points_array);
            if (dimensions3d.X < 0 || dimensions3d.Y < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * origin.Angle, new Vector3d(0, 0, 1), this.origin.Position);
            Transform translation = Transform.Translation(position3d);
            points = new List<Point3d>(rotation.TransformList(translation.TransformList(points)));

            lines.Add(new Line(points[0], points[1]));
            lines.Add(new Line(points[1], points[2]));
            lines.Add(new Line(points[2], points[3]));
            lines.Add(new Line(points[3], points[4]));
            lines.Add(new Line(points[4], points[0]));
            return lines;
        }

        public List<Line> Lines2DOnGround()
        {
            List<Line> lines = new List<Line>();
            foreach (Line l in this.Lines2D())
            {
                Point3d p1 = l.From;
                Point3d p2 = l.To;
                p1.Z = 0;
                p2.Z = 0;
                lines.Add(new Line(p1, p2));
            }
            return lines;
        }

        public PolylineCurve Footprint()
        {
            Vector3d position3d = new Vector3d(this.origin.Position.X + this.offset.X, this.origin.Position.Y, this.origin.Position.Z + this.offset.Y);
            Vector3d dimensions3d = new Vector3d(this.Dimensions.X, this.origin.Dimensions.Y, 0);

            Point3d[] points_array = new Point3d[]
            {
                new Point3d(0,0,0),
                new Point3d(dimensions3d[0],0,0),
                new Point3d(dimensions3d[0], dimensions3d[1],0),
                new Point3d(0,dimensions3d[1],0),
                new Point3d(0,0,0)
            };

            List<Point3d> points = new List<Point3d>(points_array);
            if (dimensions3d.X < 0 || dimensions3d.Y < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * origin.Angle, new Vector3d(0, 0, 1), this.origin.Position);
            Transform translation = Transform.Translation(position3d);
            points = new List<Point3d>(rotation.TransformList(translation.TransformList(points)));
            return new Rhino.Geometry.PolylineCurve(points);
        }

        public PolylineCurve FootprintOnGround()
        {
            Vector3d position3d = new Vector3d(this.origin.Position.X + this.offset.X, this.origin.Position.Y, 0);
            Vector3d dimensions3d = new Vector3d(this.Dimensions.X, this.origin.Dimensions.Y, 0);

            Point3d[] points_array = new Point3d[]
            {
                new Point3d(0,0,0),
                new Point3d(dimensions3d[0],0,0),
                new Point3d(dimensions3d[0], dimensions3d[1],0),
                new Point3d(0,dimensions3d[1],0),
                new Point3d(0,0,0)
            };

            List<Point3d> points = new List<Point3d>(points_array);
            if (dimensions3d.X < 0 || dimensions3d.Y < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * origin.Angle, new Vector3d(0, 0, 1), this.origin.Position);
            Transform translation = Transform.Translation(position3d);
            points = new List<Point3d>(rotation.TransformList(translation.TransformList(points)));
            return new Rhino.Geometry.PolylineCurve(points);
        }

        public Polyline Polyline()
        {
            Vector3d position3d = new Vector3d(this.origin.Position.X + this.offset.X, this.origin.Position.Y, this.origin.Position.Z + this.offset.Y);
            Vector3d dimensions3d = new Vector3d(this.Dimensions.X, this.origin.Dimensions.Y, 0);

            Point3d[] points_array = new Point3d[]
            {
                new Point3d(0,0,0),
                new Point3d(dimensions3d[0],0,0),
                new Point3d(dimensions3d[0], dimensions3d[1],0),
                new Point3d(0,dimensions3d[1],0),
                new Point3d(0,0,0)
            };

            List<Point3d> points = new List<Point3d>(points_array);
            if (dimensions3d.X < 0 || dimensions3d.Y < 0) points.Reverse();

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * origin.Angle, new Vector3d(0, 0, 1), this.origin.Position);
            Transform translation = Transform.Translation(position3d);
            points = new List<Point3d>(rotation.TransformList(translation.TransformList(points)));
            return new Rhino.Geometry.Polyline(points);
        }

        public Line Axis()
        {
            Vector3d position3d = new Vector3d(this.origin.Position.X + this.offset.X, this.origin.Position.Y, this.origin.Position.Z + this.offset.Y);
            Vector3d dimensions3d = new Vector3d(this.Dimensions.X, this.origin.Dimensions.Y, 0);

            //if (origin.Angle % 360 >= 180) dimensions3d[1] = -dimensions3d[1];

            Point3d[] points = new Point3d[]
            {
                new Point3d(0,0,0) + new Vector3d(0, dimensions3d[1]/2,0),
                new Point3d(0,0,0) + new Vector3d(dimensions3d[0], dimensions3d[1]/2,0)
            };

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * origin.Angle, new Vector3d(0, 0, 1), this.origin.Position);
            Transform translation = Transform.Translation(position3d);
            points = rotation.TransformList(translation.TransformList(points));
            
            return new Line(points[0], points[1]);
        }

        public Line AxisOnGround()
        {
            Vector3d position3d = new Vector3d(this.origin.Position.X + this.offset.X, this.origin.Position.Y, 0);
            Vector3d dimensions3d = new Vector3d(this.Dimensions.X, this.origin.Dimensions.Y, 0);

            //if (origin.Angle % 360 >= 180) dimensions3d[1] = -dimensions3d[1];

            Point3d[] points = new Point3d[]
            {
                new Point3d(0,0,0) + new Vector3d(0, dimensions3d[1]/2,0),
                new Point3d(0,0,0) + new Vector3d(dimensions3d[0], dimensions3d[1]/2,0)
            };

            Transform rotation = Transform.Rotation((-1) * (Math.PI / 180.0) * origin.Angle, new Vector3d(0, 0, 1), this.origin.Position);
            Transform translation = Transform.Translation(position3d);
            points = rotation.TransformList(translation.TransformList(points));

            return new Line(points[0], points[1]);
        }

        public Line ExtendedAxis(HashSet<Wall> walls)
        {
            Line extendedAxis = this.Axis();
            foreach (Wall w in walls)
            {
                Line axis = new Line(extendedAxis.From, extendedAxis.To);
                axis.Extend(w.Width, w.Width);

                double a, b;
                if (Intersection.LineLine(axis, w.Axis(), out a, out b, 0.001, true))
                {
                    double d1, d2;
                    d1 = extendedAxis.From.DistanceTo(axis.PointAt(a));
                    d2 = extendedAxis.To.DistanceTo(axis.PointAt(a));
                    if (d1 < d2) extendedAxis.Extend(d1, 0);
                    else extendedAxis.Extend(0, d2);
                }
            }

            return extendedAxis;
        }

        public Surface Surface()
        {
            return Rhino.Geometry.Extrusion.Create(this.Footprint(), this.Dimensions.Y, true);
        }

        public Mesh[] Mesh(double maxEdge = 0.1)
        {
            MeshingParameters parameters = new MeshingParameters();
            parameters.MaximumEdgeLength = maxEdge;
            return Rhino.Geometry.Mesh.CreateFromBrep(this.Surface().ToBrep(), parameters);
        }

        #region properties

        public double X
        {
            get
            {
                return this.offset.X;
            }
            set
            {
                this.offset.X = value;
            }
        }

        public double Y
        {
            get
            {
                return this.offset.Y;
            }
            set
            {
                this.offset.Y = value;
            }
        }

        public double Length
        {
            get
            {
                return this.dimensions.X;
            }
            set
            {
                this.dimensions.X = value;
            }
        }

        public double Height
        {
            get
            {
                return this.dimensions.Y;
            }
            set
            {
                this.dimensions.Y = value;
            }
        }

        public AbsoluteObjectXYZ Origin
        {
            get
            {
                return origin;
            }

            set
            {
                origin = value;
            }
        }

        public Vector2d Dimensions
        {
            get
            {
                return dimensions;
            }

            set
            {
                dimensions = value;
            }
        }

        #endregion properties
    }
}
