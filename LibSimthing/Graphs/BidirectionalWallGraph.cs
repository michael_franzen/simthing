﻿using QuickGraph;
using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System.Collections.Generic;

namespace LibSimthing.Graphs
{
    public class BidirectionalWallGraph : AdjacencyGraph<Wall, BidirectionalWallGraph.WallEdge>
    {
        public class WallEdge : IEdge<Wall>
        {
            private Wall source;
            private Wall target;

            public Wall Source
            {
                get
                {
                    return source;
                }

                set
                {
                    source = value;
                }
            }

            public Wall Target
            {
                get
                {
                    return target;
                }

                set
                {
                    target = value;
                }
            }
        }

        public static BidirectionalWallGraph CreateFromApartment(Apartment apartment)
        {
            BidirectionalWallGraph graph = new BidirectionalWallGraph();
            List<Wall> walls = new List<Wall>(apartment.Walls);

            // compute wall-intersection graph
            for (int i = 0; i < walls.Count - 1; i++)
            {
                graph.AddVertex(walls[i]);
                for (int j = i + 1; j < walls.Count; j++)
                {
                    graph.AddVertex(walls[j]);

                    Curve curveA = walls[i].Footprint();
                    Curve curveB = walls[j].Footprint();
                    CurveIntersections curveIntersections = Intersection.CurveCurve(curveA, curveB, 0.001, 0.001);

                    if (curveIntersections.Count > 0)
                    {
                        graph.AddEdge(new BidirectionalWallGraph.WallEdge() { Source = walls[i], Target = walls[j] });
                        graph.AddEdge(new BidirectionalWallGraph.WallEdge() { Source = walls[j], Target = walls[i] });
                    }
                }
            }

            return graph;
        }
    }
}
