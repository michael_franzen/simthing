﻿using Rhino.Geometry;
using System;
using System.Collections.Generic;

namespace LibSimthing.Util
{
    public class Geometry
    {
        public static double ccw(Point3d p1, Point3d p2, Point3d p3)
        {
            return (p2[0] - p1[0]) * (p3[1] - p1[1]) - (p2[1] - p1[1]) * (p3[0] - p1[0]);
        }
    }
}
