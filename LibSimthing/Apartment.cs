﻿using CPlan.Geometry;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.Noding.Snapround;
using NetTopologySuite.Operation.Polygonize;
using NetTopologySuite.Precision;
using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System.Collections.Generic;
using System.Linq;

namespace LibSimthing
{
    public class Apartment
    {
        private HashSet<Wall> walls = new HashSet<Wall>();
        private HashSet<Interior> interiors = new HashSet<Interior>();
        
        private HashSet<Window> windows = new HashSet<Window>();
        private Dictionary<Window, Wall> windowToWall = new Dictionary<Window, Wall>();
        private Dictionary<Wall, HashSet<Window>> wallToWindows = new Dictionary<Wall, HashSet<Window>>();
        private Dictionary<Window, HashSet<Room>> windowToRooms = new Dictionary<Window, HashSet<Room>>();

        private HashSet<Door> doors = new HashSet<Door>();
        private HashSet<Door> exteriorDoors = new HashSet<Door>();
        private HashSet<Door> balconyDoors = new HashSet<Door>();
        private Dictionary<Door, Wall> doorToWall = new Dictionary<Door, Wall>();
        private Dictionary<Wall, HashSet<Door>> wallToDoors = new Dictionary<Wall, HashSet<Door>>();
        private Dictionary<Door, HashSet<Room>> doorToRooms = new Dictionary<Door, HashSet<Room>>();

        private HashSet<Room> rooms = new HashSet<Room>();
        private Dictionary<Room, HashSet<Door>> roomToDoors = new Dictionary<Room, HashSet<Door>>();
        private Dictionary<Room, HashSet<Window>> roomToWindows = new Dictionary<Room, HashSet<Window>>();

        private string id;

        #region public methods
        public void ComputeRooms()
        {
            this.calculateRooms();
            foreach (Door door in this.Doors)
            {
                if (this.GetRooms(door).Count <= 1) this.ExteriorDoors.Add(door);
                foreach (Room r in this.GetRooms(door))
                    if (r.Function == Room.Functionality.Outdoor)
                        this.BalconyDoors.Add(door);
            }
        }

        public List<Line> RoomFootprintsNoDoorsNoWindows()
        {
            List<Line> lines = new List<Line>();

            foreach (Room r in this.Rooms)
            {
                for (int i = 0; i < r.CyclicVertices.Count - 1; i++)
                {
                    Point3d current = r.CyclicVertices[i];
                    Point3d next = r.CyclicVertices[i + 1];
                    LineCurve lc = new LineCurve(current, next);

                    List<Point3d> doorPoints = new List<Point3d>();
                    foreach (Door d in r.Doors)
                    {
                        if (this.GetRooms(d).Count < 2) continue;

                        CurveIntersections intersections = Intersection.CurveCurve(d.Footprint(), lc, 0.001, 0.001);
                        if (intersections.Count == 1)
                        {
                            doorPoints.AddRange(new Point3d[] { intersections[0].PointB, intersections[0].PointB2 });
                        }
                    }
                    foreach (Window w in this.Windows)
                    {
                        CurveIntersections intersections = Intersection.CurveCurve(w.FootprintOnGround(), lc, 0.001, 0.001);
                        if (intersections.Count == 1)
                        {
                            doorPoints.AddRange(new Point3d[] { intersections[0].PointB, intersections[0].PointB2 });
                        }
                        else if (intersections.Count == 2)
                        {
                            doorPoints.AddRange(new Point3d[] { intersections[0].PointB, intersections[1].PointB });
                        }
                    }

                    doorPoints.Add(current);
                    doorPoints.Add(next);
                    Line l = new Line(current, next);
                    doorPoints.Sort((x1, x2) => l.ClosestParameter(x1).CompareTo(l.ClosestParameter(x2)));

                    current = doorPoints[0];
                    for (int j = 1; j < doorPoints.Count; j++)
                    {
                        Point3d p = doorPoints[j];
                        if (j % 2 == 1)
                        {
                            lines.Add(new Line(current, p));
                        }
                        current = p;
                    }

                    next = doorPoints[doorPoints.Count - 1];

                    if (!current.EpsilonEquals(next, 0.001))
                    {
                        lines.Add(new Line(current, next));
                    }
                }

                foreach (Wall pillar in r.Pillars)
                {
                    lines.AddRange(pillar.Lines2D());
                }
            }

            // TODO: fix this
            foreach (Door d in this.Doors)
            {
                List<Line> doorLines = d.Lines2D();
                lines.Add(doorLines[1]);
                lines.Add(doorLines[3]);
            }
            foreach (Window d in this.Windows)
            {
                List<Line> windowLines = d.Lines2DOnGround();
                lines.Add(windowLines[1]);
                lines.Add(windowLines[3]);
            }

            return lines;
        }

        public List<Line> RoomFootprintsNoDoors()
        {
            List<Line> lines = new List<Line>();

            foreach (Room r in this.Rooms)
            {
                for (int i = 0; i < r.CyclicVertices.Count - 1; i++)
                {
                    Point3d current = r.CyclicVertices[i];
                    Point3d next = r.CyclicVertices[i + 1];
                    LineCurve lc = new LineCurve(current, next);

                    List<Point3d> doorPoints = new List<Point3d>();
                    foreach (Door d in r.Doors)
                    {
                        if (this.GetRooms(d).Count < 2) continue;

                        CurveIntersections intersections = Intersection.CurveSurface(lc, d.Surface(), 0.001, 0.001);
                        if (intersections.Count == 1)
                        {
                            doorPoints.AddRange(new Point3d[] { intersections[0].PointA, intersections[0].PointA2 });
                        }
                    }
                    doorPoints.Add(current);
                    doorPoints.Add(next);
                    Line l = new Line(current, next);
                    doorPoints.Sort((x1, x2) => l.ClosestParameter(x1).CompareTo(l.ClosestParameter(x2)));

                    current = doorPoints[0];
                    for (int j = 1; j < doorPoints.Count; j++)
                    {
                        Point3d p = doorPoints[j];
                        if (j % 2 == 1)
                        {
                            lines.Add(new Line(current, p));
                        }
                        current = p;
                    }

                    next = doorPoints[doorPoints.Count - 1];

                    if (!current.EpsilonEquals(next, 0.001))
                    {
                        lines.Add(new Line(current, next));
                    }
                }

                foreach (Wall pillar in r.Pillars)
                {
                    lines.AddRange(pillar.Lines2D());
                }
            }

            // TODO: fix this
            foreach (Door d in this.Doors)
            {
                List<Line> doorLines = d.Lines2D();
                lines.Add(doorLines[1]);
                lines.Add(doorLines[3]);
            }

            return lines;
            //Dictionary<Door, List<Point3d>> doorToPoints = new Dictionary<Door, List<Point3d>>();
            //foreach (Door d in this.Doors)
            //{
            //    List<Point3d> points = new List<Point3d>();
            //    List<int> counts = new List<int>();

            //    // find the intersection points for all rooms
            //    foreach (Room r in this.GetRooms(d))
            //    {
            //        for (int i = 0; i < r.CyclicVertices.Count - 1; i++)
            //        {
            //            Point3d current = r.CyclicVertices[i];
            //            Point3d next = r.CyclicVertices[i + 1];
            //            LineCurve lc = new LineCurve(current, next);

            //            CurveIntersections intersections = Intersection.CurveCurve(d.Footprint(), lc, 0.001, 0.001);
            //            if (intersections.Count > 0)
            //            {
            //                points.Add(intersections[0].PointB);
            //                points.Add(intersections[0].PointB2);
            //                counts.Add(0);
            //                counts.Add(0);
            //            }
            //        }
            //    }

            //    // check at each point if the door intersects two rooms
            //    List<Line> doorLines = d.Lines2D();
            //    for (int i = 0; i < points.Count; i++)
            //    {
            //        Transform translation = Transform.Translation(points[i] - doorLines[1].From);
            //        Point3d[] newLinePoints = translation.TransformList(new Point3d[] { doorLines[1].From, doorLines[1].To });
            //        LineCurve newLine = new LineCurve(newLinePoints[0], newLinePoints[1]);

            //        Transform rotation = Transform.Rotation(System.Math.PI, new Vector3d(0, 0, 1), newLinePoints[0]);
            //        Point3d[] newLinePoints2 = rotation.TransformList(newLinePoints);
            //        LineCurve newLine2 = new LineCurve(newLinePoints2[0], newLinePoints2[1]);

            //        foreach (Room r in this.GetRooms(d))
            //        {
            //            CurveIntersections intersections = Intersection.CurveCurve(r.Footprint(), newLine, 0.001, 0.001);
            //            CurveIntersections intersections2 = Intersection.CurveCurve(r.Footprint(), newLine2, 0.001, 0.001);
            //            if (intersections.Count > 0 || intersections2.Count > 0)
            //            {
            //                counts[i]++;
            //            }
            //        }
            //    }

            //    doorToPoints.Add(d, new List<Point3d>());
            //    for (int i = 0; i < points.Count; i++)
            //        if (counts[i] >= 2) doorToPoints[d].Add(points[i]);
            //}

            //List<Line> lines = new List<Line>();
            //foreach (Room r in this.Rooms)
            //{
            //    for (int i = 0; i < r.CyclicVertices.Count - 1; i++)
            //    {
            //        Point3d current = r.CyclicVertices[i];
            //        Point3d next = r.CyclicVertices[i + 1];
            //        Line l0 = new Line(current, next);
            //        LineCurve lc = new LineCurve(current, next);
            //        Line l = new Line(current, next);

            //        Dictionary<Point3d, bool> drawToNode = new Dictionary<Point3d, bool>();
            //        drawToNode[current] = true;
            //        drawToNode[next] = true;

            //        List<Point3d> doorPoints = new List<Point3d>();
            //        doorPoints.Add(current);
            //        doorPoints.Add(next);
            //        foreach (Door d in r.Doors)
            //        {
            //            doorToPoints[d].Sort((x1, x2) => l.ClosestParameter(x1).CompareTo(l.ClosestParameter(x2)));
            //            CurveIntersections intersections = Intersection.CurveCurve(d.Footprint(), lc, 0.001, 0.001);
            //            if (intersections.Count == 1) {
            //                bool draw = false;
            //                foreach (Point3d p in doorToPoints[d])
            //                {
            //                    draw = !draw;

            //                    if (l0.MinimumDistanceTo(p) < 0.001)
            //                    {
            //                        bool alreadyAdded = false;
            //                        foreach (Point3d p2 in doorPoints)
            //                        {
            //                            if (p2.EpsilonEquals(p, 0.001))
            //                            {
            //                                alreadyAdded = true;
            //                                drawToNode[p2] = draw;
            //                                break;
            //                            }
            //                        }
            //                        if (!alreadyAdded)
            //                        {
            //                            doorPoints.Add(p);
            //                            drawToNode[p] = draw;
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        doorPoints.Sort((x1, x2) => l.ClosestParameter(x1).CompareTo(l.ClosestParameter(x2)));

            //        current = doorPoints[0];
            //        for (int j = 1; j < doorPoints.Count; j++)
            //        {
            //            Point3d p = doorPoints[j];
            //            if (drawToNode[p])
            //                lines.Add(new Line(current, p));

            //            current = p;
            //        }

            //        next = doorPoints[doorPoints.Count - 1];

            //        if (!current.EpsilonEquals(next, 0.001))
            //            lines.Add(new Line(current, next));
            //    }
            //}

            //return lines;
        }

        #endregion

        #region setters & adders

        /// <summary>
        /// Adds a wall the apartment
        /// </summary>
        /// <param name="x">The x value of the unrotated wall.</param>
        /// <param name="y">The y value of the unrotated wall.</param>
        /// <param name="z">The z value of the unrotated wall.</param>
        /// <param name="length">The wall's x-offset</param>
        /// <param name="width">The wall's y-offset</param>
        /// <param name="height">The wall's z-offset</param>
        /// <param name="angle">The wall's angle</param>
        /// <returns></returns>
        public Wall AddWall(double x, double y, double z, double length, double width, double height, double angle)
        {
            Wall wall = new Wall() { X = x, Y = y, Z = z, Length = length, Width = width, Height = height, Angle = angle};
            if (this.walls.Add(wall)) return wall;
            return null;
        }


        /// <summary>
        /// Adds an interior object to the apartment.
        /// </summary>
        /// <param name="category">The interior's category</param>
        /// <param name="url">The url to the interior json file.</param>
        /// <param name="x">The x value without rotation</param>
        /// <param name="y">The y value without rotation</param>
        /// <param name="z">The y value without rotation</param>
        /// <param name="angle">The angle</param>
        /// <returns>The Interior object if the placement was successfull, null otherwise</returns>
        public Interior AddInterior(Interior.InteriorCategory category, string url, double x, double y, double z, double angle)
        {
            Interior interior = new Interior { Category = category, Url = url, X = x, Y = y, Z = z, Angle = angle };
            if (this.Interiors.Add(interior)) return interior;
            return null;
        }


        /// <summary>
        /// Adds a door to the apartment.
        /// </summary>
        /// <param name="wall">The wall into which it is placed.</param>
        /// <param name="x">The x-offset in the unrotated wall</param>
        /// <param name="y">The y-offset in the unrotated wall</param>
        /// <param name="length">The length</param>
        /// <param name="height">The height</param>
        /// <returns>Returns the door if the placement was successfull, null otherwise</returns>
        public Door AddDoor(Wall wall, double x, double y, double length, double height)
        {
            Door door = new Door { Origin = wall, X = x, Y = y, Length = length, Height = height};
            if (this.Doors.Add(door))
            {
                this.doorToWall.Add(door, wall);

                if (!this.wallToDoors.ContainsKey(wall))
                    this.wallToDoors[wall] = new HashSet<Door>();
                this.wallToDoors[wall].Add(door);

                return door;
            }
            return null;
        }

        /// <summary>
        /// Adds a window to the apartment.
        /// </summary>
        /// <param name="wall">The wall into which it is placed.</param>
        /// <param name="x">The x-offset in the unrotated wall</param>
        /// <param name="y">The y-offset in the unrotated wall</param>
        /// <param name="length">The length</param>
        /// <param name="height">The height</param>
        /// <returns>Returns the door if the placement was successfull, null otherwise</returns>
        public Window AddWindow(Wall wall, double x, double y, double length, double height)
        {
            Window window = new Window { Origin = wall, X = x, Y = y, Length = length, Height = height };
            if (this.Windows.Add(window))
            {
                this.windowToWall.Add(window, wall);

                if (!this.wallToWindows.ContainsKey(wall))
                    this.wallToWindows[wall] = new HashSet<Window>();
                this.wallToWindows[wall].Add(window);

                return window;
            }
            return null;
        }


        private void AddRoom(Room room)
        {
            this.Rooms.Add(room);
            this.roomToDoors.Add(room, new HashSet<Door>(room.Doors));
            this.roomToWindows.Add(room, new HashSet<Window>(room.Windows));
            foreach (Door door in room.Doors)
            {
                if (!this.doorToRooms.ContainsKey(door)) this.doorToRooms[door] = new HashSet<Room>();
                this.doorToRooms[door].Add(room);
            }
            foreach (Window window in room.Windows)
            {
                if (!this.windowToRooms.ContainsKey(window)) this.windowToRooms[window] = new HashSet<Room>();
                this.windowToRooms[window].Add(room);
            }
        }

        #endregion

        #region getters

        public HashSet<Window> GetWindows(Wall wall)
        {
            if (!this.wallToWindows.ContainsKey(wall)) return new HashSet<Window>();
            return this.wallToWindows[wall];
        }

        public HashSet<Door> GetDoors(Wall wall)
        {
            if (!this.wallToDoors.ContainsKey(wall)) return new HashSet<Door>();
            return this.wallToDoors[wall];
        }

        public Wall GetWall(Window window)
        {
            if (!this.windowToWall.ContainsKey(window)) return null;
            return this.windowToWall[window];
        }

        public Wall GetWall(Door door)
        {
            if (!this.doorToWall.ContainsKey(door)) return null;
            return this.doorToWall[door];
        }

        public HashSet<Room> GetRooms(Door door)
        {
            if (!doorToRooms.ContainsKey(door)) return new HashSet<Room>();
            return this.doorToRooms[door];
        }
        public HashSet<Room> GetRooms(Window window)
        {
            if (!windowToRooms.ContainsKey(window)) return new HashSet<Room>();
            return this.windowToRooms[window];
        }
        #endregion getters

        #region properties
        public HashSet<Wall> Walls
        {
            get
            {
                return walls;
            }

            set
            {
                walls = value;
            }
        }

        public HashSet<Window> Windows
        {
            get
            {
                return windows;
            }

            set
            {
                windows = value;
            }
        }

        public HashSet<Door> Doors
        {
            get
            {
                return doors;
            }

            set
            {
                doors = value;
            }
        }

        public HashSet<Interior> Interiors
        {
            get
            {
                return interiors;
            }

            set
            {
                interiors = value;
            }
        }

        public HashSet<Room> Rooms
        {
            get
            {
                return rooms;
            }

            set
            {
                rooms = value;
            }
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public HashSet<Door> ExteriorDoors
        {
            get
            {
                return exteriorDoors;
            }

            set
            {
                exteriorDoors = value;
            }
        }

        public HashSet<Door> BalconyDoors
        {
            get
            {
                return balconyDoors;
            }

            set
            {
                balconyDoors = value;
            }
        }
        #endregion properties

        #region private methods

        private class LineStringAdder : IGeometryComponentFilter
        {
            PolygonizeGraph _graph;
            public LineStringAdder(PolygonizeGraph graph)
            {
                _graph = graph;
            }

            public void Filter(IGeometry g)
            {
                var lineString = g as ILineString;
                if (lineString != null)
                    _graph.AddEdge(lineString);

            }
        }

        /// <summary>
        /// Generates an apartment footprint including ALL lines for usage in the NetTopologSuite.
        /// </summary>
        /// <returns></returns>
        private ICollection<IGeometry> NTS_Footprint()
        {
            // get wall lines
            List<Line> lines = new List<Line>();
            foreach (Wall wall in this.Walls)
                //lines.Add(wall.ExtendedAxis(this.Walls));
                lines.AddRange(wall.Lines2D());

            // compute all segments
            List<OpenTK.Vector2d[]> lineEnds = new List<OpenTK.Vector2d[]>();
            List<Line> lineSegments = new List<Line>();
            double a, b;
            OpenTK.Vector2d fromTK, toTK;
            for (int i = 0; i < lines.Count; i++)
            {
                List<Point3d> intersectionPoints = new List<Point3d>();
                for (int j = 0; j < lines.Count; j++)
                {
                    if (i == j) continue;
                    if (Intersection.LineLine(lines[i], lines[j], out a, out b, 0.001, true))
                        intersectionPoints.Add(lines[i].PointAt(a));
                }
                intersectionPoints.Add(lines[i].PointAt(0));
                intersectionPoints.Add(lines[i].PointAt(1));
                intersectionPoints.OrderBy(x => lines[i].ClosestParameter(x));
                for (int k = 0; k < intersectionPoints.Count - 1; k++)
                {
                    lineSegments.Add(new Line(intersectionPoints[k], intersectionPoints[k + 1]));
                    fromTK = new OpenTK.Vector2d(intersectionPoints[k].X, intersectionPoints[k].Y);
                    toTK = new OpenTK.Vector2d(intersectionPoints[k + 1].X, intersectionPoints[k + 1].Y);
                    lineEnds.Add(new OpenTK.Vector2d[] { fromTK, toTK });
                }
            }

            // construct network
            List<Line2D> network = new List<Line2D>();
            for (int i = 0; i < lineSegments.Count; i++)
            {
                network.Add(new Line2D(lineEnds[i][0], lineEnds[i][1]));
            }

            // YUFAN START
            IGeometryFactory geometryFactory = new GeometryFactory();
            WKTReader rdr = new WKTReader(geometryFactory);
            PrecisionModel precisionModel = new PrecisionModel(10000d);

            // reduce precision of network lines
            GeometryPrecisionReducer gpr = new GeometryPrecisionReducer(precisionModel);
            LineString lineString;
            List<IGeometry> graphLinesRounded = new List<IGeometry>();
            foreach (Line2D netLine in network)
            {
                if (netLine.Start == netLine.End) continue;
                lineString = GeometryConverter.ToLineString(netLine);
                IGeometry line = rdr.Read(lineString.ToString());
                IGeometry reducedLine = gpr.Reduce(line);
                if (!reducedLine.IsEmpty)
                    graphLinesRounded.Add(reducedLine);
            }

            // node network
            GeometryNoder geomNoder = new GeometryNoder(precisionModel);
            IList<IGeometry> graphLines = new List<IGeometry>();
            IList<ILineString> lineStrings = geomNoder.Node(graphLinesRounded as ICollection<IGeometry>);

            HashSet<LineString> addedLinestrings = new HashSet<LineString>();
            // remove duplicates
            foreach (LineString linestring in lineStrings)
            {
                if (!addedLinestrings.Contains(linestring))
                {
                    graphLines.Add(rdr.Read(linestring.ToString()));
                    addedLinestrings.Add(linestring);
                }
            }
            ICollection<IGeometry> unionized = new List<IGeometry>();
            unionized.Add(NetTopologySuite.Operation.Union.UnaryUnionOp.Union(graphLines));
            return unionized;
        }

        private ICollection<EdgeRing> polygonize(ICollection<IGeometry> geomList)
        {
            IGeometry geom_init = geomList.First() as IGeometry;
            PolygonizeGraph _graph = new PolygonizeGraph(geom_init.Factory);

            foreach (var geometry in geomList)
            {
                LineStringAdder _lineStringAdder = new LineStringAdder(_graph);
                geometry.Apply(_lineStringAdder);
            }

            ICollection<IGeometry> _polyList = new List<IGeometry>();
            ICollection<ILineString> _dangles = new List<ILineString>();
            ICollection<ILineString> _cutEdges = new List<ILineString>();
            IList<IGeometry> _invalidRingLines = new List<IGeometry>();
            List<EdgeRing> _holeList;
            List<EdgeRing> _shellList;

            // if no geometries were supplied it's possible that graph is null
            _dangles = _graph.DeleteDangles();
            _cutEdges = _graph.DeleteCutEdges();
            var edgeRingList = _graph.GetEdgeRings();

            IList<EdgeRing> validEdgeRingList = new List<EdgeRing>();
            foreach (var er in edgeRingList)
            {
                if (er.IsValid)
                    validEdgeRingList.Add(er);
                else _invalidRingLines.Add(er.LineString);
            }

            _holeList = new List<EdgeRing>();
            _shellList = new List<EdgeRing>();
            foreach (var er in validEdgeRingList)
            {
                er.ComputeHole();
                if (er.IsHole)
                    _holeList.Add(er);
                else _shellList.Add(er);
            }

            return _shellList;
        }

        private void calculateRooms()
        {
            // compute polygons
            ICollection<EdgeRing> polys = polygonize(this.NTS_Footprint());

            // check which polygons are valid rooms
            foreach (EdgeRing er in polys)
            {
                List<Point3d> points = new List<Point3d>();
                for (int i = 0; i < er.Polygon.Coordinates.Count(); i++)
                {
                    Coordinate coord = er.Polygon.Coordinates[i];
                    points.Add(new Point3d(coord.X, coord.Y, 0));
                }

                Room room = new Room(this, points);
                List<Point3d> cyclicVertices = room.CyclicVertices;
                bool hasOpenSpace = true;

                List<List<Point3d>> pillars = new List<List<Point3d>>();
                Curve roomFootprint = room.Footprint();
                foreach (Wall w in this.Walls)
                {
                    bool isPillar = true;

                    List<Line> wallLines = w.Lines2D();
                    foreach (Line line in wallLines)
                    {
                        PointContainment pc1 = roomFootprint.Contains(line.From);
                        PointContainment pc2 = roomFootprint.Contains(line.From);

                        if (pc2 == PointContainment.Outside || pc1 == PointContainment.Outside)
                            isPillar = false;
                    }
                    if (isPillar) room.AddPillar(w);


                    int coincideCount = 0;
                    Curve curveW = w.Footprint();

                    foreach (Point3d p in cyclicVertices)
                    {
                        PointContainment pc = curveW.Contains(p);
                        if (pc != PointContainment.Outside) coincideCount++;
                    }
                    if (coincideCount >= cyclicVertices.Count)
                    {
                        hasOpenSpace = false;
                        break;
                    }
                }
                if (hasOpenSpace)
                {
                    this.AddRoom(room);
                }
            }
        }

        #endregion

    }
}
