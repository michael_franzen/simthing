﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Geometry;

namespace LibSimthing
{
    public class SimthingResultObject
    {
        private string name;
        private List<Rhino.Geometry.GeometryBase> geometry;
        private List<double> values;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public List<GeometryBase> Geometry
        {
            get
            {
                return geometry;
            }

            set
            {
                geometry = value;
            }
        }

        public List<double> Values
        {
            get
            {
                return values;
            }

            set
            {
                values = value;
            }
        }
    }
}
