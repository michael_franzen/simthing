﻿using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LibSimthing
{
    public class Room : IPhysicalObject
    {
        private List<Wall> cyclicWalls = new List<Wall>();
        private List<Door> doors = new List<Door>();
        private List<Window> windows = new List<Window>();
        private Apartment apartment = new Apartment();
        private List<Point3d> cyclicVertices = new List<Point3d>();
        private List<Interior> interiors = new List<Interior>();
        private Functionality function = Functionality.Others;
        private List<Wall> pillars = new List<Wall>();

        public Room(Apartment apartment, List<Point3d> cyclicVertices, double tolerance = 0.001, double overlapTolerance = 0.001)
        {
            this.Apartment = apartment;
            this.CyclicVertices = cyclicVertices;
            if (this.CyclicVertices[0] != this.CyclicVertices.Last())
            {
                this.CyclicVertices.Add(this.CyclicVertices[0]);
            }

            PolylineCurve footprint = new PolylineCurve(this.CyclicVertices);
            
            foreach (Door door in Apartment.Doors)
            {
                CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveSurface(footprint, door.Surface(), tolerance, overlapTolerance);
                if (intersections.Count > 0) this.Doors.Add(door);
            }

            foreach (Window window in Apartment.Windows)
            {
                CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveCurve(footprint, window.FootprintOnGround(), tolerance, overlapTolerance);
                if (intersections.Count > 0) this.Windows.Add(window);
            }

            foreach (Wall wall in Apartment.Walls)
            {
                CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveSurface(footprint, wall.Surface(), tolerance, overlapTolerance);
                if (intersections.Count > 0) this.Walls.Add(wall);
            }

            Dictionary<Functionality, int> counts = new Dictionary<Functionality, int>();
            foreach (Interior interior in this.apartment.Interiors)
            {
                PointContainment pc = footprint.Contains(interior.Position);
                if (pc != PointContainment.Outside )
                {
                    this.Interiors.Add(interior);

                    Functionality function;
                    switch (interior.Category)
                    {
                        case Interior.InteriorCategory.bathroom:
                            function = Functionality.Bathroom;
                            break;
                        case Interior.InteriorCategory.kitchen:
                            function = Functionality.Kitchen;
                            break;
                        case Interior.InteriorCategory.hallway:
                            function = Functionality.Hallway;
                            break;
                        case Interior.InteriorCategory.outdoor:
                            function = Functionality.Outdoor;
                            break;
                        default:
                            function = Functionality.Others;
                            break;
                    }
                    if (!counts.ContainsKey(function)) counts[function] = 0;
                    counts[function]++;
                }

                int max = 0;
                foreach (Functionality f in counts.Keys)
                {
                    if (counts[f] > max)
                    {
                        max = counts[f];
                        this.Function = f;
                    }
                    else if (this.Function == Functionality.Others && counts[f] == max)
                    {
                        this.Function = f;
                    }
                }
            }
        }

        public List<Line> Lines2D()
        {
            List<Line> lines = new List<Line>();
            Point3d current = this.CyclicVertices[0];
            for (int i = 1; i < this.CyclicVertices.Count; i++)
            {
                lines.Add(new Line(current, this.CyclicVertices[i]));
                current = this.CyclicVertices[i];
            }

            foreach (Wall wall in this.Pillars)
            {
                lines.AddRange(wall.Lines2D());
            }
            return lines;
        }

        public PolylineCurve Footprint()
        {
            return new PolylineCurve(CyclicVertices);
        }
        

        public Surface Surface()
        {
            PolylineCurve pl = this.Footprint();
            return Rhino.Geometry.Extrusion.Create(this.Footprint(), Walls[0].Height, true);
        }

        public Mesh[] Mesh(double maxEdge = 0.1)
        {
            MeshingParameters parameters = new MeshingParameters();
            parameters.MaximumEdgeLength = maxEdge;
            return Rhino.Geometry.Mesh.CreateFromBrep(this.Surface().ToBrep(), parameters);
        }

        public void AddPillar(Wall wall)
        {
            this.Pillars.Add(wall);
        }

        public void AddWall(Wall wall)
        {
            this.Walls.Add(wall);
        }
        

        public enum Functionality
        {
            Kitchen,
            Bathroom,
            Outdoor,
            Hallway,
            Others
        }

        #region properties
        public Point3d Center
        {
            get
            {
                List<Point3d> wallCenters = new List<Point3d>();
                foreach (Wall wall in Walls)
                    wallCenters.Add(wall.Axis().PointAt(0.5));

                return new Polyline(wallCenters).CenterPoint();
            }
        }

        public List<Wall> Walls
        {
            get
            {
                return cyclicWalls;
            }

            set
            {
                cyclicWalls = value;
            }
        }

        public List<Door> Doors
        {
            get
            {
                return doors;
            }

            set
            {
                doors = value;
            }
        }

        public List<Window> Windows
        {
            get
            {
                return windows;
            }

            set
            {
                windows = value;
            }
        }

        public Apartment Apartment
        {
            get
            {
                return apartment;
            }

            set
            {
                apartment = value;
            }
        }

        public double Height
        {
            get
            {
                return this.Walls[0].Height;
            }
        }

        public List<Point3d> CyclicVertices
        {
            get
            {
                return cyclicVertices;
            }

            set
            {
                cyclicVertices = value;
            }
        }

        public Functionality Function
        {
            get
            {
                return function;
            }

            set
            {
                function = value;
            }
        }

        public List<Interior> Interiors
        {
            get
            {
                return interiors;
            }

            set
            {
                interiors = value;
            }
        }

        public List<Wall> Pillars
        {
            get
            {
                return pillars;
            }

            set
            {
                pillars = value;
            }
        }

        public double Perimeter()
        {
            return this.Footprint().GetLength();
        }

        public double Area()
        {
            double floorArea = 0.0;
            Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(this.Footprint());
            if (breps != null) foreach (Brep brep in breps) floorArea += brep.GetArea();
            return floorArea;
        }
        #endregion properties

    }
}
