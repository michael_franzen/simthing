﻿using Rhino.Geometry;

namespace LibSimthing
{
    public interface IPhysicalObject
    {
        PolylineCurve Footprint();

        Surface Surface();

        Mesh[] Mesh(double maxEdge);
    }
}
