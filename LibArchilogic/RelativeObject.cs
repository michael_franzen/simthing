﻿namespace Archilogic.Data
{
    public class ALVolatileObject : ALSolidObject
    {
        private VolatileObjectType type;

        public new VolatileObjectType Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public enum VolatileObjectType
        {
            Box,
            Door,
            Floorplan,
            Floor,
            Wall,
            Window,
            Interior,
            Group,
            Closet,
            Polyfloor,
            Object,
            Polybox,
            Railing,
            Curtain,
            Level,
            Kitchen,
            Camera_Bookmark,
            [Newtonsoft.Json.JsonProperty(PropertyName = "Camera-bookmark")]
            Camerabookmark,
            Script
        }
    }
}
