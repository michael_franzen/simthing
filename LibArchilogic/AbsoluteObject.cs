﻿using Newtonsoft.Json;
using Rhino.Geometry;
using System.Collections.ObjectModel;

namespace Archilogic.Data
{
    public class ALSolidObject
    {
        private SolidObjectType type;
        protected Collection<ALVolatileObject> children;
        protected double angle;
        protected Point3d corner = new Point3d();
        protected Point3d offset = new Point3d();
        private string interior = "";

        public Collection<ALVolatileObject> Children
        {
            get
            {
                return this.children;
            }
            set
            {
                this.children = value;
            }
        }

        public SolidObjectType Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public double X
        {
            get
            {
                return corner[0];
            }

            set
            {
                corner[0] = value;
            }
        }

        public double Y
        {
            get
            {
                return corner[1];
            }

            set
            {
                // archilogic swapped this with Z value
                corner[2] = value;
            }
        }

        public double Z
        {
            get
            {
                return corner[2];
            }

            set
            {
                // archilogic swapped this with Y value
                corner[1] = value;
            }
        }

        public double L
        {
            get
            {
                return offset[0];
            }

            set
            {
                offset[0] = value;
            }
        }

        public double W
        {
            get
            {
                return offset[1];
            }

            set
            {
                offset[1] = value;
            }
        }

        public double H
        {
            get
            {
                return offset[2];
            }

            set
            {
                offset[2] = value;
            }
        }

        public double Ry
        {
            get
            {
                return angle;
            }

            set
            {
                angle = value;
            }
        }

        public Point3d Corner
        {
            get
            {
                return corner;
            }

            set
            {
                corner = value;
            }
        }

        public Point3d Offset
        {
            get
            {
                return offset;
            }

            set
            {
                offset = value;
            }
        }

        public string Interior
        {
            get
            {
                return interior;
            }

            set
            {
                interior = value;
            }
        }

        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum SolidObjectType
        {

            Box,
            Group,
            Closet,
            Floorplan,
            Floor,
            Wall,
            Polyfloor,
            Object,
            Polybox,
            Railing,
            Interior,
            Curtain,
            Level,
            Kitchen,
            Camera_Bookmark,
            [Newtonsoft.Json.JsonProperty(PropertyName ="Camera-bookmark")]
            Camerabookmark,
            Script
        }
    }
}