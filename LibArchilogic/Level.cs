﻿using System.Collections.ObjectModel;

namespace Archilogic.Data
{
    /// <summary>
    /// A level consists of a set of fixed objects with absolute coordinates.
    /// </summary>
    public class ALLevel
    {
        private string id;
        private Collection<ALSolidObject> children;

        /// <summary>
        /// The objects within the level (i.e. walls, floors and interior)
        /// </summary>
        public Collection<ALSolidObject> Children
        {
            get
            {
                return this.children;
            }
            set
            {
                this.children = value;
            }
        }

        /// <summary>
        ///  The level's guid.
        /// </summary>
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }
}
