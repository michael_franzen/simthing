﻿using System.Collections.ObjectModel;

namespace Archilogic.Data
{
    /// <summary>
    /// An ArchiLogic Apartment object consists of multiple Level objects.
    /// </summary>
    public class ALApartment
    {
        private Collection<ALLevel> children;

        private string id;

        /// <summary>
        /// The Apartment's levels.
        /// </summary>
        public Collection<ALLevel> Children
        {
            get
            {
                return children;
            }

            set
            {
                children = value;
            }
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        public string Uúid
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }
}
