﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GPUIsovistField.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Cloo;

namespace CPlan.Isovist2D
{
	public class GPUIsovistFieldAcoustics
	{
		protected static GPUIsovistFieldAcoustics m_instance;

		float[] m_wallP1X;
		float[] m_wallP1Y;
		float[] m_wallP2X;
		float[] m_wallP2Y;
		float[] m_extWallPX;
		float[] m_extWallPY;
		float[] m_pX;
		float[] m_pY;
		float m_precision;

		ComputeProgram program;
		ComputePlatform platform;
		IList<ComputeDevice> devices;
		ComputeContextPropertyList properties;
		ComputeContext context;

		ComputeBuffer<float> wallP1X_buf;
		ComputeBuffer<float> wallP1Y_buf;
		ComputeBuffer<float> wallP2X_buf;
		ComputeBuffer<float> wallP2Y_buf;
		ComputeBuffer<float> extWallPX_buf;
		ComputeBuffer<float> extWallPY_buf;
		ComputeBuffer<float> pX_buf;
		ComputeBuffer<float> pY_buf;
		ComputeBuffer<float> area_buf;
		ComputeKernel kernel;
		ComputeEventList eventList;
		ComputeCommandQueue commands;


		#region ComputationTextMultipleIsovistCalculation
		string clIsovistMultipleCalculation = @"
float2 LineIntersect(float L1P1X, float L1P1Y, float L1P2X, float L1P2Y,
float L2P1X, float L2P1Y,float L2P2X, float L2P2Y)
{
	float2 ptIntersection;
	ptIntersection.x = MAXFLOAT;
	ptIntersection.y = MAXFLOAT;
	
	float d = (L2P2Y - L2P1Y) * (L1P2X - L1P1X) - (L2P2X - L2P1X) * (L1P2Y - L1P1Y);
	float n_a = (L2P2X - L2P1X) * (L1P1Y - L2P1Y) - (L2P2Y - L2P1Y) * (L1P1X - L2P1X);
	float n_b = (L1P2X - L1P1X) * (L1P1Y - L2P1Y) - (L1P2Y - L1P1Y) * (L1P1X - L2P1X);

	if (d == 0)
		return ptIntersection;

	float ua = n_a / d;
	float ub = n_b / d;

	if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
	{
		ptIntersection.x = L1P1X + (ua * (L1P2X - L1P1X));
		ptIntersection.y = L1P1Y + (ua * (L1P2Y - L1P1Y));
		return ptIntersection;
	}
	return ptIntersection;
}

bool PointOnLine(float pointX, float pointY, float startX, float startY, float endX, float endY)
{
float dxc = pointX - startX;
float dyc = pointY - startY;

float dxl = endX - startX;
float dyl = endY - startY;

float cross = dxc * dyl - dyc * dxl;

if (cross == 0)
  return true;
else
  return false;
}


float Map(float val, float minOrigin, float maxOrigin, float minTarget, float maxTarget)
{
	if (val > maxOrigin)
		return maxTarget;
	else
		return minTarget - ((minTarget - maxTarget) * ((val - minOrigin) / (maxOrigin - minOrigin)));
}

kernel void Calculate(
	global  read_only float* wallP1X,
	global  read_only float* wallP1Y,
	global  read_only float* wallP2X,
	global  read_only float* wallP2Y,
	global  read_only float* pX,
	global  read_only float* pY,
	global write_only float* area,
	float exactness,
	int wallCount,
	global  read_only float* extWallPX,
	global  read_only float* extWallPY,
	int extWallPCount
)
{
		int gID0 = get_global_id(0);
		int rayCount = (int) (2*3.14159265359 / exactness);

		float2 curPerimeterPoint;
		float2 oldPerimeterPoint;
		float2 curPWeighted;
		float2 oldPWeighted;
		float2 firstPerimeterPoint;
		float2 rayP1 = (float2){ pX[gID0], pY[gID0] };
		float tmp_area = 0.0f;
		float sound_startDecibel = 70.0f;
		float sound_attentuation = 14.244f;

        // numbers for random number generator
        unsigned int rx = gID0 + 632632;
        unsigned int ry = gID0 + 21523626;
        unsigned int rz = gID0 + 12315;
        unsigned int rw = gID0 + 51261;

		//Isovist berechnen...........................................................
		int curWall = 0;
		int prevWall = 0;
		
		for (int i = 0; i< rayCount; i++)
		{
			float angle = i*exactness;
			float l = 10000;
			float2 rayP2;        
			rayP2.x = rayP1.x + sin(angle) * l;
			rayP2.y = rayP1.y + cos(angle) * l;

			float2 cutPoint;
			float mindist = MAXFLOAT;
			bool cutted = false; 

			for (int j = 0; j < wallCount; j++)
			{
				bool intersects = true;
				float2 iPoint = LineIntersect(rayP1.x, rayP1.y, rayP2.x, rayP2.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
				if ((iPoint.x == MAXFLOAT) && (iPoint.y == MAXFLOAT))
					intersects = false;

				if (intersects)
				{
					float dist = distance(rayP1, iPoint);

					if (dist < mindist)
					{
						cutPoint = iPoint;
						cutted = true;
						mindist = dist;
						curWall = j;
					}
				}
			}

			if (!cutted)
				cutPoint = rayP2;
			
			if (i == 0)
				firstPerimeterPoint = cutPoint;                

			oldPerimeterPoint = curPerimeterPoint;
			curPerimeterPoint = cutPoint;
			
			if (i != 0)
			{
                int monte_carlo_sample_size = 100;
                float weighted_area = 0.0f;
                for (int k = 0; k < monte_carlo_sample_size; k++) {
                    // generate two random numbers from 0 to 1
                    //t
                    unsigned int rt = rx;
                    rt ^= rt << 11;
                    rt ^= rt >> 8;
                    rx = ry; ry = rz; rz = rw;
                    rw ^= rw >> 19;
                    rw ^= rt;
                    float t = (float)(rw / 4294967295.0);

                    //beta
                    rt = rx;
                    rt ^= rt << 11;
                    rt ^= rt >> 8;
                    rx = ry; ry = rz; rz = rw;
                    rw ^= rw >> 19;
                    rw ^= rt;
                    float beta = (float)(rw / 4294967295.0);

                    float2 currentPoint = ((curPerimeterPoint-oldPerimeterPoint)*beta+oldPerimeterPoint-rayP1)*t + rayP1;
					float add_term = (sound_startDecibel-sound_attentuation*length(currentPoint - rayP1))/sound_startDecibel;
					if (add_term > 0) weighted_area += add_term;
                }
                float total_area = 0.5*distance(oldPerimeterPoint, rayP1)*distance(curPerimeterPoint, rayP1)*sin(exactness);
                tmp_area += weighted_area * total_area / monte_carlo_sample_size;
			}
		}
		
		area[gID0] = tmp_area;
}";
		#endregion

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		#region Constructor

		protected GPUIsovistFieldAcoustics()
		{
            InitializeGPUWithoutReflection();
		}

		~GPUIsovistFieldAcoustics()
		{
			m_instance = null;
		}

		

		#endregion

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		# region Methods

		static public GPUIsovistFieldAcoustics GetInstance()
		{
			if (m_instance == null)
				m_instance = new GPUIsovistFieldAcoustics();
			return m_instance;
		}

		public void CreateWithoutReflection(float[] wallP1X, float[] wallP1Y, float[] wallP2X, float[] wallP2Y, float[] extWallPX, float[] extWallPY, float[] pX, float[] pY, float precision)
		{
			m_wallP1X = wallP1X;
			m_wallP1Y = wallP1Y;
			m_wallP2X = wallP2X;
			m_wallP2Y = wallP2Y;
			if (extWallPX.Length != 0)
			{
				m_extWallPX = extWallPX;
				m_extWallPY = extWallPY;
			}
			else
			{
				m_extWallPX = new float[1];
				m_extWallPX[0] = float.MaxValue;          
				m_extWallPY = new float[1];
				m_extWallPY[0] = float.MaxValue;
			}
			m_precision = precision;
			m_pX = pX;
			m_pY = pY;

			SetParametersWithoutReflection();
		}

		private void InitializeGPUWithoutReflection()
		{
			try
			{
				//ComputeDevice dev = ComputePlatform.Platforms.
				if (ComputePlatform.Platforms.Count > 1)
				{
					platform = ComputePlatform.Platforms[ComputePlatform.Platforms.Count-1];
					for (int p = 0; p < ComputePlatform.Platforms.Count; p++)
					{
						Console.WriteLine(ComputePlatform.Platforms[p].Name);
						Console.WriteLine(ComputePlatform.Platforms[p].Vendor);
						Console.WriteLine(ComputePlatform.Platforms[p].Profile);
						Console.WriteLine(ComputePlatform.Platforms[p].Version);
						Console.WriteLine(ComputePlatform.Platforms[p].Devices.Select(d => d.Name + d.Available).Aggregate((a,b) => a + " " + b));
						string vers = ComputePlatform.Platforms[p].Version;
						if (vers.Contains("CUDA"))  platform = ComputePlatform.Platforms[p];
					}
				}
				else platform = ComputePlatform.Platforms[0];

				devices = new List<ComputeDevice>();
				devices.Add(platform.Devices[0]);
				properties = new ComputeContextPropertyList(platform);
				context = new ComputeContext(devices, properties, null, IntPtr.Zero);
				eventList = new ComputeEventList();
				commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);

				program = new ComputeProgram(context, clIsovistMultipleCalculation);

				program.Build(null, null, null, IntPtr.Zero);
			}
			catch (System.Exception ex)
			{
				StringBuilder output = new StringBuilder();
				StringWriter log = new StringWriter(output);
				log.WriteLine(ex.ToString());
				log.WriteLine(" ");
				if (program != null)
					log.WriteLine(program.GetBuildLog(context.Devices[0]));
				
			}
			try
			{
				kernel = program.CreateKernel("Calculate");
			}
			catch (System.Exception ex)
			{
				StringBuilder output = new StringBuilder();
				StringWriter log = new StringWriter(output);
				log.WriteLine(ex.ToString());
				log.WriteLine(" ");
				if (program != null)
					log.WriteLine(program.GetBuildLog(context.Devices[0]));
				
			}

		}
		

		public void SetParametersWithoutReflection()
		{
			int cnt = m_pY.Length;
			//activeCells_buf = new ComputeBuffer<bool>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.ActiveCells);
			area_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);

			wallP1X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP1X);
			wallP1Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP1Y);
			wallP2X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP2X);
			wallP2Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP2Y);
			extWallPX_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_extWallPX);
			extWallPY_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_extWallPY);
			pX_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_pX);
			pY_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_pY);

			//Übergabe der Parameter:
			kernel.SetMemoryArgument(0, wallP1X_buf);
			kernel.SetMemoryArgument(1, wallP1Y_buf);
			kernel.SetMemoryArgument(2, wallP2X_buf);
			kernel.SetMemoryArgument(3, wallP2Y_buf);

			kernel.SetMemoryArgument(4, pX_buf);
			kernel.SetMemoryArgument(5, pY_buf);

			kernel.SetMemoryArgument(6, area_buf);

			kernel.SetValueArgument(7, m_precision);
			kernel.SetValueArgument(8, m_wallP1X.Length);
			kernel.SetMemoryArgument(9, extWallPX_buf);
			kernel.SetMemoryArgument(10, extWallPY_buf);
			if ((m_extWallPX[0] == float.MaxValue) && (m_extWallPX[0] == float.MaxValue))
				kernel.SetValueArgument(11, 0);
			else
				kernel.SetValueArgument(11, m_extWallPX.Length);
		}
		
		public float[,] RunWithoutReflection()
		{
			float[,] results = new float[m_pX.Length, 14];
 
			try
			{
				if (m_wallP1X.Length > 0)
				{
					if (m_wallP1X.Length <= 0)
						return null;

					long[] globalSize = new long[] { (long)Math.Ceiling(m_pX.Length / 256.0) * 256 };
					long[] localSize = new long[] { 256 };
						
					commands.Execute(kernel, null, globalSize, localSize, eventList);
					commands.Finish();

					float[] area = new float[m_pX.Length];
					commands.ReadFromBuffer(area_buf, ref area, false, eventList);

					commands.Finish();


					for (int i = 0; i < m_pX.Length; i++)
					{
						results[i, 0] = area[i];
					}
				}
			}
			
			catch (System.Exception)
			{
				results = null;            
			}


			return results;
		}
		

		#endregion
	}
}
