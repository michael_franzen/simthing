﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GPUIsovistField.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Cloo;

namespace CPlan.Isovist2D
{
	public class GPUIsovistFieldAcousticReflections
	{
		protected static GPUIsovistFieldAcousticReflections m_instance;

		float[] m_wallP1X;
		float[] m_wallP1Y;
		float[] m_wallP2X;
		float[] m_wallP2Y;
		float[] m_extWallPX;
		float[] m_extWallPY;
		float[] m_pX;
		float[] m_pY;
		float[] m_reflectionCoefficients;
		float m_precision;

		ComputeProgram program;
		ComputePlatform platform;
		IList<ComputeDevice> devices;
		ComputeContextPropertyList properties;
		ComputeContext context;

		ComputeBuffer<float> wallP1X_buf;
		ComputeBuffer<float> wallP1Y_buf;
		ComputeBuffer<float> wallP2X_buf;
		ComputeBuffer<float> wallP2Y_buf;
		ComputeBuffer<float> extWallPX_buf;
		ComputeBuffer<float> extWallPY_buf;
		ComputeBuffer<float> pX_buf;
		ComputeBuffer<float> pY_buf;
		ComputeBuffer<float> reflectionCoefficients_buf;
		ComputeBuffer<float> area_buf;
		ComputeBuffer<float> reflected_area_buf;
		ComputeBuffer<float> reflectionRatio_buf;
		ComputeBuffer<float> audiblePerimeter_buf;
		ComputeBuffer<float> audibleOcclusivity_buf;
		ComputeBuffer<float> audibleMinRadial_buf;
		ComputeBuffer<float> audibleMaxRadial_buf;
		ComputeBuffer<float> audibleMeanRadial_buf;
		ComputeBuffer<float> buffer_buf;
		ComputeKernel kernel;
		ComputeEventList eventList;
		ComputeCommandQueue commands;
		
		#region ComputationTextMultipleIsovistCalculation
		string clIsovistMultipleCalculation = @"
float2 LineIntersect(float L1P1X, float L1P1Y, float L1P2X, float L1P2Y,
float L2P1X, float L2P1Y,float L2P2X, float L2P2Y)
{
	float2 ptIntersection;
	ptIntersection.x = MAXFLOAT;
	ptIntersection.y = MAXFLOAT;
	
	float d = (L2P2Y - L2P1Y) * (L1P2X - L1P1X) - (L2P2X - L2P1X) * (L1P2Y - L1P1Y);
	float n_a = (L2P2X - L2P1X) * (L1P1Y - L2P1Y) - (L2P2Y - L2P1Y) * (L1P1X - L2P1X);
	float n_b = (L1P2X - L1P1X) * (L1P1Y - L2P1Y) - (L1P2Y - L1P1Y) * (L1P1X - L2P1X);

	if (d == 0)
		return ptIntersection;

	float ua = n_a / d;
	float ub = n_b / d;

	if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
	{
		ptIntersection.x = L1P1X + (ua * (L1P2X - L1P1X));
		ptIntersection.y = L1P1Y + (ua * (L1P2Y - L1P1Y));
		return ptIntersection;
	}
	return ptIntersection;
}

float2 reflectionRay(float2 wall, float2 ray) {
	float2 wallNormal1 = (float2) {-wall.y, wall.x};
	float2 wallNormal2 = (float2) {wall.y, -wall.x};
	float2 wallNormal;
	if (distance(wallNormal1, ray) <= distance(wallNormal2, ray)) {
		wallNormal = wallNormal1;
	}
	else {
		wallNormal = wallNormal2;
	}
	wallNormal /= length(wallNormal);
	return ray - 2.0f * dot(ray, wallNormal) * wallNormal;
}

kernel void Calculate(
	global  read_only float* wallP1X,
	global  read_only float* wallP1Y,
	global  read_only float* wallP2X,
	global  read_only float* wallP2Y,
	global  read_only float* pX,
	global  read_only float* pY,
	global read_only float* reflectionCoefficients,
	float exactness,
	int wallCount,
	global  read_only float* extWallPX,
	global  read_only float* extWallPY,
	int extWallPCount,
	global write_only float* area,
	global write_only float* buffer,
	global write_only float* reflected_area,
	global write_only float* reflectionRatio,
	global write_only float* perimeter,
	global write_only float* occlusivity,
	global write_only float* minRadial,
	global write_only float* maxRadial,
	global write_only float* meanRadial
)
{
		int gID0 = get_global_id(0);
	
		int rayCount = (int) (2*3.14159265359 / exactness);

		float2 curPerimeterPoint;
		float2 oldPerimeterPoint;
		float2 curPWeighted;
		float2 oldPWeighted;
		float2 firstPerimeterPoint;
		float2 rayP1 = (float2){ pX[gID0], pY[gID0] };
		float tmp_area = 0.0f;
		float tmp_reflected_area = 0.0f;
		float tmp_perimeter = 0.0f;
		float tmp_occlusivity = 0.0f;
		float tmp_minradial = MAXFLOAT;
		float tmp_maxradial = 0.0f;
		float tmp_meanradial = 0.0f;

		// coefficients
		float sound_startDecibel = 70.0f;
		float sound_attentuation = 14.244f;

		// numbers for random number generator
		unsigned int rx = gID0 + 632632;
		unsigned int ry = gID0 + 21523626;
		unsigned int rz = gID0 + 12315;
		unsigned int rw = gID0 + 51261;
		
		int curWall = 0;
		int prevWall = 0;
		for (int i = 0; i< rayCount; i++)
		{
			float angle = i*exactness;
			float l = 10000;
			float2 rayP2;        
			rayP2.x = rayP1.x + sin(angle) * l;
			rayP2.y = rayP1.y + cos(angle) * l;

			float2 cutPoint;
			float mindist = MAXFLOAT;
			bool cutted = false; 

			prevWall = curWall;
			for (int j = 0; j < wallCount; j++)
			{
				bool intersects = true;
				float2 iPoint = LineIntersect(rayP1.x, rayP1.y, rayP2.x, rayP2.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
				if ((iPoint.x == MAXFLOAT) || (iPoint.y == MAXFLOAT))
					intersects = false;

				if (intersects)
				{
					if (distance(rayP1, iPoint) < mindist)
					{
						cutPoint = iPoint;
						cutted = true;
						mindist = distance(rayP1, iPoint);
						curWall = j;
					}
				}
			}
			if (!cutted)
				cutPoint = rayP2;
			
			if (i == 0)
				firstPerimeterPoint = cutPoint;
			
			oldPerimeterPoint = curPerimeterPoint;
			curPerimeterPoint = cutPoint;
			float maxAudiblePerimeter = 0.0;
			float maxAudibleDist = 0.0;

			if (i != 0)
			{
				////////////////////////////////// sound area
				int monte_carlo_sample_size = 100;
				float weighted_area = 0.0f;
				for (int k = 0; k < monte_carlo_sample_size; k++) {
					// generate two random numbers from 0 to 1
					//t
					unsigned int rt = rx;
					rt ^= rt << 11;
					rt ^= rt >> 8;
					rx = ry; ry = rz; rz = rw;
					rw ^= rw >> 19;
					rw ^= rt;
					float t = (float)(rw / 4294967295.0);

					//beta
					rt = rx;
					rt ^= rt << 11;
					rt ^= rt >> 8;
					rx = ry; ry = rz; rz = rw;
					rw ^= rw >> 19;
					rw ^= rt;
					float beta = (float)(rw / 4294967295.0);

					float2 currentPoint = ((curPerimeterPoint-oldPerimeterPoint)*beta+oldPerimeterPoint-rayP1)*t + rayP1;
					float add_term = (sound_startDecibel-sound_attentuation*length(currentPoint - rayP1))/sound_startDecibel;
					if (add_term > 0) {
						weighted_area += add_term;
						float tmpAudibleDist = distance(rayP1, currentPoint);
						float tmpAudiblePerimeter = distance((curPerimeterPoint - rayP1)*t, (oldPerimeterPoint - rayP1)*t);
						if (tmpAudibleDist > maxAudibleDist) maxAudibleDist = tmpAudibleDist;
						if (tmpAudiblePerimeter > maxAudiblePerimeter) maxAudiblePerimeter = tmpAudiblePerimeter;
					}
				}
				float total_area = 0.5*distance(oldPerimeterPoint, rayP1)*distance(curPerimeterPoint, rayP1)*sin(exactness);
				tmp_area += weighted_area * total_area / monte_carlo_sample_size;


				///////////////////////////////// sound reflections
				float2 curWallStart = (float2) { wallP1X[curWall], wallP1Y[curWall] };
				float2 curWallEnd = (float2) { wallP2X[curWall], wallP2Y[curWall] };
				float2 prevWallStart = (float2) { wallP1X[prevWall], wallP1Y[prevWall] };
				float2 prevWallEnd = (float2) { wallP2X[prevWall], wallP2Y[prevWall] };
				
				float2 curWallDelta = curWallEnd - curWallStart;
				float2 prevWallDelta = prevWallEnd - prevWallStart;
				float2 curPerimeterDelta = curPerimeterPoint - rayP1;
				float2 oldPerimeterDelta = oldPerimeterPoint - rayP1;

				float2 oldReflectionRay = oldPerimeterPoint + reflectionRay(prevWallDelta, oldPerimeterDelta) * l;
				float2 curReflectionRay = curPerimeterPoint + reflectionRay(curWallDelta, curPerimeterDelta) * l;

				// perimter points for the reflection
				float2 oldReflectionPerimeterPoint;
				float2 curReflectionPerimeterPoint;
				
				// reflection walls
				int oldReflectionWall = -1;
				int curReflectionWall = -1;

				// compute intersection point for old reflection
				mindist = MAXFLOAT;
				for (int j = 0; j < wallCount; j++)
				{
					if (j == prevWall) continue;

					bool intersects = true;
					float2 iPoint = LineIntersect(oldPerimeterPoint.x, oldPerimeterPoint.y,  oldReflectionRay.x,  oldReflectionRay.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
					if ((iPoint.x == MAXFLOAT) || (iPoint.y == MAXFLOAT))
						intersects = false;
				
					if (intersects) {

						if (distance(oldPerimeterPoint, iPoint) < mindist) {
							oldReflectionPerimeterPoint = iPoint;
							mindist = distance(oldPerimeterPoint, iPoint);
							oldReflectionWall = j;
						}
					}
				}

				// compute intersection point for current reflection
				mindist = MAXFLOAT;
				for (int j = 0; j < wallCount; j++)
				{
					if (j == curWall) continue;

					bool intersects = true;
					float2 iPoint = LineIntersect(curPerimeterPoint.x, curPerimeterPoint.y,  curReflectionRay.x,  curReflectionRay.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
					if ((iPoint.x == MAXFLOAT) || (iPoint.y == MAXFLOAT))
						intersects = false;

					if (intersects) {
						if (distance(curPerimeterPoint, iPoint) < mindist) {
							curReflectionPerimeterPoint = iPoint;
							mindist = distance(curPerimeterPoint, iPoint);
							curReflectionWall = j;
						}
					}
				}

				if (oldReflectionWall == -1 || curReflectionWall == -1) continue;

				// reflection area
				monte_carlo_sample_size = 1000;
				weighted_area = 0.0f;
				for (int k = 0; k < monte_carlo_sample_size; k++) {
					// generate two random numbers from 0 to 1
					//t1
					unsigned int rt = rx;
					rt ^= rt << 11;
					rt ^= rt >> 8;
					rx = ry; ry = rz; rz = rw;
					rw ^= rw >> 19;
					rw ^= rt;
					float t1 = (float)(rw / 4294967295.0);

					//t2
					rt = rx;
					rt ^= rt << 11;
					rt ^= rt >> 8;
					rx = ry; ry = rz; rz = rw;
					rw ^= rw >> 19;
					rw ^= rt;
					float t2 = (float)(rw / 4294967295.0);

					float2 pp1 = (curPerimeterPoint - oldPerimeterPoint)*t1+oldPerimeterPoint;
					float2 pp2 = (curReflectionPerimeterPoint-oldReflectionPerimeterPoint)*t1+oldReflectionPerimeterPoint;
					float2 currentPoint = (pp2 - pp1)*t2 + pp1;
					
					float current_startDecibel = (sound_startDecibel - sound_attentuation*length(pp1-rayP1))*(reflectionCoefficients[oldReflectionWall]*t1 + reflectionCoefficients[curReflectionWall]*(1.0f-t1));
					float add_term = (current_startDecibel-sound_attentuation*length(currentPoint - pp1))/sound_startDecibel;
					if (add_term > 0) {
						weighted_area += add_term;
						float tmpAudibleDist = distance(rayP1, pp1) + distance(pp1, pp2);
						float tmpAudiblePerimeter = distance((curPerimeterPoint - curReflectionPerimeterPoint)*t2, (oldPerimeterPoint - oldReflectionPerimeterPoint)*t2);
						if (tmpAudibleDist > maxAudibleDist) maxAudibleDist = tmpAudibleDist;
						if (tmpAudiblePerimeter > maxAudiblePerimeter) maxAudiblePerimeter = tmpAudiblePerimeter;
					}
				}
				float2 diag1 = curReflectionPerimeterPoint - oldPerimeterPoint;
				float2 diag2 = oldReflectionPerimeterPoint - curPerimeterPoint;
				total_area = 0.5f*(diag1.x*diag2.y-diag1.y*diag2.x);
				if (total_area < 0) total_area *= -1;
				tmp_reflected_area += total_area * weighted_area / monte_carlo_sample_size;
				if (maxAudibleDist > tmp_maxradial) tmp_maxradial = maxAudibleDist;
				if (maxAudibleDist < tmp_minradial) tmp_minradial = maxAudibleDist;
				tmp_meanradial += maxAudibleDist;
				tmp_perimeter += maxAudiblePerimeter;
			}
		}
		
		area[gID0] = tmp_area;
		reflected_area[gID0] = tmp_reflected_area;
		reflectionRatio[gID0] = tmp_reflected_area / tmp_area;
		perimeter[gID0] = tmp_perimeter;
		occlusivity[gID0] = tmp_occlusivity;
		minRadial[gID0] = tmp_minradial;
		maxRadial[gID0] = tmp_maxradial;
		meanRadial[gID0] = tmp_meanradial;
}";
		#endregion

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		#region Constructor

		protected GPUIsovistFieldAcousticReflections()
		{
			InitializeGPUWithReflection();
		}

		~GPUIsovistFieldAcousticReflections()
		{
			m_instance = null;
		}

		

		#endregion

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		# region Methods

		static public GPUIsovistFieldAcousticReflections GetInstance()
		{
			if (m_instance == null)
				m_instance = new GPUIsovistFieldAcousticReflections();
			return m_instance;
		}
		
		public void CreateWithReflection(float[] reflectionCoefficients, float[] wallP1X, float[] wallP1Y, float[] wallP2X, float[] wallP2Y, float[] extWallPX, float[] extWallPY, float[] pX, float[] pY, float precision)
		{
			m_wallP1X = wallP1X;
			m_wallP1Y = wallP1Y;
			m_wallP2X = wallP2X;
			m_wallP2Y = wallP2Y;
			if (extWallPX.Length != 0)
			{
				m_extWallPX = extWallPX;
				m_extWallPY = extWallPY;
			}
			else
			{
				m_extWallPX = new float[1];
				m_extWallPX[0] = float.MaxValue;
				m_extWallPY = new float[1];
				m_extWallPY[0] = float.MaxValue;
			}
			m_precision = precision;
			m_pX = pX;
			m_pY = pY;
			m_reflectionCoefficients = reflectionCoefficients;

			SetParametersWithReflection();
		}

		
		private void InitializeGPUWithReflection()
		{
			try
			{
				//ComputeDevice dev = ComputePlatform.Platforms.
				if (ComputePlatform.Platforms.Count > 1)
				{
					platform = ComputePlatform.Platforms[ComputePlatform.Platforms.Count - 1];
					for (int p = 0; p < ComputePlatform.Platforms.Count; p++)
					{
						Console.WriteLine(ComputePlatform.Platforms[p].Name);
						Console.WriteLine(ComputePlatform.Platforms[p].Vendor);
						Console.WriteLine(ComputePlatform.Platforms[p].Profile);
						Console.WriteLine(ComputePlatform.Platforms[p].Version);
						Console.WriteLine(ComputePlatform.Platforms[p].Devices.Select(d => d.Name + d.Available).Aggregate((a, b) => a + " " + b));
						string vers = ComputePlatform.Platforms[p].Version;
						if (vers.Contains("CUDA")) platform = ComputePlatform.Platforms[p];
					}
				}
				else platform = ComputePlatform.Platforms[0];

				devices = new List<ComputeDevice>();
				devices.Add(platform.Devices[0]);
				properties = new ComputeContextPropertyList(platform);
				context = new ComputeContext(devices, properties, null, IntPtr.Zero);
				eventList = new ComputeEventList();
				commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);

				program = new ComputeProgram(context, clIsovistMultipleCalculation);

				program.Build(null, null, null, IntPtr.Zero);
			}
			catch (System.Exception ex)
			{
				StringBuilder output = new StringBuilder();
				StringWriter log = new StringWriter(output);
				log.WriteLine(ex.ToString());
				log.WriteLine(" ");
				if (program != null)
					log.WriteLine(program.GetBuildLog(context.Devices[0]));
				
				//MyTrace.Trace(String.Format(ex.ToString()));
				//MyTrace.Trace(program.GetBuildLog(context.Devices[0]));
			}
			try
			{
				kernel = program.CreateKernel("Calculate");
			}
			catch (System.Exception ex)
			{
				StringBuilder output = new StringBuilder();
				StringWriter log = new StringWriter(output);
				log.WriteLine(ex.ToString());
				log.WriteLine(" ");
				if (program != null)
					log.WriteLine(program.GetBuildLog(context.Devices[0]));
				
				//MyTrace.Trace(String.Format(ex.ToString()));
				//MyTrace.Trace(program.GetBuildLog(context.Devices[0]));
			}

		}

		public void SetParametersWithReflection()
		{
			int cnt = m_pY.Length*2;
			wallP1X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP1X);
			wallP1Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP1Y);
			wallP2X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP2X);
			wallP2Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP2Y);
			extWallPX_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_extWallPX);
			extWallPY_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_extWallPY);
			reflectionCoefficients_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_reflectionCoefficients);
			pX_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_pX);
			pY_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_pY);
			area_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			buffer_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			reflected_area_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			reflectionRatio_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			audiblePerimeter_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			audibleOcclusivity_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			audibleMinRadial_buf = new ComputeBuffer< float > (context, ComputeMemoryFlags.WriteOnly, cnt);
			audibleMaxRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
			audibleMeanRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);

		//Übergabe der Parameter:
			kernel.SetMemoryArgument(0, wallP1X_buf);
			kernel.SetMemoryArgument(1, wallP1Y_buf);
			kernel.SetMemoryArgument(2, wallP2X_buf);
			kernel.SetMemoryArgument(3, wallP2Y_buf);

			kernel.SetMemoryArgument(4, pX_buf);
			kernel.SetMemoryArgument(5, pY_buf);

			kernel.SetMemoryArgument(6, reflectionCoefficients_buf);

			kernel.SetValueArgument(7, m_precision);
			kernel.SetValueArgument(8, m_wallP1X.Length);
			kernel.SetMemoryArgument(9, extWallPX_buf);
			kernel.SetMemoryArgument(10, extWallPY_buf);
			if ((m_extWallPX[0] == float.MaxValue) && (m_extWallPX[0] == float.MaxValue))
				kernel.SetValueArgument(11, 0);
			else
				kernel.SetValueArgument(11, m_extWallPX.Length);

			kernel.SetMemoryArgument(12, area_buf);
			kernel.SetMemoryArgument(13, buffer_buf);
			kernel.SetMemoryArgument(14, reflected_area_buf);
			kernel.SetMemoryArgument(15, reflectionRatio_buf);
			kernel.SetMemoryArgument(16, audiblePerimeter_buf);
			kernel.SetMemoryArgument(17, audibleOcclusivity_buf);
			kernel.SetMemoryArgument(18, audibleMinRadial_buf);
			kernel.SetMemoryArgument(19, audibleMaxRadial_buf);
			kernel.SetMemoryArgument(20, audibleMeanRadial_buf);
		}

		public float[,] RunWithReflection()
		{
			float[,] results = new float[m_pX.Length, 8];

			try
			{
				if (m_wallP1X.Length > 0)
				{
					if (m_wallP1X.Length <= 0)
						return null;

					long[] globalSize = new long[] { (long)Math.Ceiling(m_pX.Length / 256.0) * 256 };
					long[] localSize = new long[] { 256 };

					commands.Execute(kernel, null, globalSize, localSize, eventList);
					commands.Finish();

					float[] area = new float[2*m_pX.Length];
					float[] reflected_area = new float[2 * m_pX.Length];
					float[] reflectionRatio = new float[2 * m_pX.Length];
					float[] audiblePerimeter = new float[2 * m_pX.Length];
					float[] audibleOcclusivity = new float[2 * m_pX.Length];
					float[] audibleMinRadial = new float[2 * m_pX.Length];
					float[] audibleMaxRadial = new float[2 * m_pX.Length];
					float[] audibleMeanRadial = new float[2 * m_pX.Length];

					commands.ReadFromBuffer(area_buf, ref area, false, eventList);
					commands.ReadFromBuffer(reflected_area_buf, ref reflected_area, false, eventList);
					commands.ReadFromBuffer(reflectionRatio_buf, ref reflectionRatio, false, eventList);
					commands.ReadFromBuffer(audiblePerimeter_buf, ref audiblePerimeter, false, eventList);
					commands.ReadFromBuffer(audibleOcclusivity_buf, ref audibleOcclusivity, false, eventList);
					commands.ReadFromBuffer(audibleMinRadial_buf, ref audibleMinRadial, false, eventList);
					commands.ReadFromBuffer(audibleMaxRadial_buf, ref audibleMaxRadial, false, eventList);
					commands.ReadFromBuffer(audibleMeanRadial_buf, ref audibleMeanRadial, false, eventList);
					commands.Finish();

					for (int i = 0; i < m_pX.Length; i++)
					{
						results[i, 0] = area[i];
						results[i, 1] = reflected_area[i];
						results[i, 2] = reflectionRatio[i];
						results[i, 3] = audiblePerimeter[i];
						results[i, 4] = audibleOcclusivity[i];
						results[i, 5] = audibleMinRadial[i];
						results[i, 6] = audibleMaxRadial[i];
						results[i, 7] = audibleMeanRadial[i];
					}
				}
			}

			catch (System.Exception)
			{
				results = null;
			}
			
			return results;
		}


		#endregion
	}
}
