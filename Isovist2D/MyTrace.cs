﻿using System;

namespace CPlan.Isovist2D
{
    public static class MyTrace
    {
        static System.Diagnostics.TraceSwitch dommcTrace = new System.Diagnostics.TraceSwitch("DOMMC Diagnostic", "DOMMC diagnostic switch");

        public static void Trace(String myMessage)
        {
            System.Diagnostics.Trace.WriteLine(myMessage);
        }
    }
}
