﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = AnalysisPoint.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using OpenTK;

namespace CPlan.Isovist2D
{
    [Serializable]
    public class AnalysisPoint
    {
        public double X, Y;
        Dictionary<AnalysisMeasure, float> m_analysisResults = new Dictionary<AnalysisMeasure, float>();

        public AnalysisPoint(Vector2d point)
        {
            X = point.X;
            Y = point.Y;
        }

        public Dictionary<AnalysisMeasure, float> Results
        {
            get { return m_analysisResults; }
            set { m_analysisResults = value; }
        }
    }
}
