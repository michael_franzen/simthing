﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace LibThermal
{
    public class ThermalSimulation
    {
        private Matrix<double> coefficients;
        private Vector<double> startValues;
        private bool[] isConstant;

        private Matrix cExp;
        private double[,] temperatures;
        private double[,] totalInflux;
        private double[,] innerInflux;
        private double[,] totalOutflow;
        private double[,] innerOutflow;
        private double[,] totalDelta;
        private double[,] innerDelta;


        public ThermalSimulation(int[][] neighbors, double[] conductivity, double[] startTemperature, bool[] isConstant, double[] scalingFactor)
        {
            this.isConstant = isConstant;
            double[] startValues = startTemperature;
            double[,] coefficients = new double[startTemperature.Length, startTemperature.Length];
            for (int i = 0; i < startTemperature.Length; i++)
            {
                if (isConstant != null && isConstant[i]) continue;

                double total = 0.0;
                for (int j = 0; j < neighbors[i].Length; j++)
                {
                    if (i == j) continue;
                    coefficients[i, neighbors[i][j]] = conductivity[neighbors[i][j]] * scalingFactor[i] * scalingFactor[j];
                    total += coefficients[i, neighbors[i][j]];
                }
                coefficients[i, i] = -total;
            }
            this.coefficients = DenseMatrix.OfArray(coefficients);
            this.startValues = DenseVector.OfArray(startValues);
            this.cExp = (Matrix)this.expm(this.coefficients);
        }

        public void SimulateUntil(int maxtime)
        {
            this.Temperatures = new double[maxtime, this.startValues.Count];
            this.TotalInflux = new double[maxtime, this.startValues.Count];
            this.InnerInflux = new double[maxtime, this.startValues.Count];
            this.TotalOutflow = new double[maxtime, this.startValues.Count];
            this.InnerOutflow = new double[maxtime, this.startValues.Count];
            this.TotalDelta = new double[maxtime, this.startValues.Count];
            this.InnerDelta = new double[maxtime, this.startValues.Count];
            Vector c = (Vector)this.startValues;
            for (int i = 0; i < maxtime; i++)
            {
                Vector ci = (Vector)this.cExp.Multiply(c);
                for (int j = 0; j < this.startValues.Count; j++)
                {
                    this.Temperatures[i, j] = ci[j];
                    this.TotalInflux[i, j] = 0.0;
                    this.TotalOutflow[i, j] = 0.0;

                    for (int k = 0; k < this.startValues.Count; k++)
                    {
                        if (j != k) this.TotalInflux[i, j] += this.cExp[j, k] * c[k];
                        if (j != k && !this.isConstant[k]) this.InnerInflux[i, j] += this.cExp[j, k] * c[k];
                        if (j != k) this.TotalOutflow[i, j] += this.cExp[j, k] * c[j];
                        if (j != k && !this.isConstant[k]) this.InnerOutflow[i, j] += this.cExp[j, k] * c[j];
                    }
                    this.TotalDelta[i, j] = this.TotalInflux[i, j] - this.TotalOutflow[i, j];
                    this.InnerDelta[i, j] = this.InnerInflux[i, j] - this.InnerOutflow[i, j];
                }
                c = ci;
            }
        }

        public double[,] Temperatures
        {
            get
            {
                return temperatures;
            }

            set
            {
                temperatures = value;
            }
        }

        public double[,] InnerInflux
        {
            get
            {
                return innerInflux;
            }

            set
            {
                innerInflux = value;
            }
        }

        public double[,] TotalInflux
        {
            get
            {
                return totalInflux;
            }

            set
            {
                totalInflux = value;
            }
        }

        public double[,] TotalOutflow
        {
            get
            {
                return totalOutflow;
            }

            set
            {
                totalOutflow = value;
            }
        }

        public double[,] InnerOutflow
        {
            get
            {
                return innerOutflow;
            }

            set
            {
                innerOutflow = value;
            }
        }

        public double[,] TotalDelta
        {
            get
            {
                return totalDelta;
            }

            set
            {
                totalDelta = value;
            }
        }

        public double[,] InnerDelta
        {
            get
            {
                return innerDelta;
            }

            set
            {
                innerDelta = value;
            }
        }


        // Computes the matrix exponential using the 13/13 Badé approximant
        // see "The Scaling and Squaring Method for the Matrix Exponential Revisited", Nicholas J. Higham (2006)
        // improvement possible: "A New Scaling and Squaring Algorithm for the Matrix Exponential", Awad H. Al-Mohy and Nicholas J. Higham (2009)
        private Matrix<double> expm(Matrix<double> A)
        {
            double[] B = new double[] { 64764752532480000, 32382376266240000, 7771770303897600, 31187353796428800, 129060195264000, 10559470521600, 4670442572800, 33522128640, 1323241920, 540840800, 960960, 16380, 182, 1 };

            int s = (int)Math.Ceiling(Math.Log(A.L1Norm() / 5.4e0) / Math.Log(2.0));
            if (s < 0) s = 0;

            Matrix<double> I = MathNet.Numerics.LinearAlgebra.Double.DenseMatrix.CreateIdentity(A.RowCount);
            A = A / Math.Pow(2, s);
            Matrix<double> A2 = A.Multiply(A);
            Matrix<double> A4 = A2.Multiply(A2);
            Matrix<double> A6 = A4.Multiply(A2);

            Matrix<double> U1 = A6.Multiply(A6 * B[13] + A4 * B[11] + A2 * B[9]);
            MathNet.Numerics.LinearAlgebra.Double.Matrix U = (MathNet.Numerics.LinearAlgebra.Double.Matrix)A.Multiply(U1 + A6 * B[7] + A4 * B[5] + A2 * B[3] + I * B[1]);

            Matrix<double> V1 = A6.Multiply(A6 * B[12] + A4 * B[10] + A2 * B[8]);
            MathNet.Numerics.LinearAlgebra.Double.Matrix V = (MathNet.Numerics.LinearAlgebra.Double.Matrix)(V1 + A6 * B[6] + A4 * B[4] + A2 * B[2] + I * B[0]);
            
            Matrix<double> R = (V - U).Solve(V + U); 

            for (int i = 0; i < s; i++)
                R = R.Multiply(R);
            return R;
        }
    }
}
