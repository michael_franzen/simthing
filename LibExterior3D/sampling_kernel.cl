﻿kernel void downsample(global int *output, global uchar *input, int patchsize, int numCategories) {
	int a1 = get_local_id(0) * patchsize;
	int a2 = a1 + patchsize;
	
	int tmp[256];

	for (int i = 0; i < numCategories; i++) {
		tmp[i] = 0;
	}

	for (int i = a1; i < a2; i++) {
		tmp[input[3*i]]++;
	}

	for (int i = 0; i < numCategories; i++) {
		atomic_add(&output[i], tmp[i]);
	}
}