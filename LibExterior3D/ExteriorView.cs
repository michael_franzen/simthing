﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using LibSimthing;
using System.Drawing;
using OpenTK.Input;
using System.Diagnostics;
using Cloo;
using System.IO;

namespace LibExterior3D
{
    public class ExteriorView
    {
        private int frameWidth;
        private int frameHeight;
        private List<List<OpenTK.Vector3d>> triangles;
        private List<List<int>> categories;
        
        public ExteriorView(List<List<OpenTK.Vector3d>> triangles, List<List<int>> categories, int frameWidth = 64, int frameHeight = 64)
        {
            if (frameWidth < 32 || frameHeight < 32)
                throw new ArgumentOutOfRangeException("frame must be at least 32x32 pixels.");

            // set geoemtry and categories
            this.triangles = triangles;
            this.categories = categories;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            
            this.glInit();
            this.clInit();
        }

        #region OpenGL

        // window
        private OpenTK.GameWindow glWindow;

        // buffers
        int[] glBufferIds = new int[3];
        float[] glVertexArrayData;
        byte[] glVertexColorArrayData;
        uint[] glVertexElementArrayData;

        // shader program
        int glProframId;

        private void glInit()
        {
            // set up context
            this.glWindow = new OpenTK.GameWindow(
                this.frameWidth,
                this.frameHeight,
                GraphicsMode.Default,
                "ExteriorView",
                OpenTK.GameWindowFlags.Default,
                OpenTK.DisplayDevice.Default,
                4,
                3,
                GraphicsContextFlags.Default);

            // create buffer for vertices, indices & colors
            this.glVertexArrayData = new float[this.triangles.Count * 9];
            this.glVertexColorArrayData = new byte[this.triangles.Count * 9];
            this.glVertexElementArrayData = new uint[this.triangles.Count * 9];

            for (uint k = 0; k < this.triangles.Count; k++)
            {
                for (uint i = 0; i < 3; i++)
                {
                    // vertex
                    OpenTK.Vector3d corner = triangles[Convert.ToInt32(k)][Convert.ToInt32(i)];
                    int category = categories[Convert.ToInt32(k)][Convert.ToInt32(i)];

                    glVertexArrayData[9 * k + 3 * i] = (float)corner.X;
                    glVertexArrayData[9 * k + 3 * i + 1] = (float)corner.Z;
                    glVertexArrayData[9 * k + 3 * i + 2] = (float)corner.Y;

                    // color
                    glVertexColorArrayData[9 * k + 3 * i] = (byte)(category % 256);
                    glVertexColorArrayData[9 * k + 3 * i + 1] = (byte)(category % 256);
                    glVertexColorArrayData[9 * k + 3 * i + 2] = (byte)(category % 256);

                    // index
                    glVertexElementArrayData[3 * k + i] = 3 * k + i;
                }

            }
            
            // create vertex, color & index buffer
            GL.GenBuffers(3, this.glBufferIds);

            GL.BindBuffer(BufferTarget.ArrayBuffer, this.glBufferIds[0]);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(this.glVertexArrayData.Length * sizeof(float)), this.glVertexArrayData, BufferUsageHint.StaticDraw);
            
            GL.BindBuffer(BufferTarget.ArrayBuffer, this.glBufferIds[1]);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(this.glVertexColorArrayData.Length * sizeof(byte)), this.glVertexColorArrayData, BufferUsageHint.StaticDraw);
            
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, this.glBufferIds[2]);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(this.glVertexElementArrayData.Length * sizeof(uint)), this.glVertexElementArrayData, BufferUsageHint.StaticDraw);
            
            // load shaders
            int glVertexShaderId = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(glVertexShaderId, String.Join("\n", ExteriorView.glVertexShaderSource));
            GL.CompileShader(glVertexShaderId);

            int glFragmentShaderId = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(glFragmentShaderId, String.Join("\n", ExteriorView.glFragmentShaderSource));
            GL.CompileShader(glFragmentShaderId);

            // create opengl program
            this.glProframId = GL.CreateProgram();
            GL.AttachShader(this.glProframId, glVertexShaderId);
            GL.AttachShader(this.glProframId, glFragmentShaderId);
            GL.LinkProgram(this.glProframId);
        }

        private static string[] glVertexShaderSource = new string[]
        {
            @"#version 330 core",
            @"layout(location = 0) in vec3 vertexPosition;",
            @"layout(location = 1) in vec3 vertexColor;",
            @"out vec3 fragmentColor;",
            @"uniform mat4 projection_matrix;",
            @"uniform mat4 view_matrix;",
            @"void main(void) {",
            @"fragmentColor = vertexColor;",
            @"gl_Position = projection_matrix * view_matrix * vec4(vertexPosition, 1);",
            @"}"
        };

        private static string[] glFragmentShaderSource = new string[]
        {
            @"#version 330 core",
            @"out vec3 color;",
            @"in vec3 fragmentColor;",
            @"void main(void) {",
            @"color = fragmentColor / 255.0;",
            @"}"
        };
        
        public Stopwatch glStopwatch = new Stopwatch();

        int frame = 0;

        public void glRender(OpenTK.Vector3d from, List<OpenTK.Vector3d> viewport)
        {
            glStopwatch.Start();

            // compute stuff for projection / view matrices
            //OpenTK.Vector3d windowHeightVector = new OpenTK.Vector3d(0, 0, (viewport[2] - viewport[1]).Length);
            OpenTK.Vector3d windowHeightVector = new OpenTK.Vector3d(0, 0, from.Z);
            OpenTK.Vector3d to = viewport[0] + 0.5 * (viewport[1] - viewport[0]) + windowHeightVector;
            double fov = OpenTK.Vector3d.CalculateAngle(viewport[0] - from, viewport[2] - from);
            double aspectratio = (viewport[1] - viewport[0]).Length / windowHeightVector.Length;

            // clear graphics & activate shader program
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.UseProgram(glProframId);
            GL.Enable(EnableCap.DepthTest);

            // construct projection & view matrix
            OpenTK.Matrix4 projection = OpenTK.Matrix4.CreatePerspectiveFieldOfView((float)fov, (float)aspectratio, 0.00001f, 1000f);
            int projectionMatrixLocation = GL.GetUniformLocation(glProframId, "projection_matrix");
            GL.UniformMatrix4(projectionMatrixLocation, false, ref projection);
            
            OpenTK.Matrix4 view = OpenTK.Matrix4.LookAt(
                new OpenTK.Vector3((float)from.X, (float)from.Z, (float)from.Y),
                new OpenTK.Vector3((float)to.X, (float)to.Z, (float)to.Y),
                OpenTK.Vector3.UnitY);
            int viewMatrixLocation = GL.GetUniformLocation(glProframId, "view_matrix");
            GL.UniformMatrix4(viewMatrixLocation, false, ref view);
            
            // bind buffers
            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, glBufferIds[0]); // vertex
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
            
            GL.EnableVertexAttribArray(1);
            GL.BindBuffer(BufferTarget.ArrayBuffer, glBufferIds[1]); // color
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.UnsignedByte, false, 0, 0);
            
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, glBufferIds[2]); // index

            // draw elements
            GL.DrawElements(BeginMode.Triangles, glVertexElementArrayData.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);

            //Bitmap bmp = new Bitmap(this.frameWidth, this.frameHeight);
            //System.Drawing.Imaging.BitmapData data =
            //    bmp.LockBits(new Rectangle(0, 0, this.frameWidth, this.frameHeight), System.Drawing.Imaging.ImageLockMode.WriteOnly,
            //                 System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //GL.ReadPixels(0, 0, this.frameWidth, this.frameHeight, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0);
            //bmp.UnlockBits(data);
            //bmp.Save(String.Format(@"C:\Users\mfranzen\frames\{0}.bmp", this.frame));
            //this.frame++;

            glStopwatch.Stop();
        }

        #endregion

        #region OpenCL

        private static string clKernelSource = Path.GetFullPath(
            Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName + "\\sampling_kernel.cl");

        private ComputePlatform clPlatform;
        private ComputeContext clContext;
        private ComputeCommandQueue clQueue;
        private ComputeProgram clProgram;
        private ComputeKernel clKernel;
        long clWorkGroupSize;

        public Stopwatch clStopwatchGather = new Stopwatch();
        public Stopwatch clStopwatchKernel = new Stopwatch();

        private void clInit()
        {
            // get platform
            clPlatform = ComputePlatform.Platforms[0];

            // create context
            clContext = new ComputeContext(ComputeDeviceTypes.Gpu,
                new ComputeContextPropertyList(clPlatform), null, IntPtr.Zero);

            // get device
            clQueue = new ComputeCommandQueue(clContext,
                clContext.Devices[0], ComputeCommandQueueFlags.None);
            
            // read kernel
            StreamReader streamReader = new StreamReader(ExteriorView.clKernelSource);
            string clSource = streamReader.ReadToEnd();
            streamReader.Close();

            // create program
            clProgram = new ComputeProgram(clContext, clSource);
            clProgram.Build(null, null, null, IntPtr.Zero);

            // create kernel
            clKernel = clProgram.CreateKernel("downsample");

            // set work group size
            clWorkGroupSize = Math.Min((this.frameWidth * this.frameHeight)/28, clKernel.GetWorkGroupSize(clContext.Devices[0]));
        }

        public int[] clSample(int numCategories)
        {

            // get data from OpenGL
            byte[] input = new byte[this.frameWidth * this.frameHeight * 3];
            GL.ReadPixels(0, 0, this.frameWidth, this.frameHeight, PixelFormat.Rgb, PixelType.UnsignedByte, input);
            
            int[] output = new int[numCategories];
            ComputeBuffer<int> clOutput = new ComputeBuffer<int>(clContext, ComputeMemoryFlags.UseHostPointer, output);
            ComputeBuffer<byte> clInput = new ComputeBuffer<byte>(clContext, ComputeMemoryFlags.UseHostPointer, input);

            clKernel.SetMemoryArgument(0, clOutput);
            clKernel.SetMemoryArgument(1, clInput);
            clKernel.SetValueArgument(2, frameWidth * frameHeight / clWorkGroupSize);
            clKernel.SetValueArgument(3, numCategories);
            clStopwatchGather.Stop();
            
            clQueue.Execute(clKernel,
                new long[] { 0 }, 
                new long[] { clWorkGroupSize },
                new long[] { clWorkGroupSize },
                null);
            
            clQueue.Finish();
            clStopwatchKernel.Stop();
            
            clOutput.Dispose();
            clInput.Dispose();

            return output;

            //IntPtr glHandle = (GraphicsContext.CurrentContext as IGraphicsContextInternal).Context.Handle;
            //IntPtr wglHandle = wglGetCurrentDC();
            //ComputePlatform platform = ComputePlatform.GetByName("NVIDIA CUDA");
            //ComputeContextProperty p1 = new ComputeContextProperty(ComputeContextPropertyName.Platform, (IntPtr)platform.Handle);
            //ComputeContextProperty p2 = new ComputeContextProperty(ComputeContextPropertyName.CL_GL_CONTEXT_KHR, glHandle);
            //ComputeContextProperty p3 = new ComputeContextProperty(ComputeContextPropertyName.CL_WGL_HDC_KHR, wglHandle);
            //ComputeContextPropertyList cpl = new ComputeContextPropertyList(new ComputeContextProperty[] { p1, p2, p3 });
            //ComputeContext context = new ComputeContext(ComputeDeviceTypes.Gpu, cpl, null, IntPtr.Zero);

            ////hint: creating a ComputeBuffer
            //ComputeBuffer<float> clBuffer = ComputeBuffer<float>.CreateFromGLBuffer<float>(context, ComputeMemoryFlags.ReadOnly, openGLBufferIds[2]);
        }

        #endregion
    }
}