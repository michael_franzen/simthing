﻿using LibSimthing;
using Rhino.Geometry;
using Rhino.Geometry.Intersect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSWBS
{
    public class SWBS
    {
        private Apartment apartment;
        private List<Room> rooms = new List<Room>();
        private List<PolylineCurve> roomFootprints = new List<PolylineCurve>();
        private List<Room.Functionality> roomFunctionalities = new List<Room.Functionality>();
        private List<double> roomAreas = new List<double>();

        // Personenhaushalt: 0 = Studio, 8 = 8-Personen-Haushalt
        private int phh = 0;
        // Area in sqm
        private double sia416area = 0;

        public SWBS(Apartment apartment)
        {
            this.Apartment = apartment;
            this.rooms = apartment.Rooms.ToList();
            foreach (Room r in rooms)
            {
                this.roomFootprints.Add(r.Footprint());
                this.roomFunctionalities.Add(r.Function);

                // compute area
                double roomArea = 0d;
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(this.roomFootprints.Last());
                if (breps != null) foreach (Brep brep in breps) roomArea += brep.GetArea();
                this.roomAreas.Add(roomArea);

                // total area does not include outdoor areas
                if (!(this.roomFunctionalities.Last() == Room.Functionality.Outdoor))
                    this.sia416area += roomArea;
            }

            // compute phh
            this.phh = Math.Min(8, (int)(Math.Max(0, (this.sia416area - 30) / 10)));
        }


        public double B1()
        {
            // 1 point for these values (phh -> min)
            int[] min = new int[]
            {
                30,
                40,
                50,
                60,
                70,
                80,
                90,
                100,
                110
            };

            // 3 points for these values (phh -> max)
            int[] max = new int[]
            {
                36,
                46,
                58,
                70,
                82,
                96,
                108,
                120,
                132
            };

            return Math.Max(0, Math.Min(3, Math.Ceiling((this.sia416area - min[this.phh]) / (max[this.phh] - min[this.phh]) * 3)));
        }

        // TODO: Include kitchens with more than 8m^2 more space
        // TODO: Include roof configurations
        public double B2()
        {
            int roomcount = 0;
            for (int i = 0; i < this.rooms.Count; i++)
            {
                if (this.roomFunctionalities[i] == Room.Functionality.Outdoor) continue;
                if (this.roomFunctionalities[i] == Room.Functionality.Kitchen) continue;
                if (this.roomAreas[i] < 8) continue;
                roomcount++;
            }
            
            // 1 point for these values (phh -> min)
            int[] min = new int[]
            {
                0,
                1,
                1,
                2,
                2,
                3,
                3,
                4,
                4
            };

            // 3 points for these values (phh -> max)
            int[] max = new int[]
            {
                1,
                2,
                2,
                3,
                4,
                5,
                6,
                7,
                8
            };

            return Math.Max(0, Math.Min(3, Math.Ceiling(((double)roomcount - min[this.phh]) / (max[this.phh] - min[this.phh]) * 3)));
        }

        // TODO: Need fitting
		// TODO: Was sind Aufenthaltsräume?
        public double B3()
        {
            return -1d;
        }

        // TODO: Need fitting
        public double B4()
        {
            return -1d;
        }

        //TODO: Include cardinal direction (north is bad, -1 point)
        //TODO (machbar): Include window height (high is bad, -1/2 point)
        public double B5()
        {
            List<double> directions = new List<double>();
            for (int i = 0; i < this.rooms.Count; i++)
            {
                if (this.roomFunctionalities[i] == Room.Functionality.Outdoor) continue;
                if (this.roomFunctionalities[i] == Room.Functionality.Kitchen) continue;
                if (this.roomFunctionalities[i] == Room.Functionality.Bathroom) continue;

                foreach (Window w in this.rooms[i].Windows)
                    directions.Add(w.Origin.Angle);
            }
            directions.Sort();
            
            int view90 = 0;
            int view180 = 0;
            for (int i = 1; i < directions.Count; i++) {
                double angle = (directions[i] - directions[0]);
                if (angle > 89.5 || angle < -89.5) view90++;
                if (angle > 179.5 || angle < 179.5) view180++;
            }

            double b5 = 0d;

            if (directions.Count > 0)
            {
                if (this.phh <= 4)
                {
                    if (view90 > 0 && view180 > 0) b5 = 3;
                    else if (view180 > 0) b5 = 2.5;
                    else if (view90 > 0) b5 = 2;
                    else b5 = 1;

                }
                else
                {
                    if (view90 > 0 && view180 > 0) b5 = 3;
                    else if (view180 > 0) b5 = 2;
                    else if (view90 > 0) b5 = 1.5;
                    else b5 = 1;
                }
            }

            return b5;
        }

        //TODO: Need Essbereich
        public double B6()
        {
            return -1d;
        }

        //TODO: Need Essbereich
        public double B7()
        {
            return -1d;
        }

        //TODO: Need Essbereich
        public double B8()
        {
            return -1d;
        }

        //TODO: Need Essbereich
        public double B9()
        {
            Room kitchen = this.rooms.Find(x => (x.Function == Room.Functionality.Kitchen));
            if (kitchen == null) return 0d;

            double b9 = 0d;

            // THIS MAY BE BUGGY!
            IEnumerable<Point3d> kitchenInterior = kitchen.Interiors.FindAll(x => x.Category == Interior.InteriorCategory.kitchen).Select(x => x.Position);
            BoundingBox bbox = new BoundingBox(kitchenInterior);

            bool windowNearKitchen = false;
            bool windowGood = false;
            foreach (Window w in kitchen.Windows)
            {
                foreach (Line l in w.Lines2D())
                {
                    foreach (Line l2 in bbox.GetEdges())
                    {
                        double a, b;
                        Intersection.LineLine(l, l2, out a, out b);
                        if (l.PointAt(a).DistanceTo(l2.PointAt(b)) <= 3)
                        {
                            windowNearKitchen = true;
                            if (w.Y <= 1.4) windowGood = true;
                        }
                    }
                }
            }

            bool kitchenHasBalcony = false;
            foreach (Door d in kitchen.Doors)
            {
                if (this.apartment.BalconyDoors.Contains(d)) kitchenHasBalcony = true;
            }

            if (windowNearKitchen && windowGood) b9 = 3d;
            else if (windowNearKitchen && kitchenHasBalcony) b9 = 2d;
            else if (windowNearKitchen) b9 = 1d;

            return b9;
        }

        //TODO: Need bath interior types (shower/bathtub/...)
        public double B10()
        {
            return -1d;
        }

        //TODO: Need window opening
        public double B11()
        {
            List<Room> bathrooms = this.rooms.FindAll(x => (x.Function == Room.Functionality.Bathroom));
            if (bathrooms == null) return 0d;

            double b11 = 0d;

            
            foreach (Room bathroom in bathrooms)
            {
                double b11tmp = 0d;

                // TODO
                bool hasLock = true;
                bool windowInBathroom = bathroom.Windows.Count > 0;
                bool windowGood = bathroom.Windows.FindAll(x => x.Y <= 1.4).Count > 0;

                if (windowInBathroom && windowGood) b11tmp = 3d;
                else if (windowInBathroom) b11tmp = 2d;
                else if (hasLock) b11tmp = 1d;

                b11 += b11tmp;
            }

            return b11 / bathrooms.Count;
        }

        // TODO: Need fitting
        public double B12()
        {
            return -1d;
        }

        // TODO: Raumteilung
        public double B13()
        {
            return -1d;
        }

        // TODO: Raumteilung
        public double B14()
        {
            return -1d;
        }

        // TODO: Raumteilung
        public double B15()
        {
            return -1d;
        }

        // TODO: Need cardinal direction
        public double B16()
        {
            double balconiesArea = 0d;
            for(int i = 0; i < this.rooms.Count; i++)
                if (this.rooms[i].Function == Room.Functionality.Outdoor) balconiesArea += this.roomAreas[i];

            // 1 point for these values (phh -> min)
            int[] min = new int[]
            {
                3,
                3,
                3,
                3,
                4,
                5,
                6,
                7,
                8
            };

            // 3 points for these values (phh -> max)
            int[] max = new int[]
            {
                5,
                5,
                6,
                9,
                12,
                15,
                18,
                21,
                24
            };

            return Math.Max(0, Math.Min(3, Math.Ceiling((balconiesArea - min[this.phh]) / (max[this.phh] - min[this.phh]) * 3)));
        }

        public double TotalValue()
        {
            Func<double>[] elements = new Func<double>[]
            {
                B1,
                B2,
                B3,
                B4,
                B5,
                B6,
                B7,
                B8,
                B9,
                B10,
                B11,
                B12,
                B13,
                B14,
                B15,
                B16
            };

            double[] weights = new double[]
            {
                3,
                3,
                3,
                3,
                2,
                2,
                2,
                2,
                1,
                1,
                1,
                4,
                2,
                2,
                2,
                3
            };

            double total = 0d;
            for (int i = 0; i < elements.Length; i++)
                if (elements[i]() > 0)
                    total += weights[i] * elements[i]();

            return total;
        }

        #region properties

        public Apartment Apartment
        {
            get
            {
                return apartment;
            }

            set
            {
                apartment = value;
            }
        }

        #endregion
    }
}
