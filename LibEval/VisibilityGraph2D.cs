﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CPlan.Geometry;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

using OpenTK.Graphics.OpenGL;
using Helper;
using OpenTK;
using Cloo;
using Cloo.Bindings;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Diagnostics;

namespace VisibilityAnalysisModel
{
    [Serializable]
    public class VisibilityGraph2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        HashSet<Node> m_nodes;
        HashSet<Edge> m_edges;
        List<Line2D> m_obstacleLines;

        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public HashSet<Node> Nodes
        {
            get { return m_nodes; }
            set { m_nodes = value; }
        }

        public HashSet<Edge> Edges
        {
            get { return m_edges; }
            set { m_edges = value; }
        }

        public List<Line2D> ObstacleLines
        {
            get { return m_obstacleLines; }
            set { m_obstacleLines = value;}
        }



        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public VisibilityGraph2D(List<Vector2D> points, List<Line2D> lines)
        {
            //create Graph:
            HashSet<Node> tmpNodes = new HashSet<Node>();
            foreach (Vector2D vec in points)
                tmpNodes.Add(new Node(vec, null));

            m_edges = new HashSet<Edge>();
            m_nodes = new HashSet<Node>();
            m_obstacleLines = lines;

            BuildGraph(tmpNodes);

            CalculateCentrality();
        }

        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public void BuildGraph(HashSet<Node> tmpNodes)
        {

            while (tmpNodes.Count != 0)
            {
                Node curNode = tmpNodes.ElementAt<Node>(0);

                foreach(Node elem in tmpNodes)
                {
                    if (elem == curNode)
                        continue;

                    bool isVisible = true;
                    foreach (Line2D line in m_obstacleLines)
                        if (GeoAlgorithms2D.LineIntersection(line.Start, line.End, curNode.Position, elem.Position) != null)
                        {
                            isVisible = false;
                            break;
                        }
                    if (isVisible)
                    {
                        curNode.Neighbors.Add(elem);
                        elem.Neighbors.Add(curNode);
                        m_edges.Add(new Edge(curNode, elem));
                    }
                }
                m_nodes.Add(curNode);
                tmpNodes.Remove(curNode);
            }
        }

        public void BuildGraph()
        {            

            List<Node> tmpNodes = new List<Node>();
            foreach (Node node in m_nodes)
                tmpNodes.Add(new Node(node.Position, null));

            m_nodes.Clear();
            m_edges.Clear();

            while (tmpNodes.Count != 0)
            {
                Node curNode = tmpNodes[0];

                for (int i = 1; i < tmpNodes.Count; i++)
                {
                    bool isVisible = true;
                    foreach (Line2D line in m_obstacleLines)
                        if (GeoAlgorithms2D.LineIntersection(line.Start, line.End, curNode.Position, tmpNodes[i].Position) != null)
                        {
                            isVisible = false;
                            break;
                        }
                    if (isVisible)
                    {
                        curNode.Neighbors.Add(tmpNodes[i]);
                        tmpNodes[i].Neighbors.Add(curNode);
                        m_edges.Add(new Edge(curNode, tmpNodes[i]));
                    }
                }
                m_nodes.Add(curNode);
                tmpNodes.Remove(curNode);
            }
        }

        public void CalculateCentrality()
        {
            int count = 0;
            foreach (Node curNode in m_nodes)
            {
                //create List of all expept first view
                HashSet<Node> tmpNodes = new HashSet<Node>();
                HashSet<Node> curStepDepthNodes = new HashSet<Node>();
                foreach (Node n in m_nodes)
                {
                    if (!curNode.Neighbors.Contains(n))
                        tmpNodes.Add(n);
                    else
                        curStepDepthNodes.Add(n);
                    count++;
                }

                curNode.StepDepthCountings.Add(curStepDepthNodes.Count);

                while (tmpNodes.Count != 0)
                {
                    curStepDepthNodes = NextStepDepth(curNode, curStepDepthNodes, ref tmpNodes, ref count);
                    if (curStepDepthNodes.Count > 0)
                        curNode.StepDepthCountings.Add(curStepDepthNodes.Count);
                    else
                        break;
                }
            }
            int final = count;
            //Calculate Centrality From StepDepthCountings...
            foreach (Node curNode in m_nodes)
            {
                for (int i = 0; i < curNode.StepDepthCountings.Count; i++)
                {
                    curNode.Closeness += curNode.StepDepthCountings[i] * (i+1);
                }
            }


        }

        HashSet<Node> NextStepDepth(Node curNode, HashSet<Node> curStepDepthNodes, ref HashSet<Node> tmpNodes, ref int count)
        {
            HashSet<Node> nextStepDepthNodes = new HashSet<Node>();
            HashSet<Node> tmpNodesCopy = new HashSet<Node>();

            foreach (Node elem in tmpNodes)
            {
                bool found = false;
                foreach (Node node in curStepDepthNodes)
                {
                    if (node.Neighbors.Contains(elem))
                    {
                        nextStepDepthNodes.Add(elem);
                        count++;
                        found = true;
                        break;
                    }
                }

                if (!found)
                    tmpNodesCopy.Add(elem);
            }

            tmpNodes = tmpNodesCopy;

            return nextStepDepthNodes;
        }

        public Vector2D GetMinMaxCloseness()
        {

            double max = double.MinValue;
            double min = double.MaxValue;
            foreach (Node node in m_nodes)
            {
                if (node.Closeness > max)
                    max = node.Closeness;
                if (node.Closeness < min)
                    min = node.Closeness;
            }
            return new Vector2D(min, max);
            
        }

        #endregion

    }

    [Serializable]
    public class Node
    {
        Vector2D m_pos;
        HashSet<Node> m_neighbors;
        List<int> m_stepDepthCountings;
        double m_closeness;

        public Vector2D Position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }

        public HashSet<Node> Neighbors
        {
            get { return m_neighbors; }
            set { m_neighbors = value; }
        }

        public List<int> StepDepthCountings
        {
            get { return m_stepDepthCountings; }
            set { m_stepDepthCountings = value; }
        }

        public int Connectivity
        {
            get { return m_neighbors.Count; }
        }

        public double Closeness
        {
            get { return m_closeness; }
            set { m_closeness = value; }
        }


        public Node(Vector2D pos, HashSet<Node> neighbors)
        {
            m_pos = pos;
            
            if (neighbors != null)
                m_neighbors = neighbors;
            else
                m_neighbors = new HashSet<Node>();

            m_stepDepthCountings = new List<int>();
        }

        public Node(Node old)
        {
            m_pos = old.m_pos;
            m_neighbors = old.m_neighbors;
        }

    }

    [Serializable]
    public class Edge
    {
        Node m_node1;
        Node m_node2;


        public Node Node1
        {
            get { return m_node1; }
            set { m_node1 = value; }
        }
        public Node Node2
        {
            get { return m_node2; }
            set { m_node2 = value; }
        }

        public Edge(Node node1, Node node2)
        {
            m_node1 = node1;
            m_node2 = node2;                       
        }

    }




}
