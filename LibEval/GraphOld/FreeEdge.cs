﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FreeEdge.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using Tektosyne.Collections;
using Tektosyne.Geometry;

namespace CPlan.Evaluation
{

    /// <summary>
    /// Represents a half-edge in a planar <see cref="Subdivision"/>.</summary>
    /// <remarks><para>
    /// <b>SubdivisionEdge</b> does not represent a complete edge in a <see cref="Subdivision"/> but
    /// rather a half-edge. Any given <b>SubdivisionEdge</b> holds one end point of an edge, and a
    /// pointer to its twin half-edge which holds the other end point.
    /// </para><para>
    /// Every <b>SubdivisionEdge</b> is part of a cycle of half-edges that is connected by the <see
    /// cref="SubdivisionEdge.Next"/> and <see cref="SubdivisionEdge.Previous"/> pointers. Assuming
    /// y-coordinates increase upward, a clockwise cycle forms the inner boundary of the incident
    /// <see cref="SubdivisionFace"/>, and a counter-clockwise cycle forms its outer boundary. A
    /// <b>SubdivisionEdge</b> may form a cycle with its own twin half-edge; such a zero-area cycle
    /// always forms an inner boundary.</para></remarks>

    [Serializable]
    public sealed class FreeEdge: IEquatable<FreeEdge>, IKeyedValue<Int32> {
        #region FreeEdge(Int32)

        /// <overloads>
        /// Initializes a new instance of the <see cref="SubdivisionEdge"/> class.</overloads>
        /// <summary>
        /// Initializes a new instance of the <see cref="SubdivisionEdge"/> class with the specified
        /// unique key.</summary>
        /// <param name="key">
        /// The unique key of the <see cref="SubdivisionEdge"/> within its containing <see
        /// cref="Subdivision"/>.</param>

        internal FreeEdge(int key) {
            _key = key;
        }

        #endregion
        #region FreeEdge(Int32, PointD, ...)

        /// <summary>
        /// Initializes a new instance of the <see cref="SubdivisionEdge"/> class with the specified
        /// unique key, origin, incident face, twin, next and previous half-edge.</summary>
        /// <param name="key">
        /// The unique key of the <see cref="SubdivisionEdge"/> within its containing <see
        /// cref="Subdivision"/>.</param>
        /// <param name="origin">
        /// The coordinates where the <see cref="SubdivisionEdge"/> begins.</param>
        /// <param name="twin">
        /// The <see cref="SubdivisionEdge"/> that is the twin of the current instance.</param>
        /// <param name="face">
        /// The <see cref="SubdivisionFace"/> that is bounded by the <see cref="SubdivisionEdge"/>.
        /// </param>
        /// <param name="next">
        /// The next <see cref="SubdivisionEdge"/> that bounds the same <paramref name="face"/>.
        /// </param>
        /// <param name="previous">
        /// The previous <see cref="SubdivisionEdge"/> that bounds the same <paramref name="face"/>.
        /// </param>
        /// <remarks>
        /// This constructor is intended for unit testing.</remarks>

        internal FreeEdge(int key, PointD origin, FreeEdge twin, FreeEdge next, FreeEdge previous)
        {

            _key = key;
            _origin = origin;
            _twin = twin;
            _next = next;
            _previous = previous;
        }

        #endregion
        #region Internal Fields

        /// <summary>
        /// The unique key of the <see cref="SubdivisionEdge"/>.</summary>

        internal int _key;

        /// <summary>
        /// The coordinates where the <see cref="SubdivisionEdge"/> begins.</summary>

        internal PointD _origin;

        /// <summary>
        /// The next <see cref="SubdivisionEdge"/> that bounds the same <see cref="Face"/>.
        /// </summary>

        internal FreeEdge _next;

        /// <summary>
        /// The previous <see cref="SubdivisionEdge"/> that bounds the same <see cref="Face"/>.
        /// </summary>

        internal FreeEdge _previous;

        /// <summary>
        /// The <see cref="SubdivisionEdge"/> that is the twin of the current instance.</summary>

        internal FreeEdge _twin;

        #endregion
        #region Public Properties
        #region Key

        /// <summary>
        /// Gets the unique key of the <see cref="SubdivisionEdge"/>.</summary>
        /// <value>
        /// The unique key of the <see cref="SubdivisionEdge"/> within its containing <see
        /// cref="Subdivision"/>.</value>
        /// <remarks><para>
        /// <b>Key</b> begins at zero for the first <see cref="SubdivisionEdge"/> instance in a <see
        /// cref="Subdivision"/>, and is incremented by one whenever an additional instance is
        /// created. The <b>Key</b> values of all <see cref="SubdivisionEdge"/> instances thus
        /// reflect the order in which they were created.
        /// </para><para>
        /// <b>Key</b> is usually immutable, unless <see cref="Subdivision.RenumberEdges"/> is
        /// called on the containing <see cref="Subdivision"/>.</para></remarks>

        public int Key {
            //[DebuggerStepThrough]
            get { return _key; }
        }

        #endregion
        #region Origin

        /// <summary>
        /// Gets the coordinates where the <see cref="SubdivisionEdge"/> begins.</summary>
        /// <value>
        /// The <see cref="PointD"/> coordinates of the origin of the half-edge represented by the
        /// <see cref="SubdivisionEdge"/>.</value>

        public PointD Origin {
            //[DebuggerStepThrough]
            get { return _origin; }
        }

        #endregion
        #region Destination

        /// <summary>
        /// Gets the coordinates where the <see cref="SubdivisionEdge"/> ends.</summary>
        /// <value>
        /// The <see cref="Origin"/> of the <see cref="Twin"/> of the <see cref="SubdivisionEdge"/>.
        /// </value>

        public PointD Destination {
            get { return _twin._origin; }
        }

        #endregion
        #region Previous

        /// <summary>
        /// Gets the previous <see cref="SubdivisionEdge"/> that bounds the same <see
        /// cref="Face"/>.</summary>
        /// <value>
        /// The <see cref="SubdivisionEdge"/> that ends at the <see cref="Origin"/> of the current
        /// instance and bounds the same <see cref="Face"/>.</value>
        /// <remarks><para>
        /// <b>Previous</b> returns the <see cref="Twin"/> of the current instance if no other <see
        /// cref="SubdivisionEdge"/> ends at its <see cref="Origin"/>.
        /// </para><para>
        /// If there are multiple eligible <see cref="SubdivisionEdge"/> instances, <b>Previous</b>
        /// returns the nearest in counter-clockwise direction, assuming that y-coordinates increase
        /// upward.
        /// </para><para>
        /// <b>Previous</b> defaults to a null reference while a <see cref="Subdivision"/> is being
        /// created, but never returns a null reference after construction is finished.
        /// </para></remarks>

        public FreeEdge Previous
        {
            //[DebuggerStepThrough]
            get { return _previous; }
        }

        #endregion
        #region Twin

        /// <summary>
        /// Gets the <see cref="SubdivisionEdge"/> that is the twin of the current instance.
        /// </summary>
        /// <value>
        /// The <see cref="SubdivisionEdge"/> that begins at the <see cref="Destination"/> and ends
        /// at the <see cref="Origin"/> of the current instance.</value>
        /// <remarks><para>
        /// A <see cref="SubdivisionEdge"/> and its <b>Twin</b> combine to form one edge in a <see
        /// cref="Subdivision"/>, corresponding to a single <see cref="LineD"/> instance.
        /// </para><para>
        /// <b>Twin</b> defaults to a null reference while a <see cref="Subdivision"/> is being
        /// created, but never returns a null reference after construction is finished.
        /// </para></remarks>

        public FreeEdge Twin
        {
            //[DebuggerStepThrough]
            get { return _twin; }
        }

        #endregion 
        #region OriginEdges

        /// <summary>
        /// Gets an <see cref="IEnumerable{T}"/> collection that contains all half-edges with the
        /// same <see cref="Origin"/>.</summary>
        /// <value>
        /// An <see cref="IEnumerable{T}"/> collection that contains each <see
        /// cref="SubdivisionEdge"/> with the same <see cref="Origin"/>.</value>
        /// <remarks>
        /// <b>OriginEdges</b> begins with the current <see cref="SubdivisionEdge"/> and follows the
        /// chain of <see cref="Twin"/> and <see cref="Next"/> pointers until the sequence is
        /// complete, yielding each encountered <see cref="SubdivisionEdge"/> in turn.</remarks>

        public IEnumerable<FreeEdge> OriginEdges
        {
            get {
                FreeEdge cursor = this;
                do {
                    yield return cursor;
                    cursor = cursor._twin._next;
                } while (cursor != this);
            }
        }

        #endregion       
        #endregion

        #region Public Methods
        #region GetEdgeTo(PointD)

        /// <summary>
        /// Returns the half-edge with the same origin and the specified destination.</summary>
        /// <summary>
        /// Returns the half-edge with the same origin and the specified destination, using exact
        /// coordinate comparisons.</summary>
        /// <param name="destination">
        /// The <see cref="Destination"/> of the half-edge.</param>
        /// <returns><para>
        /// The <see cref="SubdivisionEdge"/> with the same <see cref="Origin"/> as the current
        /// instance, and with the specified <paramref name="destination"/>.
        /// </para><para>-or-</para><para>
        /// A null reference if no matching <see cref="SubdivisionEdge"/> was found.
        /// </para></returns>
        /// <remarks>
        /// <b>GetEdgeTo</b> is an O(m) operation, where m is the number of half-edges originating
        /// from the current <see cref="Origin"/>.</remarks>

        public FreeEdge GetEdgeTo(PointD destination)
        {
            var edge = this;

            do {
                var twin = edge._twin;
                if (twin._origin == destination)
                    return edge;

                edge = twin._next;
            } while (edge != this);

            return null;
        }

        #endregion
        #region GetEdgeTo(PointD, Double)

        /// <summary>
        /// Returns the half-edge with the same origin and the specified destination, given the
        /// specified epsilon for coordinate comparisons.</summary>
        /// <param name="destination">
        /// The <see cref="Destination"/> of the half-edge.</param>
        /// <param name="epsilon">
        /// The maximum absolute difference at which coordinates should be considered equal.</param>
        /// <returns><para>
        /// The <see cref="SubdivisionEdge"/> with the same <see cref="Origin"/> as the current
        /// instance, and with the specified <paramref name="destination"/>.
        /// </para><para>-or-</para><para>
        /// A null reference if no matching <see cref="SubdivisionEdge"/> was found.
        /// </para></returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="epsilon"/> is less than zero.</exception>
        /// <remarks>
        /// <b>GetEdgeTo</b> is identical with the basic <see cref="GetEdgeTo(PointD)"/> overload
        /// but uses the specified <paramref name="epsilon"/> to compare the specified <paramref
        /// name="destination"/> against existing <see cref="Subdivision.Vertices"/>.</remarks>

        public FreeEdge GetEdgeTo(PointD destination, double epsilon)
        {
            var edge = this;

            do {
                var twin = edge._twin;
                if (PointD.Equals(twin._origin, destination, epsilon))
                    return edge;

                edge = twin._next;
            } while (edge != this);

            return null;
        }

        #endregion
        #region GetHashCode

        /// <summary>
        /// Returns the hash code for this <see cref="SubdivisionEdge"/> instance.</summary>
        /// <returns>
        /// An <see cref="Int32"/> hash code.</returns>
        /// <remarks>
        /// <b>GetHashCode</b> returns the value of the <see cref="Key"/> property, which is
        /// guaranteed to be unique within the containing <see cref="Subdivision"/>.</remarks>

        public override int GetHashCode() {
            return _key;
        }

        #endregion 
        #region ToLine

        /// <summary>
        /// Converts the <see cref="SubdivisionEdge"/> to a <see cref="LineD"/> with the same
        /// direction.</summary>
        /// <returns>
        /// A <see cref="LineD"/> whose <see cref="LineD.Start"/> point equals the <see
        /// cref="Origin"/> and whose <see cref="LineD.End"/> point equals the <see
        /// cref="Destination"/> of the <see cref="SubdivisionEdge"/>.</returns>

        public LineD ToLine() {
            return new LineD(_origin, _twin._origin);
        }

        #endregion
        #region ToLineReverse

        /// <summary>
        /// Converts the <see cref="SubdivisionEdge"/> to a <see cref="LineD"/> with the opposite
        /// direction.</summary>
        /// <returns>
        /// A <see cref="LineD"/> whose <see cref="LineD.Start"/> point equals the <see
        /// cref="Destination"/> and whose <see cref="LineD.End"/> point equals the <see
        /// cref="Origin"/> of the <see cref="SubdivisionEdge"/>.</returns>

        public LineD ToLineReverse() {
            return new LineD(_twin._origin, _origin);
        }

        #endregion
        #region ToString

        /// <summary>
        /// Returns a <see cref="String"/> that represents the <see cref="SubdivisionEdge"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="String"/> containing the culture-invariant string representations of the
        /// <see cref="Key"/> and <see cref="Origin"/> properties, as well as the <see cref="Key"/>
        /// values of the <see cref="Face"/>, <see cref="Twin"/>, <see cref="Next"/>, and <see
        /// cref="Previous"/> properties. The value -1 is substituted for any properties that are
        /// null references.</returns>

        public override string ToString() {
            Func<FreeEdge, Int32> edgeKey = (e => e == null ? -1 : e._key);

            return String.Format(CultureInfo.InvariantCulture,
                "Edge {0}: {1}, Twin={2}, Face={3}, Next={4}, Previous={5}",
                _key, _origin, edgeKey(_twin), edgeKey(_next), edgeKey(_previous));
        }

        #endregion
        #endregion
        
        #region Internal Methods
        #region FindEdgePosition

        /// <summary>
        /// Finds the position of a half-edge to the specified <see cref="Destination"/> within the
        /// vertex chain around <see cref="Origin"/>.</summary>
        /// <param name="destination">
        /// The <see cref="Destination"/> of the half-edge whose position to find.</param>
        /// <param name="nextEdge">
        /// Returns the <see cref="SubdivisionEdge"/> that is <see cref="Next"/> from the <see
        /// cref="Twin"/> of a half-edge to <paramref name="destination"/> within the vertex chain
        /// around <see cref="Origin"/>.</param>
        /// <param name="previousEdge">
        /// Returns the <see cref="SubdivisionEdge"/> whose <see cref="Twin"/> is <see
        /// cref="Previous"/> from a half-edge to <paramref name="destination"/> within the vertex
        /// chain around <see cref="Origin"/>.</param>

        internal void FindEdgePosition(PointD destination,
            out FreeEdge nextEdge, out FreeEdge previousEdge)
        {

            nextEdge = this;
            previousEdge = this;

            // determine reference angle between new and existing edge
            PointD pivot = _twin._origin;
            double firstAngle = _origin.AngleBetween(destination, pivot);
            double angle = firstAngle;

            if (firstAngle > 0) {
                // positive angle: decrease until first negative angle found,
                // or until angle wraps around to greater than starting value
                do {
                    previousEdge = nextEdge;
                    nextEdge = nextEdge._twin._next;
                    pivot = nextEdge._twin._origin;
                    angle = _origin.AngleBetween(destination, pivot);
                } while (angle > 0 && angle < firstAngle);
            } else {
                // negative angle: increase until first positive angle found,
                // or until angle wraps around to smaller than starting value
                do {
                    nextEdge = previousEdge;
                    previousEdge = previousEdge._previous._twin;
                    pivot = previousEdge._twin._origin;
                    angle = _origin.AngleBetween(destination, pivot);
                } while (angle < 0 && angle > firstAngle);
            }
        }

        #endregion      
        #region IsCompatibleDestination

        /// <summary>
        /// Determines whether the specified <see cref="Destination"/> is compatible with the vertex
        /// chain around <see cref="Origin"/>.</summary>
        /// <param name="destination">
        /// The <see cref="PointD"/> coordinates of the new <see cref="Destination"/>.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="destination"/> is compatible with the vertex chain around
        /// <see cref="Origin"/>; otherwise, <c>false</c>.</returns>
        /// <remarks><para>
        /// <b>IsCompatibleDestination</b> always returns <c>true</c> if the <see
        /// cref="SubdivisionEdge"/> is the only incident half-edge at its <see cref="Origin"/>, or
        /// if the specified <paramref name="destination"/> equals the current <see
        /// cref="Destination"/>.
        /// </para><para>
        /// Otherwise, <b>IsCompatibleDestination</b> returns <c>true</c> exactly if rotating the
        /// <see cref="SubdivisionEdge"/> to <paramref name="destination"/>, in the direction that
        /// minimizes angular distance, would not traverse any neighboring edge in the vertex chain
        /// around <see cref="Origin"/>.</para></remarks>

        internal bool IsCompatibleDestination(PointD destination) {

            // succeed if only one edge or same destination
            if (_next == _twin) return true;
            PointD pivot = _twin._origin;
            if (pivot == destination) return true;

            // compute angles to destination and previous edge
            double pivotAngle = _origin.AngleBetween(pivot, destination);
            double prevAngle = _origin.AngleBetween(pivot, _previous._origin);

            // compute angle to next edge, if different
            FreeEdge next = _twin._next._twin;
            double nextAngle = (_previous == next ? prevAngle :
                _origin.AngleBetween(pivot, next._origin));

            // adjust signs of neighboring angles
            if (prevAngle > 0) prevAngle -= 2 * Math.PI;
            if (nextAngle < 0) nextAngle += 2 * Math.PI;

            if (pivotAngle < 0) {
                if (prevAngle < 0)
                    return (pivotAngle > prevAngle);
                else {
                    //Debug.Assert(nextAngle < 0);
                    return (pivotAngle > nextAngle);
                }
            } else {
                if (prevAngle > 0)
                    return (pivotAngle < prevAngle);
                else {
                    //Debug.Assert(nextAngle > 0);
                    return (pivotAngle < nextAngle);
                }
            }
        }

        #endregion
        #endregion

        #region IEquatable Members
        #region Equals(Object)

        /// <overloads>
        /// Determines whether two <see cref="SubdivisionEdge"/> instances have the same value.
        /// </overloads>
        /// <summary>
        /// Determines whether this <see cref="SubdivisionEdge"/> instance and a specified object,
        /// which must be a <see cref="SubdivisionEdge"/>, have the same value.</summary>
        /// <param name="obj">
        /// An <see cref="Object"/> to compare to this <see cref="SubdivisionEdge"/> instance.
        /// </param>
        /// <returns>
        /// <c>true</c> if <paramref name="obj"/> is another <see cref="SubdivisionEdge"/> instance
        /// and its value is the same as this instance; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// If the specified <paramref name="obj"/> is another <see cref="SubdivisionEdge"/>
        /// instance, or an instance of a derived class, <b>Equals</b> invokes the strongly-typed
        /// <see cref="Equals(SubdivisionEdge)"/> overload to test the two instances for value
        /// equality.</remarks>

        public override bool Equals(object obj) {
            return Equals(obj as FreeEdge);
        }

        #endregion
        #region Equals(FreeEdge)

        /// <summary>
        /// Determines whether this instance and a specified <see cref="SubdivisionEdge"/> have the
        /// same value.</summary>
        /// <param name="edge">
        /// A <see cref="SubdivisionEdge"/> to compare to this instance.</param>
        /// <returns>
        /// <c>true</c> if the value of <paramref name="edge"/> is the same as this instance;
        /// otherwise, <c>false</c>.</returns>
        /// <remarks><para>
        /// <b>Equals</b> compares the values all properties of the two <see
        /// cref="SubdivisionEdge"/> instances to test for value equality. Properties of type <see
        /// cref="SubdivisionEdge"/> and <see cref="SubdivisionFace"/> are compared using <see
        /// cref="Object.ReferenceEquals"/>.
        /// </para><para>
        /// <b>Equals</b> is intended for unit testing, as any two <see cref="SubdivisionEdge"/>
        /// instances created during normal operation are never equal.</para></remarks>

        public bool Equals(FreeEdge edge)
        {

            if (Object.ReferenceEquals(edge, this)) return true;
            if (Object.ReferenceEquals(edge, null)) return false;

            return (_origin.Equals(edge._origin)
                && Object.ReferenceEquals(_twin, edge._twin)
                && Object.ReferenceEquals(_next, edge._next)
                && Object.ReferenceEquals(_previous, edge._previous));
        }

        #endregion
        #region Equals(FreeEdge, FreeEdge)

        /// <summary>
        /// Determines whether two specified <see cref="SubdivisionEdge"/> instances have the same
        /// value.</summary>
        /// <param name="x">
        /// The first <see cref="SubdivisionEdge"/> to compare.</param>
        /// <param name="y">
        /// The second <see cref="SubdivisionEdge"/> to compare.</param>
        /// <returns>
        /// <c>true</c> if the value of <paramref name="x"/> is the same as the value of <paramref
        /// name="y"/>; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// <b>Equals</b> invokes the non-static <see cref="Equals(SubdivisionEdge)"/> overload to
        /// test the two <see cref="SubdivisionEdge"/> instances for value equality.</remarks>

        public static bool Equals(FreeEdge x, FreeEdge y)
        {

            if (Object.ReferenceEquals(x, null))
                return Object.ReferenceEquals(y, null);

            return x.Equals(y);
        }

        #endregion
        #region StructureEquals

        /// <summary>
        /// Determines whether this instance and a specified <see cref="SubdivisionEdge"/> have the
        /// same structure.</summary>
        /// <param name="edge">
        /// A <see cref="SubdivisionEdge"/> to compare to this instance.</param>
        /// <returns>
        /// <c>true</c> if the structure of <paramref name="edge"/> is the same as this instance;
        /// otherwise, <c>false</c>.</returns>
        /// <remarks><para>
        /// <b>StructureEquals</b> compares the values all properties of the two <see
        /// cref="SubdivisionEdge"/> instances to test for structural equality. Properties of type
        /// <see cref="SubdivisionEdge"/> and <see cref="SubdivisionFace"/> are compared by their
        /// <see cref="Key"/> values.
        /// </para><para>
        /// <b>StructureEquals</b> is intended for testing the <see cref="Subdivision.Clone"/> 
        /// method which replicate <see cref="SubdivisionEdge"/> and <see cref="SubdivisionFace"/>
        /// keys but not references.</para></remarks>

        public bool StructureEquals(FreeEdge edge)
        {

            if (Object.ReferenceEquals(edge, this)) return true;
            if (Object.ReferenceEquals(edge, null)) return false;

            return (_key == edge._key
                && _origin == edge._origin
                && _twin._key == edge._twin._key
                && _next._key == edge._next._key
                && _previous._key == edge._previous._key);
        }

        #endregion
        #endregion
    }
}
