﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ShortestPathesGrid.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Linq;
using CPlan.Geometry;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;

namespace CPlan.Evaluation
{
    /// <summary>
    /// Computes a shortest path analysis.
    /// <remarks><para>
    /// With this analysis, for each place in a grid total depth for 
    /// metric distances and angular distances (Space Syntax) is computed.
    /// </para></remarks>
    [Serializable]
    public class ShortestPathesGrid
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary> Tektosyne <see cref="IGraph2D"/> list, that represents the grid graph. </summary>
        public readonly PolygonGrid CurGrid;
        /// <summary> Holds the walls. Based on them the visibility is computed. </summary>
        //public List<GLine> Walls = new List<GLine>();
        /// <summary> <see cref="ArrayEx"/> instance to hold the computed distance (total depth) for each cell </summary>
        public ArrayEx<Single> DistMetric;
        /// <summary> <see cref="ArrayEx"/> instance to hold the computed angles in a path for each cell </summary>
        public ArrayEx<Single> DistAngle;
        /// <summary> <see cref="ArrayEx"/> instance to hold the counter, how often a cell was part of a shortest path (choice).</summary>
        public ArrayEx<Single> Choice;

        //private double GridCellSize;

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        /// <summary>
        /// Initialise a new instance of the <b>AVisibility</b> class .
        /// </summary>
        /// <param name="grid">A grid corresponding to a PolygonGrid.</param>
        /// <param name="walls">A list of walls as list of <see cref="GLine"/> instances.</param>
        /// <param name="resolution">The size of the analysed grid in rows and columns</param>
        /// <param name="cellSize">The size of a cell in the grid</param>
        //public ShortestPathesGrid(PolygonGrid grid, List<GLine> walls, SizeI resolution, double cellSize)
        //{
        //    if (grid == null)
        //        ThrowHelper.ThrowArgumentNullException("grid");
        //    CurGrid = grid;
        //    Walls = walls;
        //    GridCellSize = cellSize;

        //    DistMetric = new ArrayEx<Single>(resolution.Width, resolution.Height);
        //    DistAngle = new ArrayEx<Single>(resolution.Width, resolution.Height);
        //    Choice = new ArrayEx<Single>(resolution.Width, resolution.Height);
        //}
        # endregion

        //############################################################################################################################################################################
        # region Methods
        /// <summary>
        /// Computes the shortes pathes for the grid/graph.
        /// </summary>
        /// <remarks><para>
        /// The results of this computation are stored in the DistMetric and DistAngle <see cref="ArrayEx"/>.
        /// </para></remarks>
        /// <param name="progressBar">For the use of a ToolStripProgressBar. 
        /// If there is no ProgressBar use "null" as parameter.</param>
        //public void Evaluate(ToolStripProgressBar progressBar)
        //{
        //    if (progressBar != null)
        //    {
        //        progressBar.Visible = true;
        //        progressBar.Value = 100;
        //    }

        //    // --- reset the counting value -----------------------------
        //    for (int i = 0; i < DistMetric.Count; i++)
        //    {
        //        DistMetric.SetValue(0, i);
        //        DistAngle.SetValue(0, i);
        //        Choice.SetValue(0, i);
        //    }

        //    //--- Create a Subdivision (Graph) from the Grid ---
        //    //PointD offset = new PointD(GridCellSize / 2, GridCellSize / 2);
        //    PointD offset = new PointD(0, 0);
        //    Subdivision curSubdiv = CurGrid.ToSubdivisionFromLines(offset, Walls);
        //    Subdivision tempSubdiv = (Subdivision)curSubdiv.Clone();

        //    // ---------------------------------------------------------------------------
        //    IGraph2D<PointD> curGraph = curSubdiv;
        //    var shortPathes = new Dijkstra<PointD>(curGraph);
        //    IEnumerable<PointD> alllNodes = curSubdiv.Nodes;
        //    Dictionary<PointD, int> steps = new Dictionary<PointD, int>();
        //    PointI curPos;
        //    foreach (PointD curNode in alllNodes)
        //    {
        //        // --- the distances ---
        //        shortPathes.CalculateDistance(curNode);
        //        curPos = CurGrid.DisplayToGrid(curNode);
        //        DistMetric[curPos.X, curPos.Y] = Convert.ToSingle(shortPathes.TotalDepth());

        //        // --- choice ---
        //        foreach (PointD toNode in alllNodes)
        //        {
        //            IList<PointD> curPath = shortPathes.Nodes(toNode);
        //            foreach (PointD curStep in curPath)
        //            {
        //                curPos = CurGrid.DisplayToGrid(curStep);
        //                Choice[curPos.X, curPos.Y] += 1;
        //            }
        //        }

        //        // --- the angles ---
        //        curPos = CurGrid.DisplayToGrid(curNode);
        //        Single sumPathAngles = 0;
        //        foreach (PointD toNode in alllNodes)
        //        {
        //            sumPathAngles += GraphTools.AngleDepth(shortPathes.Nodes(toNode));
        //        }
        //        DistAngle[curPos.X, curPos.Y] = sumPathAngles;

        //        Application.DoEvents(); // makes the progress of the progressBar visible
        //    }

        //    Single pow = 0.1f;
        //    for (int i = 0; i < Choice.Count; i++)
        //    {
        //        Single curNode = Choice[i];
        //        Choice[i] = Convert.ToSingle(Math.Pow(curNode, pow));
        //    }

        //    Extensions.InvertValues(DistMetric);
        //    Extensions.InvertValues(DistAngle);

        //    if (progressBar != null)
        //    {
        //        progressBar.Value = 0;
        //        progressBar.Visible = false;
        //    }
        //}

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Choice value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Choice values as <see cref="float[,]"/> array.
        /// </returns>
        public Single[,] GetNormChoiceArray()
        {
            Tools.CutMinValue(Choice, Fortran.Min(Choice.ToArray<Single>()));
            Tools.Normalize(Choice);
            Tools.IntervalExtension(Choice, Fortran.Max(Choice.ToArray<Single>()));
            return Extensions.CopyTo2dArray(Choice);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised DistMetric value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise DistMetric values as <see cref="float[,]"/> array.
        /// </returns>
        public Single[,] GetNormDistMetricArray()
        {
            Tools.Normalize(DistMetric);
            Tools.IntervalExtension(DistMetric, Fortran.Max(DistMetric.ToArray<Single>()));
            return Extensions.CopyTo2dArray(DistMetric);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised DistAngle value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise DistAngle values as <see cref="float[,]"/> array.
        /// </returns>
        public Single[,] GetNormDistAngleArray()
        {
            Tools.Normalize(DistAngle);
            Tools.IntervalExtension(DistAngle, Fortran.Max(DistAngle.ToArray<Single>()));
            return Extensions.CopyTo2dArray(DistAngle);
        }

        # endregion

    }
}
