﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = DijkstraAll.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Tektosyne;
using Tektosyne.Geometry;
using Tektosyne.Graph;

namespace CPlan.Evaluation
{

    [Serializable]
    public class DijkstraAll<T> //: IGraphPath<T>
    {
        // -- using two dictionorys for the same purpose because smaller dictionorys are faster: http://www.dotnetperls.com/dictionary-size
        public Dictionary<LineD, IList<T>> AllPathes;
        //public Dictionary<LineD, IList<T>> PathesTwo;

        private IGraph2D<T> CurGraph;
        private IList<T> Destinations;
        private IList<T> Basis;
        private Dictionary <T, double> Dist;
        private Dictionary<T, T> Previous;
        private Dictionary<LineD, double> RefValues;
        private Subdivision RefSubDiv;
        public bool CleanPathes = false;

        /// <summary>
        /// Konstruktor 
        /// </summary>
        /// <param name="graph">
        /// The <see cref="IGraph2D{T}"/> on which all searches are performed. 
        /// DijkstraGeneral is useful if a inverse graph is used, but the metric/angular values from the original graph should be used for calulations</param>
        /// <param name="refValues"> send the reference values that represent the distances between two nodes. 
        /// Can be metric distances, or angular distances or anything else.</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="graph"/> The graph to calculate on.</exception>
        /// <paramref name="refValues"/> Reference values can be metric distances, angles or other values to calculate shortest pathes with.</exception>
        public DijkstraAll(IGraph2D<T> graph, Dictionary<LineD, double> refValues, Subdivision refSubDiv) // Dictionary<LineD : sollte noch generisch werden!!! Tests mit T[] haben aber wegen Dictionary.Contains() nicht funktioniert. Grund unbekannt...
        {
            if (graph == null)
                ThrowHelper.ThrowArgumentNullException("graph");
            CurGraph = graph;
            RefValues = refValues;
            AllPathes = new Dictionary<LineD, IList<T>>();
            RefSubDiv = refSubDiv;
        }

        /// <summary>
        /// Calculates the shortest pathes from the start node to all other nodes.
        /// </summary>
        /// <param name="start">start node</param>
        public void CalculateDistance(T start)
        {
            Destinations = new List<T>();
            Basis = new List<T>();
            Dist = new Dictionary<T, double>();
            Previous = new Dictionary<T, T>();
            // -- Knoten aufnehmen --
            foreach (T node in CurGraph.Nodes)
            {
                Previous.Add(node, default(T));
                Dist.Add(node, double.MaxValue);
                Basis.Add(node);
                PointD S = CurGraph.GetWorldLocation(start);
                PointD D = CurGraph.GetWorldLocation(node);
                LineD testLine = new LineD(S, D);
                if (! AllPathes.Keys.Contains(testLine))
                    Destinations.Add(node);
            }

            Dist[start] = 0;
            while (Destinations.Count > 0)
            {
                T u = GetNodeWithSmallestDistance();               
                if (EqualityComparer<T>.Default.Equals(u, default(T))) 
                {
                    Basis.Clear();
                    Destinations.Clear();
                }
                else 
                {

                    foreach (T v in CurGraph.GetNeighbors(u))
                    {
                        PointD U = CurGraph.GetWorldLocation(u);
                        PointD V = CurGraph.GetWorldLocation(v);
                        LineD lineA = new LineD(U, V);
                        LineD lineB = new LineD(V, U);
                        double refValue = double.MaxValue;
                        if (RefValues.Keys.Contains(lineA))
                        {
                            refValue = RefValues[lineA];
                        }
                        else if (RefValues.Keys.Contains(lineB))
                        {
                            refValue = RefValues[lineB];
                        }
                        double alt = Dist[u] + refValue;// CurGraph.GetDistance(u, v);
                        if (alt < Dist[v])
                        {
                            Dist[v] = alt;
                            Previous[v] = u;
                        }
                    }
                    
                    Basis.Remove(u);
                    //if (Destinations.Contains(u)) 
                    Destinations.Remove(u);
                }
            }

            // -----------------------------------------------------------------------------------
            // -- save the pathes, inversePathes in a global list -----------------
            // -----------------------------------------------------------------------------------
            foreach (T node in CurGraph.Nodes)
            {
                // -- Points: S=Start, D=Destination, M=Middle --
                PointD S = CurGraph.GetWorldLocation(start);
                PointD D = CurGraph.GetWorldLocation(node);
                LineD lineA = new LineD(S, D);
                LineD lineB = new LineD(D, S);

                if (!AllPathes.Keys.Contains(lineA))// & (!AllPathes.Keys.Contains(lineB)))
                {
                    IList<T> curPath = Nodes(node);
                    if (curPath.Count > 1)
                    {

                        {
                            for (int i = 0; i < curPath.Count - 1; i++)
                            {
                                int nextIdx = i + 2;
                                if (curPath.Count - 1 < nextIdx) break;
                                PointD firstPt = CurGraph.GetWorldLocation(curPath[i]);
                                PointD secondPt = CurGraph.GetWorldLocation(curPath[nextIdx]);
                                if (RefSubDiv.FindEdge(firstPt, secondPt) != null) // --- if the edge exists
                                {
                                    curPath.Remove(curPath[i + 1]); // --- remove the node in between => avoids the detour...
                                }
                            }
                        }

                        // -- full path --
                        AllPathes.Add(lineA, new List<T>(curPath));
                        // -- full path in the other direction --
                        // -- inversePath holds the path in the other direction --
                        List<T> inversePath = new List<T>(curPath);
                        inversePath.Reverse();
                        AllPathes.Add(lineB, new List<T>(inversePath));
                    }
                }
            }

        }

        /// <summary>
        /// Gets a list of all nodes in the best path found by the last
        /// successful path search.
        /// </summary>
        /// <param name="s"> source node </param>
        /// <param name="d">target node </param>
        /// <returns></returns>
        public IList<T> Nodes(T s, T d)
        {
            IList<T> path = new List<T>();
            PointD S = CurGraph.GetWorldLocation(s);
            PointD D = CurGraph.GetWorldLocation(d);
            LineD refLine = new LineD(S, D);
            if (AllPathes.Keys.Contains(refLine))
            {
                path = AllPathes[refLine];
            }
            //else
            //    path = path;
            return path;
        }

        private IList<T> Nodes(T d)
        {
            IList<T> path = new List<T>();
            path.Insert(0, d);
            while (!EqualityComparer<T>.Default.Equals(Previous[d], default(T)))
            {
                d = Previous[d];
                path.Insert(0, d);
            }
            return path;
        }
        
        /// <summary>
        /// Get the total cost to target node.
        /// </summary>
        /// <param name="s"> source node </param>
        /// <param name="d"> target node </param>
        /// <returns>total cost</returns>
        public double TotalCost(T s, T d)
        {
            double sum = 0, refValue = 0;
            IList<T> curPath = Nodes(s, d);
            for (int i = 0; i < curPath.Count - 1; i++)
            {
                refValue = 0;
                T node = curPath[i];
                T nextNode = curPath[i+1];
                PointD S = CurGraph.GetWorldLocation(node);
                PointD D = CurGraph.GetWorldLocation(nextNode);
                LineD lineA = new LineD(S, D);
                LineD lineB = new LineD(D, S);
                if (RefValues.Keys.Contains(lineA))
                {
                    refValue = RefValues[lineA];
                }
                else if (RefValues.Keys.Contains(lineB))
                {
                    refValue = RefValues[lineB];
                }
                sum += refValue;
            }

            /*foreach (T node in curPath)
            {
                sum += Dist[node];
            }*/
            return sum; 
        }

        /// <summary>
        /// Get the total cost in steps (depth) to target node.
        /// </summary>
        /// <param name="s"> source node </param>
        /// <param name="d">target node</param>
        /// <returns>total steps to target</returns>
        public int Depth(T s, T d)
        {
            int sum = 0;
            IList<T> curPath = Nodes(s, d);
            sum += curPath.Count - 1;
            return sum;
        }

        /// <summary>
        /// Get the total cost in steps to all other nodes in the graph (total depth).
        /// </summary>
        /// <param name="s"> source node </param>
        /// <returns>total steps to all targets</returns>
        public int TotalDepth(T s)
        {
            int sum = 0;
            IList<T> curPath;// = Nodes(d);
            foreach (T node in CurGraph.Nodes)
            {
                curPath = Nodes(s, node);
                if (curPath.Count>0) 
                    sum += curPath.Count - 1; // -1: without counting the start node
            }
            return sum;
        }

        /// <summary>
        /// Get the total cost to all other nodes in the graph (total depth).
        /// </summary>
        /// <param name="s"> source node </param>
        /// <returns>total steps to all targets</returns>
        public double TotalCostsToAll(T s)
        {
            double sum = 0;
            foreach (T node in CurGraph.Nodes)
            {
                sum += TotalCost(s, node); 
            }
            return sum;
        }

        /// <summary>
        /// Returns the last node in the IGraphPath whose total 
        /// cost does not exceed the specified maximum cost.
        /// </summary>
        /// <param name="d">target node <n/param>
        /// <param name="maxCost">maximum cost <n/param>
     /*   public T GetLastNode(T s, T d, double maxCost)
        {
            double max = maxCost;
            IList<T> curPath = Nodes(s, d);
            T curNode = default(T);
            for (int i = curPath.Count-1; i >= 0; i--) 
            {
                if (max <= 0) break;
                T node = curPath[i];
                max -= Dist[node];
                curNode = node;
            }
            return curNode;
        }*/

        /// <summary>
        /// Returns the node with the shortest distance.
        /// </summary>
        /// <returns></returns>
        private T GetNodeWithSmallestDistance()
        {
            double distance = double.MaxValue;
            T smallest = default(T);

            foreach (T node in Basis)
            {
                if (Dist[node] < distance)
                {
                    distance = Dist[node];
                    smallest = node;
                }
            }
            return smallest;
        }
        

    }
}