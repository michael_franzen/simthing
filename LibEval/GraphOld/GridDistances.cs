﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GridDistances.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using Tektosyne.Collections;
using Tektosyne.Geometry;
using Tektosyne.Graph;

namespace CPlan.Evaluation
{
    /// <summary>
    /// Computes a visiblity analysis.
    /// <remarks><para>
    /// With this analysis, for each place in a grid the area that is visible from this place is computed (connectivity).
    /// Furthermore the distances from one place to all the visible places are computed.
    /// </para></remarks>
    [Serializable]
    public class GridDistances
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary> Tektosyne <see cref="IGraph2D"/> list, that represents the grid graph. </summary>
        public readonly IGraph2D<PointI> Graph;
        /// <summary> Holds the walls. Based on them the visibility is computed. </summary>
       // public List<GLine> Walls = new List<GLine>();
        /// <summary> <see cref="ArrayEx"/> instance to hold the computed distance from walls values </summary>
        public ArrayEx<Single> DistWalls;
        # endregion

        //############################################################################################################################################################################
        # region Constructors
        /// <summary>
        /// Initialise a new instance of the <b>AVisibility</b> class .
        /// </summary>
        /// <param name="graph">A graph corresponding to a <see cref="GGrid"/>.</param>
        /// <param name="walls">A list of walls as list of <see cref="GLine"/> instances.</param>
        /// <param name="resolution">The size of the analysed grid  in rows and columns</param>
        //public GridDistances(IGraph2D<PointI> graph, List<GLine> walls, SizeI resolution)
        //{
        //    if (graph == null)
        //        ThrowHelper.ThrowArgumentNullException("graph");
        //    Graph = graph;
        //    Walls = walls;

        //    DistWalls = new ArrayEx<Single>(resolution.Width, resolution.Height);
        //}

        # endregion

        //############################################################################################################################################################################
        //# region Methods
        ///// <summary>
        ///// Computes the visibility values for the grid/graph.
        ///// </summary>
        ///// <remarks><para>
        ///// The results of this computation are stored in the Counter and Distance <see cref="ArrayEx"/>.
        ///// </para></remarks>
        ///// <param name="progressBar">For the use of a ToolStripProgressBar. 
        ///// If there is no ProgressBar use "null" as parameter.</param>
        //public void Evaluate(ToolStripProgressBar progressBar)
        //{
        //    if (progressBar != null)
        //    {
        //        progressBar.Visible = true;
        //        progressBar.Value = 100;
        //    }

        //    GLine curWall;
        //    // --- reset the counting value -----------------------------
        //    for (int i = 0; i < DistWalls.Count; i++) DistWalls.SetValue(0, i);

        //    // ----------------------------------------------------------
        //    Single pow = 0.7f; // to power the min distance for visualisatzion purposes
        //    //Parallel.For(0, DistWalls.Count, delegate(int i)
        //    for (int i = 0; i < DistWalls.Count; i++)
        //    {
        //        double minDist, curDist;
        //        PointD tempPt = Graph.GetWorldLocation(Graph.Nodes.ElementAt(i));
        //        Vector2d curPt = new Vector2d(tempPt.X, tempPt.Y);
        //        minDist = double.MaxValue;
        //        for (int m = 0; m < Walls.Count; m++)
        //        {
        //            curWall = Walls[m];
        //            curDist = curWall.Line.DistanceToPoint(curPt);
        //            if (minDist > curDist)
        //            {
        //                minDist = curDist;
        //            }
        //        }
        //        DistWalls.SetValue(Convert.ToSingle(Math.Pow(minDist, pow)), i);
        //        Application.DoEvents(); // makes the progress of the progressBar visible
        //    } //);

        //    if (progressBar != null)
        //    {
        //        progressBar.Value = 0;
        //        progressBar.Visible = false;
        //    }
        //}

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Get the normalised DistWalls value of all grid elements.
        ///// </summary>
        ///// <remarks>
        ///// It is useful for the definition of the color values of the regular polygons
        ///// of the corresponding <see cref="GGrid"/>.
        ///// </remarks>
        ///// <returns>
        ///// The normalise DistWalls values as <see cref="float[,]"/> array.
        ///// </returns>
        //public Single[,] GetNormDistWallsArray()
        //{
        //    CollectionsExtend.CutMinValue(DistWalls, Fortran.Min(DistWalls.ToArray<Single>()));
        //    CollectionsExtend.Normalize(DistWalls);
        //    CollectionsExtend.IntervalExtension(DistWalls, Fortran.Max(DistWalls.ToArray<Single>()));
        //    return CollectionsExtend.CopyTo2dArray(DistWalls);
        //}

        //# endregion

    }
}
