﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GridVisibility.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Linq;
using CPlan.Geometry;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;
using Tektosyne.Graph;

namespace CPlan.Evaluation
{
    /// <summary>
    /// Computes a visiblity analysis.
    /// <remarks><para>
    /// With this analysis, for each place in a grid the area that is visible from this place is computed (connectivity).
    /// Furthermore the distances from one place to all the visible places are computed.
    /// </para></remarks>
    [Serializable]
    public class GridVisibility
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary> Tektosyne <see cref="IGraph2D"/> list, that represents the grid graph. </summary>
        public readonly IGraph2D<PointI> Graph;
        /// <summary> Holds the walls. Based on them the visibility is computed. </summary>
      //  public List<GLine> Walls = new List<GLine>();
        /// <summary> <see cref="ArrayEx"/> instance to hold the computed area visibility values </summary>
        public ArrayEx<Single> Counter;
        /// <summary> <see cref="ArrayEx"/> instance to hold the computed distance visibility values </summary>
        public ArrayEx<Single> Distances;
        # endregion

        //############################################################################################################################################################################
        # region Constructors
        /// <summary>
        /// Initialise a new instance of the <b>AVisibility</b> class .
        /// </summary>
        /// <param name="graph">A graph corresponding to a <see cref="GGrid"/>.</param>
        /// <param name="walls">A list of walls as list of <see cref="GLine"/> instances.</param>
        /// <param name="resolution">The size of the analysed grid in rows and columns</param>
        //public GridVisibility(IGraph2D<PointI> graph, List<GLine> walls, SizeI resolution)
        //{
        //    if (graph == null)
        //        ThrowHelper.ThrowArgumentNullException("graph");
        //    Graph = graph;
        //    Walls = walls;
        //    Counter = new ArrayEx<Single>(resolution.Width, resolution.Height);
        //    Distances = new ArrayEx<Single>(resolution.Width, resolution.Height);
        //}

        # endregion

        //############################################################################################################################################################################
        # region Methods
        /// <summary>
        /// Computes the visibility values for the grid/graph.
        /// </summary>
        /// <remarks><para>
        /// The results of this computation are stored in the GridValues.
        /// </para></remarks>
        /// <param name="progressBar">For the use of a ToolStripProgressBar. 
        /// If there is no ProgressBar use "null" as parameter.</param>
        //public void Evaluate(ToolStripProgressBar progressBar)
        //{
        //    if (progressBar != null)
        //    {
        //        progressBar.Visible = true;
        //        progressBar.Value = 100;
        //    }

        //    GLine curWall;
        //    // --- reset the counting value -----------------------------
        //    for (int i = 0; i < Counter.Count; i++) 
        //    {
        //        Counter.SetValue(0, i);
        //        Distances.SetValue(0, i);
        //    }

        //    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //    /*int sum = 0;
        //    Parallel.For(0, 100, delegate(int i) {
        //      if (isPrime(i)) {
        //        lock (this) { sum += i; }
        //      }
        //    });
            
        //    int sum = Parallel.Aggregate(0, 100, // iteration domain
        //      0,                     // initial value
        //      delegate(int i) { return (isPrime(i) ? i : 0) }, // apply 
        //                                              // on each element
        //      delegate(int x, int y) { return x+y; }  // combine results
        //    );*/

        //    // ----------------------------------------------------------
        //    //Parallel.For(0, Counter.Count, delegate(int i)
        //    for (int i = 0; i < Counter.Count; i++)
        //    {
        //        LineIntersection sPt = new LineIntersection();
        //        for (int k = i + 1; k < Counter.Count; k++)
        //        {
        //            GLine viewshaft = new GLine(Graph.GetWorldLocation(Graph.Nodes.ElementAt(i)).ToVector2D(), Graph.GetWorldLocation(Graph.Nodes.ElementAt(k)).ToVector2D());
        //            for (int m = 0; m < Walls.Count; m++)
        //            {
        //                curWall = Walls[m];
        //                sPt = LineIntersection.Find(viewshaft.Start.ToPointD(), viewshaft.End.ToPointD(), curWall.Start.ToPointD(), curWall.End.ToPointD());
        //                if (sPt.Exists) break;
        //            }

        //            if (sPt.Exists == false)
        //            {
        //                Counter.SetValue(Counter.GetValue(i) + 1, i);
        //                Counter.SetValue(Counter.GetValue(k) + 1, k);
        //                Single length = (Single)viewshaft.Line.Length;
        //                Distances.SetValue(Distances.GetValue(i) + length, i);
        //                Distances.SetValue(Distances.GetValue(k) + length, k);
        //            }
        //            Application.DoEvents(); // makes the progress of the progressBar visible
        //        }
        //    }//);

        //    if (progressBar != null)
        //    {
        //        progressBar.Value = 0;
        //        progressBar.Visible = false;
        //    }
        //}

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Counter value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Counter values as <see cref="float[,]"/> array.
        /// </returns>
        public Single[,] GetNormCounterArray()
        {
            Tools.CutMinValue(Counter, Fortran.Min(Counter.ToArray<Single>()));
            Tools.Normalize(Counter);
            Tools.IntervalExtension(Counter, Fortran.Max(Counter.ToArray<Single>()));
            return Extensions.CopyTo2dArray(Counter);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Distances value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Distances values as <see cref="float[,]"/> array.
        /// </returns>
        public Single[,] GetNormDistancesArray()
        {
            Tools.CutMinValue(Distances, Fortran.Min(Distances.ToArray<Single>()));
            Tools.Normalize(Distances);
            Tools.IntervalExtension(Distances, Fortran.Max(Distances.ToArray<Single>()));
            return Extensions.CopyTo2dArray(Distances);
        }
        # endregion
    }
}
