﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FreeGraph.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;
using Tektosyne.Graph;

namespace CPlan.Evaluation
{
    [Serializable]
    public class FreeGraph : ICloneable, IGraph2D<PointD>
    {

        /// The list of all half-edges in the <see cref="Subdivision"/>, sorted by key.</summary>
        private readonly SortedListEx<Int32, FreeEdge> _edges;

        /// <summary>
        /// The lexicographically sorted list of all vertices in the <see cref="Subdivision"/> and
        /// one of their incident half-edges.</summary>
        private readonly SortedListEx<PointD, FreeEdge> _vertices;

        /// <summary>
        /// The maximum number of half-edges originating from any vertex.</summary>
        private int _connectivity;

        /// <summary>
        /// The unique key for the next <see cref="SubdivisionEdge"/> instance.</summary>
        private int _nextEdgeKey;

        /// <summary>
        /// The epsilon used for coordinate comparisons within the <see cref="Vertices"/>
        /// collection.</summary>
        private double _epsilon;

        /// <summary>
        /// The <see cref="IGraph2D{T}"/> regions associated with all vertices, if any.</summary>
        private readonly Dictionary<PointD, PointD[]> _vertexRegions;
        

        #region FreeGraph(Int32, Int32)

        /// <summary>
        /// Initializes a new instance of the <see cref="Subdivision"/> class with the specified
        /// initial capacities.</summary>
        /// <param name="edgeCapacity">
        /// The initial <see cref="SortedListEx{TKey, TValue}.Capacity"/> for the <see
        /// cref="Edges"/> collection.</param>
        /// <param name="faceCapacity">
        /// The initial <see cref="SortedListEx{TKey, TValue}.Capacity"/> for the <see
        /// cref="Faces"/> collection.</param>
        /// <param name="vertexCapacity">
        /// The initial <see cref="SortedListEx{TKey, TValue}.Capacity"/> for the <see
        /// cref="Vertices"/> collection.</param>
        /// <remarks>
        /// The <see cref="Edges"/> and <see cref="Vertices"/> properties are initialized to empty
        /// collections with the specified initial capacities. The <see cref="Faces"/> property is
        /// initialized to a collection that contains only the unbounded face and has the specified
        /// initial capacity.</remarks>

        public FreeGraph(int edgeCapacity, int vertexCapacity)
        {
            _edges = new SortedListEx<Int32, FreeEdge>(edgeCapacity);
            _vertices = new SortedListEx<PointD, FreeEdge>(vertexCapacity, new PointDComparerY());
        }

        #endregion
        #region FreeGraph()
        public FreeGraph()
        {
            _edges = new SortedListEx<Int32, FreeEdge>();
            _vertices = new SortedListEx<PointD, FreeEdge>(new PointDComparerY());

        }
        #endregion
        #region Edges

        /// <summary>
        /// Gets a read-only view of all half-edges in the <see cref="Subdivision"/>.</summary>
        /// <value>
        /// A read-only <see cref="SortedListEx{TKey, TValue}"/> that maps the <see
        /// cref="SubdivisionEdge.Key"/> of each half-edge in the <see cref="Subdivision"/> to the
        /// corresponding <see cref="SubdivisionEdge"/> object.</value>
        /// <remarks><para>
        /// <b>Edges</b> always contains an even number of elements since every edge in the <see
        /// cref="Subdivision"/> is comprised of two <see cref="SubdivisionEdge"/> objects.
        /// </para><para>
        /// <b>Edges</b> is provided for convenience, unit testing, and faster edge scanning. This
        /// collection is not strictly needed since a list of all half-edges is easily obtained by
        /// iterating over all <see cref="Vertices"/>, e.g. using <see cref="GetEdgesByOrigin"/>.
        /// </para><para>
        /// Maintaining the <b>Edges</b> collection consumes little extra runtime but a significant
        /// amount of memory, so an alternative <see cref="Subdivision"/> implementation might
        /// choose to remove this collection and create a new list of half-edges where necessary.
        /// </para></remarks>

        public SortedListEx<Int32, FreeEdge> Edges
        {
            //[DebuggerStepThrough]
            get { return _edges.AsReadOnly(); }
        }

        #endregion
        #region Epsilon

        /// <summary>
        /// Gets or sets the epsilon used for coordinate comparisons within the <see
        /// cref="Vertices"/> collection.</summary>
        /// <value><para>
        /// The maximum absolute difference at which vertex coordinates should be considered equal.
        /// </para><para>-or-</para><para>
        /// Zero to use exact coordinate comparisons. The default is zero.</para></value>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The property is set to a negative value.</exception>
        /// <exception cref="PropertyValueException">
        /// The property is changed, and <see cref="Vertices"/> is not an empty collection.
        /// </exception>
        /// <remarks><para>
        /// <b>Epsilon</b> returns the comparison epsilon used by the <see cref="SortedListEx{TKey,
        /// TValue}.Comparer"/> that establishes the lexicographical ordering of <see
        /// cref="PointD"/> keys within the <see cref="Vertices"/> collection.
        /// </para><para>
        /// <b>Epsilon</b> cannot be set if the <see cref="Vertices"/> collection already contains
        /// one or more elements. Factory methods that create a new <see cref="Subdivision"/>
        /// typically offer a parameter to set this property upon construction.
        /// </para><note type="caution">
        /// Do not attempt to directly change the <see cref="PointDComparerY.Epsilon"/> of the <see
        /// cref="Vertices"/> comparer! This will cause incorrect search results and data corruption
        /// if the <see cref="Vertices"/> collection was not empty. Moreover, the <see
        /// cref="Subdivision"/> caches the current <b>Epsilon</b> internally, and changing the
        /// comparer epsilon directly will not update the cached value.</note></remarks>

        public double Epsilon
        {
            get { return _epsilon; }
            set
            {
                if (_epsilon == value) return;

                if (_vertices.Count > 0)
                    ThrowHelper.ThrowPropertyValueException("Vertices", Strings.PropertyNotEmpty);

                ((PointDComparerY)_vertices.Comparer).Epsilon = _epsilon = value;
            }
        }

        #endregion
        #region IsEmpty

        /// <summary>
        /// Gets a value indicating whether the <see cref="Subdivision"/> is empty.</summary>
        /// <value>
        /// <c>true</c> if the <see cref="Edges"/> collection is empty; otherwise, <c>false</c>.
        /// </value>
        /// <remarks>
        /// For any valid <see cref="Subdivision"/>, the <see cref="Edges"/> collection is empty
        /// exactly if the <see cref="Vertices"/> collection is also empty, and the <see
        /// cref="Faces"/> collection contains only the unbounded face.</remarks>

        public bool IsEmpty
        {
            get { return (_edges.Count == 0); }
        }

        #endregion
        #region ToLines

        /// <summary>
        /// Converts all edges in the <see cref="Subdivision"/> to line segments.</summary>
        /// <returns>
        /// An <see cref="Array"/> of <see cref="LineD"/> instances that represent all <see
        /// cref="Edges"/> in the <see cref="Subdivision"/>.</returns>
        /// <remarks><para>
        /// The <see cref="LineD"/> elements of the returned <see cref="Array"/> are sorted by the
        /// smaller <see cref="SubdivisionEdge.Key"/> of the two <see cref="SubdivisionEdge"/>
        /// objects that constitute each edge, using the same ordering as the <see cref="Edges"/>
        /// collection.
        /// </para><para>
        /// The returned <see cref="Array"/> has half as many elements as the <see cref="Edges"/>
        /// collection since each <see cref="LineD"/> comprises two half-edges.</para></remarks>

        public LineD[] ToLines()
        {
            LineD[] lines = new LineD[_edges.Count / 2];
            int lineIndex = 0;

            // iterate over half-edges by ascending keys
            for (int i = 0; i < _edges.Count; i++)
            {
                FreeEdge edge = _edges.GetByIndex(i);
                FreeEdge twin = edge._twin;

                // add full edge when encountering first twin only;
                // that is, when current key is less than twin key
                if (edge._key < twin._key)
                    lines[lineIndex++] = new LineD(edge._origin, twin._origin);
            }

            //Debug.Assert(lineIndex == lines.Length);
            return lines;
        }

        #endregion
        #region VertexRegions

        /// <summary>
        /// Gets a dictionary that maps <see cref="Vertices"/> to <see cref="IGraph2D{T}"/> regions.
        /// </summary>
        /// <value>
        /// A <see cref="Dictionary{TKey, TValue}"/> that maps <see cref="Vertices"/> elements, i.e.
        /// <see cref="IGraph2D{T}"/> nodes, to <see cref="IGraph2D{T}"/> regions. The default is an
        /// empty collection.</value>
        /// <remarks><para>
        /// <b>VertexRegions</b> always returns an empty collection by default, as a planar <see
        /// cref="Subdivision"/> does not inherently associate regions with <see cref="Vertices"/>.
        /// Clients must explicitly add any desired key-and-value pairs.
        /// </para><para>
        /// <see cref="GetWorldRegion"/> attempts to return the polygonal region that 
        /// <b>VertexRegions</b> associates with a specified <see cref="Vertices"/> element, i.e.
        /// <see cref="IGraph2D{T}"/> node, before returning a null reference.</para></remarks>

        public Dictionary<PointD, PointD[]> VertexRegions
        {
            //[DebuggerStepThrough]
            get { return _vertexRegions; }
        }

        #endregion
        #region Vertices

        /// <summary>
        /// Gets a read-only view of all vertices in the <see cref="Subdivision"/>.</summary>
        /// <value>
        /// A read-only <see cref="SortedListEx{TKey, TValue}"/> that maps the <see cref="PointD"/>
        /// coordinates of all vertices in the <see cref="Subdivision"/> to a <see
        /// cref="SubdivisionEdge"/> that originates with the vertex.</value>
        /// <remarks><para>
        /// <b>Vertices</b> is sorted lexicographically by <see cref="PointD"/> keys, using the
        /// ordering established by the <see cref="PointDComparerY"/> class. That is, keys are
        /// sorted first by <see cref="PointD.Y"/> and then by <see cref="PointD.X"/> coordinates.
        /// </para><para>
        /// If multiple <see cref="SubdivisionEdge"/> instances originate from the same vertex, one
        /// is selected arbitrarily for the <b>Vertices</b> collection, depending on the
        /// construction order of the <see cref="Subdivision"/>.
        /// </para><para>
        /// Every <b>Vertices</b> key is associated with a valid <see cref="SubdivisionEdge"/>; that
        /// is, a <see cref="Subdivision"/> never contains isolated points, only edges.
        /// </para></remarks>

        public SortedListEx<PointD, FreeEdge> Vertices
        {
            //[DebuggerStepThrough]
            get { return _vertices.AsReadOnly(); }
        }

        #endregion

        #region Public Methods
        #region AddEdge
        /// <summary>
        /// Adds a new edge to the <see cref="Subdivision"/> that connects the specified vertices,
        /// and returns information on the changed and added <see cref="Faces"/>.</summary>
        /// <param name="start">
        /// The <see cref="PointD"/> coordinates of the first vertex to connect.</param>
        /// <param name="end">
        /// The <see cref="PointD"/> coordinates of the second vertex to connect.</param>
        /// <param name="changedFace">
        /// Returns the <see cref="SubdivisionFace.Key"/> of the <see cref="SubdivisionFace"/> that
        /// was changed by the edge creation, if any; otherwise, -1.</param>
        /// <param name="addedFace">
        /// Returns the <see cref="SubdivisionFace.Key"/> of the <see cref="SubdivisionFace"/> that
        /// was added along with the edge, if any; otherwise, -1.</param>
        /// <returns>
        /// On success, the new <see cref="SubdivisionEdge"/> from <paramref name="start"/> to
        /// <paramref name="end"/>; otherwise, a null reference.</returns>
        /// <remarks>
        /// Please refer to the basic <see cref="AddEdge(PointD, PointD)"/> overload for details.
        /// </remarks>
        public FreeEdge AddEdge( PointD start, PointD end)
        {

            int startIndex = _vertices.IndexOfKey(start);
            int endIndex = _vertices.IndexOfKey(end);
        
            if (start == end) return null;

        /*    FreeEdge nextStartEdge = null, nextEndEdge = null;

            if (startIndex < 0 && endIndex < 0) {            
                //      face = FindFace(start);
            }
            else
            {
                FreeEdge vertexEdge = _vertices.GetByIndex(startIndex);
                FreeEdge previousStartEdge = null, previousEndEdge = null;

                // check for existing edge connecting vertices
                if (startIndex >= 0 && endIndex >= 0)
                    foreach (FreeEdge edge in vertexEdge.OriginEdges)
                        if (edge._twin._origin == end) return null;

                // find neighboring edges in start vertex chain
                if (startIndex >= 0)
                {
                    vertexEdge.FindEdgePosition(end, out nextStartEdge, out previousStartEdge);
                    Debug.Assert(nextStartEdge._previous == previousStartEdge._twin);
                }

                // find neighboring edges in end vertex chain
                if (endIndex >= 0)
                {
                    vertexEdge = _vertices.GetByIndex(endIndex);
                    vertexEdge.FindEdgePosition(start, out nextEndEdge, out previousEndEdge);
                    Debug.Assert(nextEndEdge._previous == previousEndEdge._twin);
                }

                Debug.Assert(nextStartEdge != nextEndEdge);
                Debug.Assert(previousStartEdge != previousEndEdge);
            }*/

            LineD line = new LineD(start, end);          

            // create edges and (if necessary) vertices
            FreeEdge startEdge;
            CreateTwinEdges(start, end, out startEdge);

            _connectivity = 0;
            return startEdge;
        }
        #endregion
        #region FindEdge

        /// <summary>
        /// Finds the half-edge in the <see cref="Subdivision"/> with the specified origin and
        /// destination.</summary>
        /// <param name="origin">
        /// The <see cref="SubdivisionEdge.Origin"/> of the half-edge.</param>
        /// <param name="destination">
        /// The <see cref="SubdivisionEdge.Destination"/> of the half-edge.</param>
        /// <returns><para>
        /// The <see cref="SubdivisionEdge"/> with the specified <paramref name="origin"/> and
        /// <paramref name="destination"/>.
        /// </para><para>-or-</para><para>
        /// A null reference if no matching <see cref="SubdivisionEdge"/> was found.
        /// </para></returns>
        /// <remarks><para>
        /// <b>FindEdge</b> first attempts to find the specified <paramref name="origin"/> in the
        /// <see cref="Vertices"/> collection, and then calls <see
        /// cref="SubdivisionEdge.GetEdgeTo"/> to find the half-edge leading from <paramref
        /// name="origin"/> to the specified <paramref name="destination"/>.
        /// </para><para>
        /// This is an O(ld n + m) operation, where n is the number of <see cref="Vertices"/> and m
        /// is the number of half-edges originating from <paramref name="origin"/>. All coordinate
        /// comparisons use the current <see cref="Epsilon"/>.</para></remarks>

        public FreeEdge FindEdge(PointD origin, PointD destination)
        {

            FreeEdge edge;
            if (!_vertices.TryGetValue(origin, out edge))
                return null;

            return (_epsilon == 0 ?
                edge.GetEdgeTo(destination) :
                edge.GetEdgeTo(destination, _epsilon));
        }

        #endregion
        #region FindNearestVertex

        /// <summary>
        /// Finds the vertex in the <see cref="Subdivision"/> that is nearest to the specified
        /// coordinates.</summary>
        /// <param name="q">
        /// The <see cref="PointD"/> coordinates to locate.</param>
        /// <returns>
        /// The zero-based index of <paramref name="q"/> in <see cref="Vertices"/>, if found;
        /// otherwise, the zero-based index of the <see cref="Vertices"/> key with the smallest
        /// Euclidean distance to <paramref name="q"/>.</returns>
        /// <remarks>
        /// <b>FindNearestVertex</b> returns the result of <see cref="PointDComparerY.FindNearest"/>
        /// for the <see cref="PointDComparerY"/> used to sort the <see cref="Vertices"/>
        /// collection. This is usually an O(ld n) operation, where n in the number of <see
        /// cref="Vertices"/>.</remarks>

        public int FindNearestVertex(PointD q)
        {
            PointDComparerY comparer = (PointDComparerY)_vertices.Comparer;
            return comparer.FindNearest(_vertices.Keys, q);
        }

        #endregion
        #region FromLines

        /// <summary>
        /// Creates a <see cref="Subdivision"/> from the specified line segments.</summary>
        /// <param name="lines">
        /// An <see cref="Array"/> of <see cref="LineD"/> instances that represent the <see
        /// cref="Edges"/> in the new <see cref="Subdivision"/>.</param>
        /// <param name="epsilon"><para>
        /// The maximum absolute difference at which coordinates should be considered equal.
        /// </para><para>-or-</para><para>
        /// Zero to use exact coordinate comparisons. The default is zero.</para></param>
        /// <returns>
        /// A new <see cref="Subdivision"/> instance whose <see cref="Edges"/> are the specified
        /// <paramref name="lines"/>, and whose <see cref="Vertices"/> are their <see
        /// cref="LineD.Start"/> and <see cref="LineD.End"/> points.</returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="lines"/> contains an element whose <see cref="LineD.Start"/> point
        /// equals its <see cref="LineD.End"/> point.</exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="lines"/> is a null reference.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="epsilon"/> is less than zero.</exception>
        /// <remarks><para>
        /// <b>FromLines</b> also determines the <see cref="Faces"/> that are formed by the <see
        /// cref="Edges"/> of the new <see cref="Subdivision"/>, and sets its comparison <see
        /// cref="Epsilon"/> to the specified <paramref name="epsilon"/>. The new <see
        /// cref="Subdivision"/> is empty if <paramref name="lines"/> is an empty array.
        /// </para><note type="caution">
        /// The specified <paramref name="lines"/> must not intersect or overlap anywhere except in
        /// their <see cref="LineD.Start"/> and <see cref="LineD.End"/> points. <b>FromLines</b>
        /// does not check this condition. If violated, the returned <see cref="Subdivision"/> will
        /// be invalid.</note></remarks>

        public static FreeGraph FromLines(LineD[] lines, double epsilon = 0)
        {
            if (lines == null)
                ThrowHelper.ThrowArgumentNullException("lines");

            FreeGraph division;
            if (lines.Length == 0)
            {
                division = new FreeGraph();
                division.Epsilon = epsilon;
            }
            else
            {
                division = new FreeGraph(lines.Length * 2, lines.Length);
                division.Epsilon = epsilon;
                division.CreateAllFromLines(lines);
            }

            return division;
        }

        #endregion
        #region GetEdgesByOrigin

        /// <summary>
        /// Returns a list of all half-edges in the <see cref="Subdivision"/>, lexicographically
        /// sorted by origin.</summary>
        /// <returns>
        /// An <see cref="Array"/> containing all <see cref="Edges"/>, but sorted lexicographically
        /// by their <see cref="SubdivisionEdge.Origin"/> coordinates rather than by <see
        /// cref="SubdivisionEdge.Key"/>.</returns>
        /// <remarks>
        /// <b>GetEdgesByOrigin</b> does not use the <see cref="Edges"/> collection, but rather
        /// scans the <see cref="Vertices"/> collection by lexicographically ascending coordinates.
        /// All half-edges originating from the same vertex are stored in consecutive index
        /// positions, proceeding clockwise around the vertex.</remarks>

        public FreeEdge[] GetEdgesByOrigin()
        {
            var edges = new FreeEdge[_edges.Count];
            int edgeIndex = 0;

            foreach (FreeEdge firstEdge in _vertices.Values)
            {
                FreeEdge edge = firstEdge;
                do
                {
                    edges[edgeIndex++] = edge;
                    edge = edge._twin._next;
                } while (edge != firstEdge);
            }

            //Debug.Assert(edgeIndex == edges.Length);
            return edges;
        }

        #endregion
        #region CreateAllFromLines

        /// <summary>
        /// Initializes the <see cref="Edges"/>, <see cref="Faces"/>, and <see cref="Vertices"/>
        /// collections from the specified line segments.</summary>
        /// <param name="lines">
        /// An <see cref="Array"/> of <see cref="LineD"/> instances that represent the <see
        /// cref="Edges"/> added to the <see cref="Subdivision"/>.</param>
        /// <exception cref="ArgumentException">
        /// <paramref name="lines"/> contains an element whose <see cref="LineD.Start"/> point
        /// equals its <see cref="LineD.End"/> point.</exception>
        /// <remarks>
        /// <b>CreateAllFromLines</b> requires that the <see cref="Edges"/> and <see
        /// cref="Vertices"/> collections are empty, and that the <see cref="Faces"/> collection
        /// contains only the unbounded face. They will be filled with the data extracted from the
        /// specified <paramref name="lines"/> when the method returns.</remarks>

        private void CreateAllFromLines(LineD[] lines)
        {
            //Debug.Assert(_edges.Count == 0);
            //Debug.Assert(_vertices.Count == 0);

            // convert all lines into half-edges
            FreeEdge edge;
            foreach (LineD line in lines)
                CreateTwinEdges(line.Start, line.End, out edge);
        }

        #endregion
        #region CreateTwinEdges

        /// <summary>
        /// Finds or creates both twin half-edges between the specified <see cref="PointD"/>
        /// coordinates.</summary>
        /// <param name="start">
        /// The <see cref="SubdivisionEdge.Origin"/> of the first half-edge, and the <see
        /// cref="SubdivisionEdge.Destination"/> of the second half-edge.</param>
        /// <param name="end">
        /// The <see cref="SubdivisionEdge.Origin"/> of the second half-edge, and the <see
        /// cref="SubdivisionEdge.Destination"/> of the first half-edge.</param>
        /// <param name="startEdge">
        /// Returns the <see cref="SubdivisionEdge"/> from <paramref name="start"/> to <paramref
        /// name="end"/>.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="startEdge"/> is a newly created <see
        /// cref="SubdivisionEdge"/>; <c>false</c> if <paramref name="startEdge"/> is an existing
        /// <see cref="Edges"/> element.</returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="start"/> equals <paramref name="end"/>.</exception>
        /// <remarks><para>
        /// <b>CreateTwinEdges</b> adds the specified <paramref name="start"/> and <paramref
        /// name="end"/> coordinates to the <see cref="Vertices"/> collection if they are not
        /// already present. Otherwise, the <see cref="SubdivisionEdge.Origin"/> of a newly created
        /// half-edge is set to the corresponding <see cref="Vertices"/> element, which may differ
        /// from the specified <paramref name="start"/> or <paramref name="end"/> coordinates if the
        /// current comparison <see cref="Epsilon"/> is positive.
        /// </para><para>
        /// <b>CreateTwinEdges</b> does not change the <see cref="Subdivision"/> if the <see
        /// cref="Edges"/> collection already contains twin half-edges between <paramref
        /// name="start"/> and <paramref name="end"/>.</para></remarks>

        private bool CreateTwinEdges(PointD start, PointD end, out FreeEdge startEdge)
        {
            if (start == end)
                ThrowHelper.ThrowArgumentExceptionWithFormat("lines", Strings.ArgumentContainsEmpty, "LineD");

            // search for vertices at start/end coordinates
            FreeEdge oldStartEdge, oldEndEdge;
            _vertices.TryGetValue(start, out oldStartEdge);
            _vertices.TryGetValue(end, out oldEndEdge);

            // both vertices exist: check if half-edges also exist
            if (oldStartEdge != null && oldEndEdge != null)
            {
                startEdge = oldStartEdge;
                do
                {
                    if (startEdge._twin._origin == oldEndEdge._origin)
                        return false;

                    startEdge = startEdge._twin._next;
                } while (startEdge != oldStartEdge);
            }

            // create twin edges for line segment
            startEdge = new FreeEdge(_nextEdgeKey++);
            _edges.Add(startEdge._key, startEdge);

            FreeEdge endEdge = new FreeEdge(_nextEdgeKey++);
            _edges.Add(endEdge._key, endEdge);

            // initialize Twin pointers (unchanged in this method)
            startEdge._twin = endEdge; endEdge._twin = startEdge;

            // initialize Next and Previous pointers
            // (may change if edges are linked to existing vertices)
            startEdge._next = startEdge._previous = endEdge;
            endEdge._next = endEdge._previous = startEdge;

            /*
             * Vertices might be performing searches with a non-zero epsilon, which means that
             * start/end might not exactly equal the vertex that is reported as identical.
             * Therefore, if a vertex already exists, we must assign the existing coordinates
             * to the half-edge’s Origin, and not the specified start/end coordinates.
             */

            // create or expand origin of start half-edge
            if (oldStartEdge == null)
            {
                startEdge._origin = start;
                _vertices.Add(start, startEdge);
            }
            else
                startEdge._origin = oldStartEdge._origin;

            // create or expand origin of end half-edge
            if (oldEndEdge == null)
            {
                endEdge._origin = end;
                _vertices.Add(end, endEdge);
            }
            else
                endEdge._origin = oldEndEdge._origin;

            // create links only after both Origins are set
            if (oldStartEdge != null) InsertAtEdge(startEdge, oldStartEdge);
            if (oldEndEdge != null) InsertAtEdge(endEdge, oldEndEdge);

            return true;
        }

        #endregion
        #region InsertAtEdge

        /// <summary>
        /// Inserts the specified <see cref="SubdivisionEdge"/> into the vertex chain that contains
        /// another specified instance.</summary>
        /// <param name="edge">
        /// The <see cref="SubdivisionEdge"/> to insert into the same vertex chain that contains
        /// <paramref name="oldEdge"/>.</param>
        /// <param name="oldEdge">
        /// A <see cref="SubdivisionEdge"/> that is already linked into the vertex chain around its
        /// <see cref="SubdivisionEdge.Origin"/>.</param>
        /// <remarks>
        /// <b>InsertAtEdge</b> requires that <paramref name="edge"/> and <paramref name="oldEdge"/>
        /// both have the same <see cref="SubdivisionEdge.Origin"/>.</remarks>

        private static void InsertAtEdge(FreeEdge edge, FreeEdge oldEdge)
        {
            //Debug.Assert(edge != oldEdge);
            //Debug.Assert(edge._origin == oldEdge._origin);

            if (oldEdge._previous == oldEdge._twin)
            {
                // simple case: no other edge linked to old edge
                edge._previous = oldEdge._twin;
                edge._twin._next = oldEdge;
            }
            else
            {
                // find position of edge in existing vertex chain
                FreeEdge nextEdge, previousEdge;
                oldEdge.FindEdgePosition(edge._twin._origin, out nextEdge, out previousEdge);
                edge._previous = previousEdge._twin;
                edge._twin._next = nextEdge;
            }

            // establish double-link invariants
            edge._previous._next = edge;
            edge._twin._next._previous = edge._twin;
        }

        #endregion
        #endregion

        #region IGraph2D<T> Members
        #region Connectivity

        /// <summary>
        /// Gets the maximum number of direct neighbors for any <see cref="IGraph2D{T}"/> node.
        /// </summary>
        /// <value>
        /// A positive <see cref="Int32"/> value indicating the maximum number of direct neighbors
        /// for any given <see cref="IGraph2D{T}"/> node.</value>
        /// <remarks><para>
        /// <b>Connectivity</b> returns the maximum number of half-edges that originate from any
        /// single <see cref="Vertices"/> element.
        /// </para><para>
        /// <b>Connectivity</b> scans the entire <see cref="Vertices"/> collection to determine its
        /// value on first access, and returns a cached value on subsequent accesses. The scan is
        /// repeated whenever the structure of the <see cref="Subdivision"/> changes.
        /// </para></remarks>

        public int Connectivity
        {
            get
            {
                if (_connectivity == 0 && _vertices.Count > 0)
                {
                    foreach (FreeEdge firstEdge in _vertices.Values)
                    {
                        int count = 0;

                        // count half-edges at current vertex
                        var edge = firstEdge;
                        do
                        {
                            ++count;
                            edge = edge._twin._next;
                        } while (edge != firstEdge);

                        if (_connectivity < count)
                            _connectivity = count;
                    }
                }

                return _connectivity;
            }
        }

        #endregion
        #region NodeCount

        /// <summary>
        /// Gets the total number of <see cref="Nodes"/> in the <see cref="IGraph2D{T}"/>.</summary>
        /// <value>
        /// The total number of <see cref="Nodes"/> in the <see cref="IGraph2D{T}"/>.</value>
        /// <remarks>
        /// <b>NodeCount</b> returns the current number of <see cref="Vertices"/>.</remarks>

        public int NodeCount
        {
            get { return _vertices.Count; }
        }

        #endregion
        #region Nodes

        /// <summary>
        /// Gets an <see cref="IEnumerable{T}"/> collection that contains all nodes in the <see
        /// cref="IGraph2D{T}"/>.</summary>
        /// <value>
        /// An <see cref="IEnumerable{T}"/> collection that contains all nodes in the <see
        /// cref="IGraph2D{T}"/>.</value>
        /// <remarks>
        /// <b>Nodes</b> returns all <see cref="PointD"/> keys in the <see cref="Vertices"/>
        /// collection, using its current sorting order.</remarks>

        public IEnumerable<PointD> Nodes
        {
            get
            {
                foreach (PointD vertex in _vertices.Keys)
                    yield return vertex;
            }
        }

        #endregion
        #region Contains

        /// <summary>
        /// Determines whether the <see cref="IGraph2D{T}"/> contains the specified node.</summary>
        /// <param name="node">
        /// The <see cref="IGraph2D{T}"/> node to examine.</param>
        /// <returns>
        /// <c>true</c> if the <see cref="IGraph2D{T}"/> contains the specified <paramref
        /// name="node"/>; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// <b>Contains</b> returns <c>true</c> exactly if the <see cref="Vertices"/> collection
        /// contains the specified <paramref name="node"/>. This is an O(ld n) operation, where n is
        /// the number of <see cref="Vertices"/>.</remarks>

        public bool Contains(PointD node)
        {
            return _vertices.ContainsKey(node);
        }

        #endregion
        #region GetDistance

        /// <summary>
        /// Returns the distance between the two specified <see cref="IGraph2D{T}"/> nodes.
        /// </summary>
        /// <param name="source">
        /// The source node in the <see cref="IGraph2D{T}"/>.</param>
        /// <param name="target">
        /// The target node in the <see cref="IGraph2D{T}"/>.</param>
        /// <returns>
        /// The non-negative distance between <paramref name="source"/> and <paramref
        /// name="target"/>, measured in world coordinates.</returns>
        /// <remarks><para>
        /// <b>GetDistance</b> returns zero if the specified <paramref name="source"/> and <paramref
        /// name="target"/> are identical, and the Euclidean distance between <paramref
        /// name="source"/> and <paramref name="target"/> otherwise. This is equivalent to the
        /// absolute length of the edge that connects the two vertices.
        /// </para><para>
        /// <b>GetDistance</b> does not check whether the <see cref="Subdivision"/> actually
        /// contains the specified <paramref name="source"/> and <paramref name="target"/> nodes, or
        /// whether they are connected by an edge.</para></remarks>

        public double GetDistance(PointD source, PointD target)
        {
            if (source == target) return 0;

            double x = target.X - source.X;
            double y = target.Y - source.Y;

            return Math.Sqrt(x * x + y * y);
            //return 1;
        }

        #endregion
        #region GetNearestNode

        /// <summary>
        /// Gets the <see cref="IGraph2D{T}"/> node that is nearest to the specified location, in
        /// world coordinates.</summary>
        /// <param name="location">
        /// The <see cref="PointD"/> location, in world coordinates, whose nearest <see
        /// cref="IGraph2D{T}"/> node to find.</param>
        /// <returns>
        /// The <see cref="IGraph2D{T}"/> node whose <see cref="GetWorldLocation"/> result is
        /// nearest to the specified <paramref name="location"/>.</returns>
        /// <remarks>
        /// <b>GetNearestNode</b> returns the <see cref="Vertices"/> element whose index is found by
        /// <see cref="FindNearestVertex"/> for the specified <paramref name="location"/>.</remarks>

        public PointD GetNearestNode(PointD location)
        {
            int index = FindNearestVertex(location);
            return _vertices.GetKey(index);
        }

        #endregion
        #region GetNeighbors

        /// <summary>
        /// Returns all direct neighbors of the specified <see cref="IGraph2D{T}"/> node.</summary>
        /// <param name="node">
        /// The <see cref="IGraph2D{T}"/> node whose direct neighbors to return.</param>
        /// <returns>
        /// An <see cref="IList{T}"/> containing all valid <see cref="IGraph2D{T}"/> nodes that are
        /// directly connected with the specified <paramref name="node"/>. The number of elements is
        /// at most <see cref="Connectivity"/>.</returns>
        /// <remarks><para>
        /// <b>GetNeighbors</b> never returns a null reference, but it returns an empty <see
        /// cref="IList{T}"/> if the specified <paramref name="node"/> is not found in the <see
        /// cref="Vertices"/> collections.
        /// </para><para>
        /// Otherwise, <b>GetNeighbors</b> returns the destinations of all half-edges that originate
        /// from the specified <paramref name="node"/>. This is an O(ld n + m) operation, where n is
        /// the total number of <see cref="Vertices"/> and m is the number of half-edges originating
        /// from <paramref name="node"/>.</para></remarks>

        public IList<PointD> GetNeighbors(PointD node)
        {
            var neighbors = new List<PointD>();

            // find origin in vertex collection
            FreeEdge firstEdge;
            if (!_vertices.TryGetValue(node, out firstEdge))
                return new List<PointD>(0);

            // find all destinations from origin
            var edge = firstEdge;
            do
            {
                var twin = edge._twin;
                neighbors.Add(twin._origin);
                edge = twin._next;
            } while (edge != firstEdge);

            return neighbors;
        }

        #endregion
        #region GetWorldLocation

        /// <summary>
        /// Gets the location of the specified <see cref="IGraph2D{T}"/> node, in world coordinates.
        /// </summary>
        /// <param name="node">
        /// The <see cref="IGraph2D{T}"/> node whose location to return.</param>
        /// <returns>
        /// The <see cref="PointD"/> location of the specified <paramref name="node"/>, in world
        /// coordinates.</returns>
        /// <remarks>
        /// <b>GetWorldLocation</b> simply returns the specified <paramref name="node"/>, without
        /// checking whether it is actually part of the <see cref="Subdivision"/>.</remarks>

        public PointD GetWorldLocation(PointD node)
        {
            return node;
        }

        #endregion
        #region GetWorldRegion

        /// <summary>
        /// Gets the region covered by the specified <see cref="IGraph2D{T}"/> node, in world
        /// coordinates.</summary>
        /// <param name="node">
        /// The <see cref="IGraph2D{T}"/> node whose region to return.</param>
        /// <returns><para>
        /// An <see cref="Array"/> containing the <see cref="PointD"/> vertices of the polygonal
        /// region covered by the specified <paramref name="node"/>, in world coordinates.
        /// </para><para>-or-</para><para>
        /// A null reference if <paramref name="node"/> does not define a polygonal region.
        /// </para></returns>
        /// <remarks>
        /// <b>GetWorldRegion</b> returns the polygonal region that <see cref="VertexRegions"/>
        /// associates with the specified <paramref name="node"/>, if found; otherwise, a null
        /// reference.</remarks>

        public PointD[] GetWorldRegion(PointD node)
        {
            PointD[] region;
            _vertexRegions.TryGetValue(node, out region);
            return region;
        }

        #endregion
        #endregion

        #region ICloneable Members
        #region Clone

        /// <summary>
        /// Creates a deep copy of the <see cref="Subdivision"/>.</summary>
        /// <returns>
        /// A deep copy of the <see cref="Subdivision"/>.</returns>
        /// <remarks>
        /// <b>Clone</b> replicates the entire structure of the <see cref="Subdivision"/>, creating
        /// a new <see cref="SubdivisionEdge"/> and <see cref="SubdivisionFace"/> instance for each
        /// corresponding instance found in the current structure. The new instances always have the
        /// same <b>Key</b> as the existing instances.</remarks>

        public object Clone()
        {
            var division = new FreeGraph(_edges.Count, _vertices.Count);

            // set epsilon on vertex comparer
            division.Epsilon = _epsilon;

            // copy internal counters
            division._connectivity = _connectivity;
            division._nextEdgeKey = _nextEdgeKey;

            // copy edges with key and origin
            foreach (FreeEdge oldEdge in _edges.Values)
            {
                var newEdge = new FreeEdge(oldEdge._key);
                division._edges.Add(newEdge._key, newEdge);
                newEdge._origin = oldEdge._origin;
            }

            // copy vertices with new edge references
            foreach (FreeEdge oldEdge in _vertices.Values)
                division._vertices.Add(oldEdge._origin, division._edges[oldEdge._key]);

            // update edge & face references of all edges
            foreach (FreeEdge newEdge in division._edges.Values)
            {
                var oldEdge = _edges[newEdge._key];
                newEdge._twin = division._edges[oldEdge._twin._key];
                newEdge._next = division._edges[oldEdge._next._key];
                newEdge._previous = division._edges[oldEdge._previous._key];
            }

            return division;
        }

        #endregion
        #region CloneEdges

        /// <summary>
        /// Creates a deep copy of the <see cref="Subdivision"/>, except for the <see cref="Faces"/>
        /// collection.</summary>
        /// <returns>
        /// A deep copy of the <see cref="Subdivision"/>, except for the <see cref="Faces"/>
        /// collection.</returns>
        /// <remarks><para>
        /// <b>CloneEdges</b> replicates the entire structure of the <see cref="Subdivision"/>,
        /// creating a new <see cref="SubdivisionEdge"/> instance for each corresponding instance
        /// found in the current structure. The new instances always have the same <see
        /// cref="SubdivisionEdge.Key"/> as the existing instances.
        /// </para><para>
        /// Unlike <see cref="Clone"/>, the <see cref="Faces"/> collection and all related
        /// references are <em>not</em> copied. The returned <see cref="Subdivision"/> contains only
        /// the unbounded <see cref="SubdivisionFace"/>.</para></remarks>

        private FreeGraph CloneEdges()
        {
            var division = new FreeGraph(_edges.Count, _vertices.Count);

            // set epsilon on vertex comparer
            division.Epsilon = _epsilon;

            // copy internal counters
            division._connectivity = _connectivity;
            division._nextEdgeKey = _nextEdgeKey;

            // copy edges with key and origin
            foreach (FreeEdge oldEdge in _edges.Values)
            {
                var newEdge = new FreeEdge(oldEdge._key);
                division._edges.Add(newEdge._key, newEdge);
                newEdge._origin = oldEdge._origin;
            }

            // copy vertices with new edge references
            foreach (FreeEdge oldEdge in _vertices.Values)
                division._vertices.Add(oldEdge._origin, division._edges[oldEdge._key]);

            // update edge references of all edges
            foreach (FreeEdge newEdge in division._edges.Values)
            {
                var oldEdge = _edges[newEdge._key];

                newEdge._twin = division._edges[oldEdge._twin._key];
                newEdge._next = division._edges[oldEdge._next._key];
                newEdge._previous = division._edges[oldEdge._previous._key];
            }

            return division;
        }

        #endregion
        #endregion
    }
}
