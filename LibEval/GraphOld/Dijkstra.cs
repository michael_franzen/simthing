﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Dijkstra.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using Tektosyne;
using Tektosyne.Graph;

namespace CPlan.Evaluation
{
    [Serializable]
    public class Dijkstra<T> //: IGraphPath<T>
    {

        private IGraph2D<T> CurGraph;
        private IList<T> Basis;
        private Dictionary <T, double> Dist;
        private Dictionary<T, T> Previous;

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="graph">
        /// The <see cref="IGraph2D{T}"/> on which all searches are performed.</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="graph"/> is a null reference.</exception>
        public Dijkstra(IGraph2D<T> graph)
        {
            if (graph == null)
                ThrowHelper.ThrowArgumentNullException("graph");

            CurGraph = graph;
        }

        /// <summary>
        /// Calculates the shortest pathes from the start node to all other nodes.
        /// </summary>
        /// <param name="start">start node</param>
        public void CalculateDistance(T start)
        {
            Basis = new List<T>();
            Dist = new Dictionary<T, double>();
            Previous = new Dictionary<T, T>();
            // Knoten aufnehmen
            foreach (T node in CurGraph.Nodes)
            {
                Previous.Add(node, default(T));
                Basis.Add(node);
                Dist.Add(node, double.MaxValue);
            }

            Dist[start] = 0;

            while (Basis.Count > 0)
            {
                T u = GetNodeWithSmallestDistance();
                if (EqualityComparer<T>.Default.Equals(u, default(T))) 
                {
                    Basis.Clear();
                }
                else
                {
                    //IList<T> test = CurGraph.GetNeighbors(u);
                    foreach (T v in CurGraph.GetNeighbors(u))
                    {
                        double alt = Dist[u] + CurGraph.GetDistance(u, v);
                        if (alt < Dist[v])
                        {
                            Dist[v] = alt;
                            Previous[v] = u;
                        }
                    }
                    Basis.Remove(u);
                }
            }
        }

        /// <summary>
        /// Gets a list of all nodes in the best path found by the last
        /// successful path search.
        /// </summary>
        /// <param name="d">target node </param>
        /// <returns></returns>
        public IList<T> Nodes(T d)
        {

            IList<T> path = new List<T>();

            path.Insert(0, d);

            while (!EqualityComparer<T>.Default.Equals(Previous[d], default(T)))
            {
                d = Previous[d];
                path.Insert(0, d);
            }

            return path;
        }
        
        /// <summary>
        /// Get the total cost to target node.
        /// </summary>
        /// <param name="d">target node<n/param>
        /// <returns>total cost</returns>
        public double TotalCost(T d)
        {
            double sum = 0;
            IList<T> curPath = Nodes(d); 
            foreach (T node in curPath)
            {
                sum += Dist[node];
            }
            return sum; 
        }

        /// <summary>
        /// Get the total cost in steps (depth) to target node.
        /// </summary>
        /// <param name="d">target node<n/param>
        /// <returns>total steps to target</returns>
        public int Depth(T d)
        {
            int sum = 0;
            IList<T> curPath = Nodes(d);
            sum += curPath.Count - 1;
            return sum;
        }

        /// <summary>
        /// Get the total cost in steps to all other nodes in the graph (total depth).
        /// </summary>
        /// <returns>total steps to all targets</returns>
        public int TotalDepth()
        {
            int sum = 0;
            IList<T> curPath;// = Nodes(d);
            foreach (T node in CurGraph.Nodes)
            {
                curPath = Nodes(node);
                sum += curPath.Count - 1; // -1: without counting the start node
            }
            return sum;
        }



        /// <summary>
        /// Returns the last node in the IGraphPath whose total 
        /// cost does not exceed the specified maximum cost.
        /// </summary>
        /// <param name="d">target node <n/param>
        /// <param name="maxCost">maximum cost <n/param>
        public T GetLastNode(T d, double maxCost)
        {
            double max = maxCost;
            IList<T> curPath = Nodes(d);
            T curNode = default(T);
            for (int i = curPath.Count-1; i >= 0; i--) 
            {
                if (max <= 0) break;
                T node = curPath[i];
                max -= Dist[node];
                curNode = node;
            }
            return curNode;
        }

        /// <summary>
        /// Returns the node with the shortest distance.
        /// </summary>
        /// <returns></returns>
        public T GetNodeWithSmallestDistance()
        {
            double distance = double.MaxValue;
            T smallest = default(T);

            foreach (T node in Basis)
            {
                if (Dist[node] < distance)
                {
                    distance = Dist[node];
                    smallest = node;
                }
            }
            return smallest;
        }


    }
}
