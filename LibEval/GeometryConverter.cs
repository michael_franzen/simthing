﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using GeoAPI.Geometries;
using CPlan.Geometry;

using Tektosyne.Geometry;

namespace CPlan.Evaluation

{
    public class GeometryConverter
    {

        public static Coordinate[] ToCoordinates(Poly2D poly)
        {
            List<Coordinate> result = new List<Coordinate>();
            foreach (Vector2D point in poly.Points[0])
            {
                result.Add(new Coordinate(point.X, point.Y));
            }
            result.Add(new Coordinate(poly.Points[0][0].X, poly.Points[0][0].Y));
            return result.ToArray();
        }

        public static Poly2D ToPoly2D(Coordinate[] coordinates)
        {
            Poly2D resultPoly;
            List<Vector2D> polyPts = new List<Vector2D>();
            foreach (Coordinate point in coordinates)
            {
                polyPts.Add(new Vector2D(point.X, point.Y));
            }
            resultPoly = new Poly2D(polyPts.ToArray());
            return resultPoly;
        }

        public static NetTopologySuite.Geometries.Point ToPoint(CPlan.Geometry.Vector2D point)
        {
            return new NetTopologySuite.Geometries.Point(point.X, point.Y);
        }

        public static LineString ToLineString(CPlan.Geometry.Line2D line)
        {
            return new LineString(new Coordinate[] { new Coordinate(line.Start.X, line.Start.Y), new Coordinate(line.End.X, line.End.Y) });
        }

        public static CPlan.Geometry.Vector2D ToVector2D(IPoint point)
        {
            return new CPlan.Geometry.Vector2D(point.X, point.Y);
        }

        public static CPlan.Geometry.Line2D ToLine2D(ILineString line)
        {
            return new CPlan.Geometry.Line2D(line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X, line.EndPoint.Y);
        }

        public static MultiPoint ToMultiPoint(List<CPlan.Geometry.Vector2D> points)
        {
            NetTopologySuite.Geometries.Point[] tmp = new NetTopologySuite.Geometries.Point[points.Count];

            for (int i = 0; i < points.Count; i++)
            {
                tmp[i] = ToPoint(points[i]);
            }

            return new MultiPoint(tmp);
        }

        public static List<CPlan.Geometry.Vector2D> ToVector2DList(MultiPoint points)
        {
            List<CPlan.Geometry.Vector2D> result = new List<CPlan.Geometry.Vector2D>();

            foreach (IPoint point in points.Geometries)
            {
                result.Add(ToVector2D(point));
            }

            return result;
        }

        public static MultiLineString ToMultiLineString(List<CPlan.Geometry.Line2D> lines)
        {
            LineString[] tmp = new LineString[lines.Count];

            for (int i = 0; i < lines.Count; i++)
            {
                tmp[i] = ToLineString(lines[i]);
            }

            return new MultiLineString(tmp);
        }

        public static List<CPlan.Geometry.Line2D> ToLine2DList(MultiLineString lines)
        {
            List<CPlan.Geometry.Line2D> result = new List<CPlan.Geometry.Line2D>();

            foreach (ILineString line in lines.Geometries)
            {
                result.Add(ToLine2D(line));
            }

            return result;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Convert a list of Line2D to a list of LineD (Tektosyne class).
        /// </summary>
        /// <param name="lineList"></param>
        /// <returns></returns>
        public static List<LineD> ToListLineD(List<Line2D> lineList)
        {
            List<LineD> tektoLineList = new List<LineD>();

            LineD toAddLine, reverseLine;
            // --- check for double lines -> check coordinates! ---
            foreach (Line2D line in lineList)
            {
                //bool contains = false;
                toAddLine = new LineD(line.Start.ToPointD(), line.End.ToPointD());
                //reverseLine = new LineD (line.End.ToPointD(), line.Start.ToPointD());
                //foreach (LineD tempLine in tektoLineList)
                //{                   
                //    if (tempLine.Start.X == toAddLine.Start.X && tempLine.Start.Y == toAddLine.Start.Y
                //        && tempLine.End.X == toAddLine.End.X && tempLine.End.Y == toAddLine.End.Y)
                //        contains = true;
                //    else if (tempLine.Start.X == reverseLine.Start.X && tempLine.Start.Y == reverseLine.Start.Y
                //        && tempLine.End.X == reverseLine.End.X && tempLine.End.Y == reverseLine.End.Y)
                //        contains = true;
                //}
                //if (!contains)
                    tektoLineList.Add(toAddLine);
            }
            return tektoLineList;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Convert an array of polygons with PointD (Tektosyne class) coordinates to a list of Poly2D.
        /// </summary>
        /// <param name="inPolygons"></param>
        /// <returns></returns>
        public static List<Poly2D> ToListPoly2D(PointD[][] inPolygons)
        {
            List<Vector2D> polyPts;
            List<Poly2D> outPlolygons = new List<Poly2D>();
            int nrPolys = inPolygons.GetLength(0);
            for (int i = 0; i < nrPolys; i++)
            {
                polyPts = new List<Vector2D>();
                foreach (PointD pt in inPolygons[i])
                {
                    polyPts.Add(pt.ToVector2D());
                }
                outPlolygons.Add(new Poly2D(polyPts.ToArray() ));
            }
            return outPlolygons;
        }


    }
}
