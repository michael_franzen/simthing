﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using uPLibrary.Networking.M2Mqtt;
//using VMToolkit;

namespace CPlan.Evaluation
{
    public class ConfigSettings
    {
        public int scenarioID;
        public string mqtt_topic;
        public int objID;
    }


    public class Communication2Lucy
    {
        public StreamReader OutputStream { get; private set; }
        private StreamWriter InputStream;
        private MqttClient mqttClient;
        private Dictionary<string, int> dictMqttTopic2SobjID = new Dictionary<string, int>();

        public Communication2Lucy()
        {
            //connect ("localhost", 7654);
        }


        public bool connect(String host, int port)
        {
            TcpClient socketForServer;
            try
            {
                socketForServer = new TcpClient(host, port);
            }
            catch
            {
                Console.WriteLine(
                "Failed to connect to server at {0}:{1}", host, port);
                return false;
            }

            NetworkStream networkStream = socketForServer.GetStream();
            System.IO.StreamReader streamReader =
            new System.IO.StreamReader(networkStream);
            System.IO.StreamWriter streamWriter =
            new System.IO.StreamWriter(networkStream);
            Console.WriteLine("******* Trying to communicate with lucy on host " + host + " and port " + port + " *****");

            //  streamWriter.WriteLine("{'action':'authenticate','username':'lukas','userpasswd':'1234'}");
            //  streamWriter.Flush();

            OutputStream = new StreamReader(networkStream);
            InputStream = new StreamWriter(networkStream);

            mqttClient = new MqttClient(System.Net.IPAddress.Parse(host));
            mqttClient.Connect("myClient");
            mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;


            return true;
        }

        private void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Message);
            if (message == "DONE")
            {
                int instanceID = -1;
                dictMqttTopic2SobjID.TryGetValue(e.Topic, out instanceID);

                if (instanceID != -1)
                {
                    string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
                    JObject answer = GlobalConnection.CL.sendAction2Lucy(msg);

                    Dictionary<int, object> result = new Dictionary<int, object>();
                    result.Add(instanceID, answer);

                    NotificationBroker.Instance.PostNotification(e.Topic, this, result);
                }
            }
        }

        public JObject sendAction2Lucy(string action)
        {
            InputStream.WriteLine(action);
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return jObj2;
        }

        public ConfigSettings sendCreateService2Lucy(string command, int scenarioID)
        {
            InputStream.WriteLine(command);
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            int serviceID = jObj2.Value<JObject>("result").Value<int>("SObjID");
            string mqtt_topic = jObj2.Value<JObject>("result").Value<String>("mqtt_topic");
            mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

            ConfigSettings result = new ConfigSettings() { scenarioID = scenarioID, mqtt_topic = mqtt_topic, objID = serviceID };


            return result;
        }

        public void startServiceViaMQTT(string mqtt_topic, int serviceID)
        {
            if (mqtt_topic != null)
            {
                if (mqttClient.IsConnected)
                {
                    dictMqttTopic2SobjID.Add(mqtt_topic, serviceID);
                    mqttClient.Subscribe(new string[] { mqtt_topic }, new byte[] { 1 });
                    mqttClient.Publish(mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                }
            }

        }


        public JObject sendService2Lucy(string command)
        {
            InputStream.WriteLine(command);
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            if (jObj2.Value<JToken>("result") != null)
            {
                if (((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt_topic"))
                {
                    json = OutputStream.ReadLine();
                    jObj2 = (JObject)JsonConvert.DeserializeObject(json);
                }

                if (jObj2.Value<JToken>("result") != null) // && ((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt"))
                    return ((Newtonsoft.Json.Linq.JObject)(jObj2.Value<JToken>("result")));
            }
            return null;
        }

        public JProperty authenticate(string user, string password)
        {
            return (JProperty)sendAction2Lucy("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}").First;

            InputStream.WriteLine("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}");
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
        }

        public JProperty getActionsList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['actions']}").First;

            InputStream.WriteLine("{'action':'get_list','of':['actions']}");
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
        }

        public JProperty getActionParameters(string action)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','actionname':'" + action + "'}").First;
        }

        public JProperty getServiceParameters(string service)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','servicename':'" + service + "'}").First;
        }

        public JProperty getServicesList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['services']}").First;

            InputStream.WriteLine("{'action':'get_list','of':['services']}");
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
        }

        private static string GetData()
        {
            //Ack from sql server
            return "ack";
        }

        /*
        public class Connection extends Thread{	
	Socket socket;
	ClientConsole console;
	BufferedReader input;
	DataOutputStream output;
	PrintStream print;
	boolean stopTalking = false;
		
	public Connection() throws IOException, ConnectException{		
		this.connect();
	}	   	
	
	private void connect() throws IOException{		

		System.out.println("connect");
		InetAddress host = InetAddress.getLocalHost();
        this.socket = new Socket(host.getHostName(), 7654);
		this.console = new ClientConsole(this);
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.output = new DataOutputStream(socket.getOutputStream());
		this.print = new PrintStream(socket.getOutputStream()); 
	}		*/
    }



}
