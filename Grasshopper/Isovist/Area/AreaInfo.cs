using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Area
{
    public class AreaInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Area";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.isovistarea;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2cbce96e-0e1c-11e6-b81e-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}