﻿using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace Weimar
{
    public class WeimarInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Weimar";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.isovistanalysis;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("17ed4cf5-f90b-450a-98d0-7f3b49a2cbd6");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
