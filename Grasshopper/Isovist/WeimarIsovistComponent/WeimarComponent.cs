﻿    using Grasshopper.Kernel;
using LibSimthing;
using Rhino.Geometry;
using System;
using System.Collections.Generic;

namespace Weimar
{
    public class WeimarComponent : GH_Component
    {
        private List<Line> data;

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public WeimarComponent()
          : base("Isovist2D", "Isovist2D",
              "Analyzes the 2D isovist of the apartment.",
              "Carve", " Bauhaus")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "Granularity", "The maximum edge size (>= 0.2) of the constructed .", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //pManager.AddGeometryParameter("faces", "Surfaces", "The grid faces.", GH_ParamAccess.list);
            //("Area", "Area", "The isovist's area for each grid element")
            //("AreaDistWeighted", "AreaDistWeighted", "The isovist distance-weighted area for each grid element.")
            //("Compactness", "Compactness", "The isovist compactness for each grid element. ")
            //("Perimeter", "Perimeter", "The isovist perimeter for each grid element.")
            //("Occlusivity", "Occlusivity", "The isovist occlusivity for each grid element.")
            //("MinRadial", "MinRadial", "The minimum isovist radial for each grid element.")
            //("MeanRadial", "MeanRadial", "The mean isovist radial for each grid element.")
            //("MaxRadial", "MaxRadial", "The maximum isovist radial for each grid element.")
            //("StdDevRadials", "StdDevRadials", "The standard deviation of the isovist radials for each grid element.")
            //("Variance", "Variance", "The isovist radials' variances for each grid element.")
            //("Skewness", "Skewness", "The isoivst skewness for each grid element.")
            //("NumberOfOcclusions", "NumberOfOcclusions", "The number of occlusions for each grid element.")
            //("MeanOcclusivity", "MeanOcclusivity", "The mean occlusivity for each grid element.")
            //("RelativeOcclusivity", "RelativeOcclusivity", "The ratio between occlusivity and perimeter for each grid element.")
            //("Circularity", "Circularity", "The circularity of the isovist for each grid element.")
            //("Dispersion", "Dispersion", "The isovists dispersion for each grid element")
            //("Elongation", "Elongation", "The isovists elongation for each grid element.")
            //("CompactnessBatty", "CompactnessBatty", "The batty-compactness for each grid element.")
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Input
            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;

            double granularity = 1.0;
            DA.GetData<double>("granularity", ref granularity);
            if (granularity < 0.2) granularity = 0.2;

            // Calculate Isovists
            List<CPlan.Geometry.Line2D> obstacleLines = new List<CPlan.Geometry.Line2D>();
            List<OpenTK.Vector2d> analysisPoints = new List<OpenTK.Vector2d>();
            
            foreach (Line line in apartment.RoomFootprintsNoDoors())
                obstacleLines.Add(new CPlan.Geometry.Line2D(new OpenTK.Vector2d(line.From.X, line.From.Y), new OpenTK.Vector2d(line.To.X, line.To.Y)));

            // debug
            this.data = apartment.RoomFootprintsNoDoors();
            List<Line> rooms = new List<Line>();

            // add analysis points
            List<Point3d> faceCenters = new List<Point3d>();
            List<Brep> faces = new List<Brep>();
            List<PolylineCurve> faceCurves = new List<PolylineCurve>();
            foreach (Room room in apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(footprint);
                
                if (breps == null) continue;
                foreach (Brep brep in breps)
                {
                    foreach (Mesh mesh in Rhino.Geometry.Mesh.CreateFromBrep(brep, new MeshingParameters() { MaximumEdgeLength = granularity }))
                    {
                        for (int i = 0; i < mesh.Faces.Count; i++)
                        {
                            Point3f a, b, c, d;
                            Point3d center = mesh.Faces.GetFaceCenter(i);
                            mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);

                            analysisPoints.Add(new OpenTK.Vector2d(center.X, center.Y));
                            faceCenters.Add(center);
                            faces.Add(Brep.CreateFromCornerPoints(a, b, c, d, 0.001));

                            faceCurves.Add(new PolylineCurve(new Point3d[] { a, b, c, d, a }));
                        }
                    }
                }
            }
            CPlan.Evaluation.IsovistAnalysis isovistAnalysis = new CPlan.Evaluation.IsovistAnalysis(analysisPoints, obstacleLines, (float)granularity);
            isovistAnalysis.Calculate();

            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["Area"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Area", Values = new List<double>(isovistAnalysis.AreaCleaned) };
            resultObjects["AreaDistWeighted"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="AreaDistWeighted", Values = new List<double>(isovistAnalysis.AreaDistWeightedCleaned) };
            resultObjects["Compactness"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Compactness", Values = new List<double>(isovistAnalysis.CompactnessCleaned) };
            resultObjects["Perimeter"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Perimeter", Values = new List<double>(isovistAnalysis.PerimeterCleaned) };
            resultObjects["Occlusivity"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Occlusivity", Values = new List<double>(isovistAnalysis.OcclusivityCleaned) };
            resultObjects["MinRadial"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="MinRadial", Values = new List<double>(isovistAnalysis.MinRadialCleaned) };
            resultObjects["MeanRadial"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="MeanRadial", Values = new List<double>(isovistAnalysis.MeanRadialCleaned) };
            resultObjects["MaxRadial"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="MaxRadial", Values = new List<double>(isovistAnalysis.MaxRadialCleaned) };
            resultObjects["StdDevRadials"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="StdDevRadials", Values = new List<double>(isovistAnalysis.StdDevRadialsCleaned) };
            resultObjects["Variance"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Variance", Values = new List<double>(isovistAnalysis.VarianceCleaned) };
            resultObjects["Skewness"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Skewness", Values = new List<double>(isovistAnalysis.SkewnessCleaned) };
            resultObjects["NumberOfOcclusions"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="NumberOfOcclusions", Values = new List<double>(isovistAnalysis.NumberOfOcclusionsCleaned) };
            resultObjects["MeanOcclusivity"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="MeanOcclusivity", Values = new List<double>(isovistAnalysis.MeanOcclusivityCleaned) };
            resultObjects["RelativeOcclusivity"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="RelativeOcclusivity", Values = new List<double>(isovistAnalysis.RelativeOcclusivityCleaned) };
            resultObjects["Circularity"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Circularity", Values = new List<double>(isovistAnalysis.CircularityCleaned) };
            resultObjects["Dispersion"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Dispersion", Values = new List<double>(isovistAnalysis.DispersionCleaned) };
            resultObjects["Elongation"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="Elongation", Values = new List<double>(isovistAnalysis.ElongationCleaned) };
            resultObjects["CompactnessBatty"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="CompactnessBatty", Values = new List<double>(isovistAnalysis.CompactnessBattyCleaned) };
            DA.SetData("Analysis", resultObjects);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.isovistanalysis;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{5a71638a-5314-4bdd-b7b2-3585af316222}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.primary);
            }
        }
    }
}
