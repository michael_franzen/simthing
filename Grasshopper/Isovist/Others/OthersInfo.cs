using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Others
{
    public class OthersInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Others";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.isovistothers;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2ca9134f-0e1c-11e6-a7e2-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}