using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;

namespace Radials
{
    public class RadialsComponent : GH_Component
    {
        private string[] features =
        {
            
				"AudibleMinRadial",
			
				"AudibleMaxRadial",
			
				"AudibleMeanRadial",
			
        };

        private string[] names =
        {
            
				"AudibleMinRadius",
			
				"AudibleMaxRadius",
			
				"AudibleMeanRadius",
			
        };

        private string[] descriptions =
        {
            
				"The minimum radius of the hearable area after emission from each grid point.",
			
				"The maximum radius of the hearable area after emission from each grid point.",
			
				"The mean radius of the hearable area after emission from each grid point.",
			
        };

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public RadialsComponent()
          : base("Radials", "Radials",
              "Description",
              "Carve", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis result.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            for (int i = 0; i < features.Length; i++)
                pManager.AddGenericParameter(features[i], names[i], descriptions[i], GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Dictionary < string, SimthingResultObject> resultObject = new Dictionary<string, SimthingResultObject>(); 
            if (!DA.GetData("Analysis", ref resultObject)) return;

            foreach (string feature in this.features)
            {
                DA.SetData(feature, resultObject[feature]);
            }
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.accusticRadials;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("2c646930-0e1c-11e6-a53a-c48508b0f4c7"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.secondary);
            }
        }
    }
}