﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using CPlan.Isovist2D;
using LibSimthing;
using CPlan.Geometry;

namespace AcousticsComponent
{
    public class AcousticsComponent : GH_Component
    {
        private List<double> areas = new List<double>();
        private List<double> reflectedareas = new List<double>();
        private List<double> reflectionratio = new List<double>();
        private List<double> audibleperimeter = new List<double>();
        private List<double> audibleminradial = new List<double>();
        private List<double> audiblemaxradial = new List<double>();
        private List<double> audiblemeanradial = new List<double>();

        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public AcousticsComponent()
          : base("Acoustics", "Acoustics",
              "Simulates sound emissions of 70dB from each point of a grid.",
              "Carve", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "Granularity", "The maximum edge size (>= 0.2) of the constructed grid.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            reflectedareas = new List<double>();
            areas = new List<double>();
            reflectionratio = new List<double>();
            audibleperimeter = new List<double>();
            audibleminradial = new List<double>();
            audiblemaxradial = new List<double>();
            audiblemeanradial = new List<double>();

        // Input
        Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;

            double granularity = 1.0;
            DA.GetData<double>("granularity", ref granularity);
            if (granularity < 0.2) granularity = 0.2;

            float wallReflection = .2f;
            float windowReflection = 1.0f;

            // debug
            List<Line> roomLines = new List<Line>();

            // Calculate Isovists
            List<CPlan.Geometry.Line2D> obstacleLines = new List<CPlan.Geometry.Line2D>();
            List<AnalysisPoint> analysisPoints = new List<AnalysisPoint>();
            List<float> reflectionCoefficients = new List<float>();

            foreach (Line line in apartment.RoomFootprintsNoDoorsNoWindows())
            {
                Line line2 = new Line(line.From, line.To);
                obstacleLines.Add(new CPlan.Geometry.Line2D(new OpenTK.Vector2d(line2.From.X, line2.From.Y), new OpenTK.Vector2d(line2.To.X, line2.To.Y)));
                roomLines.Add(line);
                reflectionCoefficients.Add(wallReflection);
            }
            


            // add analysis points
            List<Point3d> faceCenters = new List<Point3d>();
            List<Brep> faces = new List<Brep>();
            List<PolylineCurve> faceCurves = new List<PolylineCurve>();
            foreach (Room room in apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(footprint);

                if (breps == null) continue;
                foreach (Brep brep in breps)
                {
                    foreach (Mesh mesh in Rhino.Geometry.Mesh.CreateFromBrep(brep, new MeshingParameters() { MaximumEdgeLength = granularity }))
                    {
                        for (int i = 0; i < mesh.Faces.Count; i++)
                        {
                            Point3f a, b, c, d;
                            Point3d center = mesh.Faces.GetFaceCenter(i);
                            mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);

                            analysisPoints.Add(new AnalysisPoint(new OpenTK.Vector2d(center.X, center.Y)));
                            faceCenters.Add(center);
                            faces.Add(Brep.CreateFromCornerPoints(a, b, c, d, 0.001));

                            faceCurves.Add(new PolylineCurve(new Point3d[] { a, b, c, d, a }));
                        }
                    }
                }
            }


            foreach (Room room in apartment.Rooms)
            {
                foreach (Window w in room.Windows)
                {
                    float reflectionCoefficient = ((wallReflection * ((float)room.Height - (float)w.Height)) + windowReflection * (float)w.Height) / (float)room.Height;
                    foreach (Line line in w.Lines2DOnGround())
                    {
                        Line line2 = new Line(line.From, line.To);
                        roomLines.Add(line);
                        obstacleLines.Add(new CPlan.Geometry.Line2D(new OpenTK.Vector2d(line2.From.X, line2.From.Y), new OpenTK.Vector2d(line2.To.X, line2.To.Y)));
                        reflectionCoefficients.Add(reflectionCoefficient);
                    }
                }
            }

            this.CalculateIsovistFieldGPU(analysisPoints, obstacleLines, reflectionCoefficients);


            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["Area"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name = "Area", Values = areas };
            resultObjects["ReflectedArea"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="ReflectedArea", Values = reflectedareas };
            resultObjects["ReflectionRatio"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="ReflectionRatio", Values = reflectionratio };
            resultObjects["AudiblePerimeter"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="AudiblePerimeter", Values = audibleperimeter };
            resultObjects["AudibleMinRadial"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="AudibleMinRadial", Values = audibleminradial };
            resultObjects["AudibleMaxRadial"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="AudibleMaxRadial", Values = audiblemaxradial };
            resultObjects["AudibleMeanRadial"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="AudibleMeanRadial", Values = audiblemeanradial };
            DA.SetData("Analysis", resultObjects);
        }

        private void CalculateIsovistFieldGPU(List<AnalysisPoint> m_isovists, List<Line2D> m_obstacleLines, List<float> reflectionCoefficients, float m_precision = 0.05f, List<Line2D> m_exteriorWalls = null)
        {
            float[] wallP1X = new float[m_obstacleLines.Count];
            float[] wallP1Y = new float[m_obstacleLines.Count];
            float[] wallP2X = new float[m_obstacleLines.Count];
            float[] wallP2Y = new float[m_obstacleLines.Count];
            float[] pX = new float[m_isovists.Count];
            float[] pY = new float[m_isovists.Count];

            List<OpenTK.Vector2d> exteriorWallPoints = new List<OpenTK.Vector2d>();
            double threshold = 0.5;
            int k = 0;
            float[] extWallPX = new float[0];
            float[] extWallPY = new float[0];
            if (m_exteriorWalls != null)
            {
                foreach (Line2D extWall in m_exteriorWalls)
                {
                    int n = (int)(extWall.Length / threshold);
                    exteriorWallPoints.AddRange(extWall.Divide(n));
                }

                extWallPX = new float[exteriorWallPoints.Count];
                extWallPY = new float[exteriorWallPoints.Count];
                k = 0;
                foreach (OpenTK.Vector2d extWP in exteriorWallPoints)
                {
                    extWallPX[k] = (float)extWP.X;
                    extWallPY[k] = (float)extWP.Y;
                    k++;
                }
            }

            k = 0;
            foreach (Line2D tmpLineSLink in m_obstacleLines)
            {
                Line2D tmpLine = tmpLineSLink;
                wallP1X[k] = (float)tmpLine.Start.X;
                wallP1Y[k] = (float)tmpLine.Start.Y;
                wallP2X[k] = (float)tmpLine.End.X;
                wallP2Y[k] = (float)tmpLine.End.Y;
                k++;
            }

            k = 0;
            foreach (AnalysisPoint point in m_isovists)
            {
                pX[k] = (float)point.X;
                pY[k] = (float)point.Y;
                k++;
            }

            GPUIsovistFieldAcousticReflections gpu2 = GPUIsovistFieldAcousticReflections.GetInstance();
            gpu2.CreateWithReflection(reflectionCoefficients.ToArray(), wallP1X, wallP1Y, wallP2X, wallP2Y, extWallPX, extWallPY, pX, pY, m_precision);
            float[,] results2 = gpu2.RunWithReflection();

            for (int i = 0; i < results2.GetLength(0); i++)
            {
                areas.Add(results2[i, 0]);
                reflectedareas.Add(results2[i, 1]);
                reflectionratio.Add(results2[i, 2]);
                audibleperimeter.Add(results2[i, 3]);
                audibleminradial.Add(results2[i, 5]);
                audiblemaxradial.Add(results2[i, 6]);
                audiblemeanradial.Add(results2[i, 7]);
            }
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return DaylightComponent.Properties.Resources.accusticanalysis;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{50af039d-3247-48b8-8f69-d2549318c9ac}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.secondary);
            }
        }
    }
}
