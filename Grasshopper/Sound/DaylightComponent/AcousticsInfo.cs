﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace AcousticsComponent
{
    public class AcousticsInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "DaylightComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return DaylightComponent.Properties.Resources.accusticanalysis;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("e50e0863-a851-4a22-a87b-bcc004833098");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
