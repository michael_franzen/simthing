using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Acoustics
{
    public class AcousticsInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Acoustics";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.accusticLevels;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2c53a051-0e1c-11e6-923e-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}