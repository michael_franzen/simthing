﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace DirectSunlightResult
{
    public class DirectSunlightInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "DirectSunlightResult";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.directsunlightresult;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("9db4f4fb-2572-40ff-b8db-9d3597486c94");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
