﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;

namespace DirectSunlightComponent
{
    public class DirectSunlightComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public DirectSunlightComponent()
          : base("DirectSunlightComponent", "Sunlight",
              "Computes the direct sunlight of each window dependent on its orientation",
              "Carve", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "Granularity", "The granularity of the analysis grid.", GH_ParamAccess.item);
            pManager.AddNumberParameter("orientation", "Orientation", "The orientation in degrees as a value between 0 and 360 (0°=North, 90°=East).", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Debug only
            List<PolylineCurve> polyLines = new List<PolylineCurve>();

            // Input
            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;


            double granularity = 0.5;
            DA.GetData<double>("granularity", ref granularity);

            double orientation = 0;
            DA.GetData<double>("orientation", ref orientation);
            orientation %= 360;
            
            // get geometry
            List<List<List<Point3d>>> polygons = getPolygons(apartment, orientation);
            List<CGeom3d.Vec_3d[][]> geometry = transformPolygons(polygons);

            // get grid points
            List<OpenTK.Vector3d> gridPoints = new List<OpenTK.Vector3d>();
            List<OpenTK.Vector3d> gridNormales = new List<OpenTK.Vector3d>();

            Transform rotation = Transform.Rotation(Math.PI * orientation / 180.0, Point3d.Origin);
            List<Point3d> faceCenters = new List<Point3d>();
            List<Brep> faces = new List<Brep>();
            foreach (Room room in apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(footprint);

                if (breps == null) continue;
                foreach (Brep brep in breps)
                {
                    foreach (Mesh mesh in Rhino.Geometry.Mesh.CreateFromBrep(brep, new MeshingParameters() { MaximumEdgeLength = granularity }))
                    {
                        for (int i = 0; i < mesh.Faces.Count; i++)
                        {
                            Point3f a, b, c, d;
                            Point3d center = mesh.Faces.GetFaceCenter(i);
                            mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);

                            List<Point3d> pp = new List<Point3d>() { a, b, c, d, center };
                            pp = new List<Point3d>(rotation.TransformList(pp));
                            a = new Point3f((float)pp[0].X, (float)pp[0].Y, (float)pp[0].Z);
                            b = new Point3f((float)pp[1].X, (float)pp[1].Y, (float)pp[1].Z);
                            c = new Point3f((float)pp[2].X, (float)pp[2].Y, (float)pp[2].Z);
                            d = new Point3f((float)pp[3].X, (float)pp[3].Y, (float)pp[3].Z);
                            center = pp[4];

                            gridPoints.Add(new OpenTK.Vector3d(center.X, center.Y, 1));
                            gridNormales.Add(new OpenTK.Vector3d(0, 0, 1));
                            faceCenters.Add(center);
                            faces.Add(Brep.CreateFromCornerPoints(a, b, c, d, 0.001));
                        }
                    }
                }
            }

            // run analysis
            CPlan.Evaluation.SolarAnalysis analysis = new CPlan.Evaluation.SolarAnalysis(gridPoints, gridNormales);
            analysis.ClearGeometry();

            foreach (CGeom3d.Vec_3d[][] polygonSet in geometry)
                analysis.AddPolygon(polygonSet, 1);

            analysis.Calculate(DirectSunLightNet.CalculationMode.illuminance_klux);
            List<double> luminance_klux = new List<double>(analysis.Results);

            analysis.Calculate(DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
            List<double> luminance_kcd = new List<double>(analysis.Results);

            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["luminanceklux"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name = "luminanceklux", Values = luminance_klux };
            resultObjects["luminancekcd"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name = "luminancekcd", Values = luminance_kcd };
            DA.SetData("Analysis", resultObjects);
        }

        private List<CGeom3d.Vec_3d[][]> transformPolygons(List<List<List<Point3d>>> polygons)
        {
            
            List<CGeom3d.Vec_3d[][]> transformedList = new List<CGeom3d.Vec_3d[][]>();
            foreach (List<List<Point3d>> polySet in polygons)
            {
                List<CGeom3d.Vec_3d[]> polySetVec = new List<CGeom3d.Vec_3d[]>();
                foreach (List<Point3d> points in polySet)
                {
                    List<CGeom3d.Vec_3d> vecs = new List<CGeom3d.Vec_3d>();
                    foreach (Point3d point in points)
                    {
                        vecs.Add(new CGeom3d.Vec_3d(point.X, point.Y, point.Z));
                    }
                    polySetVec.Add(vecs.ToArray());
                }
                transformedList.Add(polySetVec.ToArray());
            }
            return transformedList;
        }

        private List<List<List<Point3d>>> getPolygons(Apartment apartment, double orientation)
        {
            Transform rotation = Transform.Rotation(Math.PI * orientation / 180.0, Point3d.Origin);

            List<List<List<Point3d>>> polygons = new List<List<List<Point3d>>>();
            // Parse apartment
            foreach (Room room in apartment.Rooms)
            {
                Vector3d wallHeightVector = new Vector3d(0, 0, room.Height);

                // construct floor polygon
                // TODO: Add pillars
                List<Point3d> floor = room.CyclicVertices;
                polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(floor)) } );


                // construct ceiling polygon
                List<Point3d> ceiling = new List<Point3d>();
                for (int i = 0; i < floor.Count; i++)
                    ceiling.Add(floor[i] + wallHeightVector);
                polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(ceiling)) });

                // construct wall polygons
                for (int i = 0; i < floor.Count - 1; i++)
                {
                    List<List<Point3d>> wallPolygons = new List<List<Point3d>>();

                    List<Point3d> wallPolygon = new List<Point3d>() {
                        floor[i],
                        floor[i + 1],
                        floor[i + 1] + wallHeightVector,
                        floor[i] + wallHeightVector,
                        floor[i]
                    };
                    wallPolygons.Add(new List<Point3d>(rotation.TransformList(wallPolygon)));

                    // add holes
                    PolylineCurve wallPolyline = new PolylineCurve(wallPolygon);

                    // add windows
                    foreach (Window window in room.Windows)
                    {
                        Vector3d windowHeightVector = new Vector3d(0, 0, window.Height);
                        foreach (Line windowLine in window.Lines2D())
                        {
                            PointContainment pc1 = wallPolyline.Contains(windowLine.From);
                            PointContainment pc2 = wallPolyline.Contains(windowLine.To);

                            if (pc1 != PointContainment.Outside && pc2 != PointContainment.Outside)
                            {
                                List<Point3d> windowPolygon = new List<Point3d>()
                                {
                                    windowLine.From,
                                    windowLine.To,
                                    windowLine.To + windowHeightVector,
                                    windowLine.From + windowHeightVector,
                                    windowLine.From
                                };
                                wallPolygons.Add(new List<Point3d>(rotation.TransformList(windowPolygon)));
                            }
                        }
                    }

                    // add doors
                    foreach (Door door in room.Doors)
                    {
                        // don't add exterior doors
                        if (apartment.GetRooms(door).Count < 2)
                            continue;

                        Vector3d doorHeightVector = new Vector3d(0, 0, door.Height);
                        foreach (Line doorLine in door.Lines2D())
                        {
                            PointContainment pc1 = wallPolyline.Contains(doorLine.From);
                            PointContainment pc2 = wallPolyline.Contains(doorLine.To);

                            if (pc1 != PointContainment.Outside && pc2 != PointContainment.Outside)
                            {
                                List<Point3d> doorPolygon = new List<Point3d>()
                                {
                                    doorLine.From,
                                    doorLine.To,
                                    doorLine.To + doorHeightVector,
                                    doorLine.From + doorHeightVector,
                                    doorLine.From
                                };
                                wallPolygons.Add(new List<Point3d>(rotation.TransformList(doorPolygon)));
                            }
                        }
                    }

                    // add wallpolygons to set of polygons
                    polygons.Add(wallPolygons);
                }


                // add door frames
                foreach (Door door in room.Doors)
                {
                    // don't add exterior doors
                    if (apartment.GetRooms(door).Count < 2)
                        continue;

                    List<Line> doorLines = door.Lines2D();
                    Vector3d doorHeight = new Vector3d(0, 0, door.Height);

                    // footprint
                    List<Point3d> doorFootprint = new List<Point3d>()
                    {
                        doorLines[0].From,
                        doorLines[1].From,
                        doorLines[2].From,
                        doorLines[3].From,
                        doorLines[0].From
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorFootprint)) });

                    // ceiling
                    List<Point3d> doorCeiling = new List<Point3d>()
                    {
                        doorLines[0].From + doorHeight,
                        doorLines[1].From + doorHeight,
                        doorLines[2].From + doorHeight,
                        doorLines[3].From + doorHeight,
                        doorLines[0].From + doorHeight
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorCeiling)) });

                    // side reels
                    List<Point3d> doorLeft = new List<Point3d>()
                    {
                        doorLines[1].From,
                        doorLines[1].From + doorHeight,
                        doorLines[2].From + doorHeight,
                        doorLines[2].From,
                        doorLines[1].From
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorLeft)) });

                    List<Point3d> doorRight = new List<Point3d>()
                    {
                        doorLines[3].From,
                        doorLines[3].From + doorHeight,
                        doorLines[0].From + doorHeight,
                        doorLines[0].From,
                        doorLines[3].From
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorRight)) });
                }
            }

            return polygons;
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.directsunlight;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{d223d5d6-90e6-4c63-a80d-db3c9251a410}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.primary);
            }
        }
    }
}
