﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace DirectSunlightComponent
{
    public class DirectSunlightnfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "DirectSunlightComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.directsunlight;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("01c6b5e0-fb26-4604-a105-8ef32b32766d");
            }
        }
        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
