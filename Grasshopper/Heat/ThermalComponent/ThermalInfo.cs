﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace ThermalComponent
{
    public class ThermalInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "ThermalComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.thermalanalysis;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("233bc815-cfe7-46c6-ae15-9afca1d4ce40");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
