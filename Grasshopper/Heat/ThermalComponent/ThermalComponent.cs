﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;
using Rhino.Geometry.Intersect;
using LibThermal;

namespace ThermalComponent
{
    public class ThermalComponent : GH_Component
    {
        private Apartment apartment = null;
        private int maxtime = 10;
        double[] conductivity;
        double[] startingTemperature;
        double[] scalingFactor;
        bool[] isConstant;
        List<Brep> faces = new List<Brep>();
        ThermalSimulation simulation;
        List<Point3d> analysisPoints = new List<Point3d>();
        List<int[]> neighbors = new List<int[]>();
        List<PolylineCurve> faceCurves = new List<PolylineCurve>();

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ThermalComponent()
          : base("Thermics", "Thermics",
              "Analyzes the apartment regarding heat dynamics.",
              "Carve", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("time", "Time", "The time step (Integer) of the simulation.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //pManager.AddGeometryParameter("faces", "Surfaces", "The rooms' floor surfaces.", GH_ParamAccess.item);
            //("temperatures", "Temperature", "The temperatures at the specified time in each room.")
            //("totalinflux", "Total Influx", "The total influx of each room at the specified time.")
            //("totaloutflow", "Total Outflow", "The total outflow of each room at the specified time.")
            //("totaldelta", "Total Delta", "The total temperature delta of each room at the specified time.")
            //("innerinflux", "Inner Influx", "The influx from other rooms of each room at the specified time.")
            //("inneroutflow", "Inner Outflow", "The outflow to other rooms of each room at the specified time.")
            //("innerdelta", "Inner Delta", "The temperature delta produced from room heat exchanges of each room at the specified time.")
            //pManager.AddGeometryParameter("lines", "Lines", "The rooms' floor surfaces.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {

            Apartment currentApartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref currentApartment)) return;

            int t = 0;
            if (!DA.GetData<int>("time", ref t)) return;
            if (t > this.maxtime || this.simulation == null || currentApartment != this.apartment)
            {
                if (currentApartment != this.apartment) this.apartment = currentApartment;
                if (t > this.maxtime) this.maxtime = t;
                //this.computeFE();
                this.computeRoomGraph();
            }
            List<double> temperatures = new List<double>();
            List<double> totalInflux = new List<double>();
            List<double> totalOutflow = new List<double>();
            List<double> totalDelta = new List<double>();
            List<double> innerInflux = new List<double>();
            List<double> innerOutflow = new List<double>();
            List<double> innerDelta = new List<double>();
            for (int i = 0; i < startingTemperature.Length; i++)
            {
                temperatures.Add(this.simulation.Temperatures[t, i]);
                totalInflux.Add(this.simulation.TotalInflux[t, i]);
                totalOutflow.Add(this.simulation.TotalOutflow[t, i]);
                totalDelta.Add(this.simulation.TotalDelta[t, i]);
                innerInflux.Add(this.simulation.InnerInflux[t, i]);
                innerOutflow.Add(this.simulation.InnerOutflow[t, i]);
                innerDelta.Add(this.simulation.InnerDelta[t, i]);
            }

            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["temperatures"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="temperatures", Values = temperatures };
            resultObjects["totalinflux"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="totalinflux", Values = totalInflux };
            resultObjects["totaloutflow"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="totaloutflow", Values = totalOutflow };
            resultObjects["totaldelta"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="totaldelta", Values = totalDelta };
            resultObjects["innerinflux"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="innerinflux", Values = innerInflux };
            resultObjects["inneroutflow"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="inneroutflow", Values = innerOutflow };
            resultObjects["innerdelta"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="innerdelta", Values = innerDelta };
            DA.SetData("Analysis", resultObjects);
        }

        private void computeRoomGraph()
        {
            // Beton: 1.21 * thickness (m)
            // Mauerziegeln: 6.25 * thickness (m)
            // Mauerziegel isoliert (PUR): 1.07 * thickness (m)
            // Porenbeton: 0.25 * thickness (m)
            double exteriorWallCoeffcient = 1.21;
            double interiorWallCoeffcient = 6.25;

            // simple window: 5.9
            // double window: 3.0
            // isolated glazing: 2.8
            // Wärmeschutzverglasung: 1.3
            // Energienachweis Schweiz: 1.3
            // Passivhausstandard: 0.5-0.8
            double windowCoeffcient = 3.0;
            double airCoefficient = 0.026;// 0.026;
            double insideTemperature = 20;
            double outsideTemperature = 30;

            Dictionary<int, Room> nodeToRoom = new Dictionary<int, Room>();
            Dictionary<Room, int> roomToNode = new Dictionary<Room, int>();
            Dictionary<Room, double> roomToFlow = new Dictionary<Room, double>();
            HashSet<Line> lines = new HashSet<Line>(); ;
            Dictionary<Line, double> lineWindowHeight = new Dictionary<Line, double>();

            foreach (Line line in this.apartment.RoomFootprintsNoDoorsNoWindows())
            {
                lines.Add(line);
                lineWindowHeight[line] = 0.0d;
            }

            foreach (Window window in this.apartment.Windows)
            {
                foreach (Line line in window.Lines2DOnGround())
                {
                    lines.Add(line);
                    lineWindowHeight[line] = window.Height;
                }
            }

            // get wall for each line and vice versa
            // lines that are associated to each other by the same wall have an edge capacity of +inf to each other
            // line capacities are downscaled using their wall thickness, wall height etc.
            Dictionary<Line, Wall> lineToWall = new Dictionary<Line, Wall>();
            Dictionary<Wall, HashSet<Line>> wallToLine = new Dictionary<Wall, HashSet<Line>>();

            foreach (Line line in lines)
            {
                foreach (Wall wall in apartment.Walls)
                {
                    foreach (Line wallLine in wall.Lines2DLong())
                    {
                        CurveIntersections intersections = Intersection.CurveCurve(new LineCurve(wallLine), new LineCurve(line), 0.01, 0.01);
                        if (intersections.Count > 0 && !intersections[0].IsPoint)
                        {
                            if (!wallToLine.ContainsKey(wall))
                                wallToLine[wall] = new HashSet<Line>();
                            wallToLine[wall].Add(line);
                            lineToWall[line] = wall;
                            break;
                        }
                    }
                }
            }

            //get rooms for each line and vice versa
            Dictionary<Line, Room> lineToRoom = new Dictionary<Line, Room>();
            Dictionary<Room, HashSet<Line>> roomToLine = new Dictionary<Room, HashSet<Line>>();
            foreach (Room room in this.apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                foreach (Line line in lines)
                {
                    PointContainment pc1 = footprint.Contains(line.From, Plane.WorldXY, 0.01);
                    PointContainment pc2 = footprint.Contains(line.To, Plane.WorldXY, 0.01);

                    if (pc1 != PointContainment.Outside || pc2 != PointContainment.Outside) {
                        if (!roomToLine.ContainsKey(room))
                            roomToLine[room] = new HashSet<Line>();
                        roomToLine[room].Add(line);
                        lineToRoom[line] = room;
                    }
                }
            }

            // remove lines that are not associated to a room
            // check for each line if it's an exterior one
            Dictionary<Line, bool> lineIsExterior = new Dictionary<Line, bool>();
            HashSet<Line> removelines = new HashSet<Line>();
            foreach (Line line in lines)
            {
                if (!lineToRoom.ContainsKey(line) || !lineToWall.ContainsKey(line))
                {
                    removelines.Add(line);
                    continue;
                }

                List<Room> associatedRooms = new List<Room>();
                associatedRooms.Add(lineToRoom[line]);
                
                Wall wall = lineToWall[line];
                foreach (Line line2 in wallToLine[lineToWall[line]])
                {
                    // if line2 and line contain the same abschnitt
                    int q = 0;
                    if (Math.Abs(line2.ClosestPoint(line.From, true).DistanceTo(line.From) - wall.Width) < 0.01) q++;
                    if (Math.Abs(line2.ClosestPoint(line.To, true).DistanceTo(line.To) - wall.Width) < 0.01) q++;
                    if (Math.Abs(line.ClosestPoint(line2.From, true).DistanceTo(line2.From) - wall.Width) < 0.01) q++;
                    if (Math.Abs(line.ClosestPoint(line2.To, true).DistanceTo(line2.To) - wall.Width) < 0.01) q++;
                    if (q < 2) continue;

                    if (lineToRoom.ContainsKey(line2))
                        associatedRooms.Add(lineToRoom[line2]);
                }

                if (associatedRooms.Count == 1)
                {
                    lineIsExterior[line] = true;
                }
                else
                    lineIsExterior[line] = false;
            }

            foreach (Line line in removelines)
                lines.Remove(line);

            // compute line capacities & areas for later scaling
            Dictionary<Line, double> lineCapacities = new Dictionary<Line, double>();
            Dictionary<Line, double> lineArea = new Dictionary<Line, double>();
            foreach (Line line in lines)
            {
                double windowArea = line.Length * lineWindowHeight[line];
                double wallArea = line.Length * lineToWall[line].Height - windowArea;
                double wallThickness = lineToWall[line].Width;

                lineCapacities[line] = (interiorWallCoeffcient * wallArea * wallThickness + windowCoeffcient * windowArea) / (wallArea + windowArea);//;(lineToRoom[line].Area() * lineToRoom[line].Height); // remove area term
                if (lineIsExterior[line])
                    lineCapacities[line] = (exteriorWallCoeffcient * wallArea * wallThickness + windowCoeffcient * windowArea) / (wallArea + windowArea); //(lineToRoom[line].Area() * lineToRoom[line].Height); // remove area term

                lineArea[line] = line.Length * lineToWall[line].Height;
            }

            // create graph
            double[] startingTemperature0 = new double[apartment.Rooms.Count + lines.Count + 1];
            double[] conductivity0 = new double[apartment.Rooms.Count + lines.Count + 1];
            bool[] isConstant0 = new bool[apartment.Rooms.Count + lines.Count + 1];
            List<List<int>> neighbors = new List<List<int>>();
            for(int i = 0; i < conductivity0.Length; i++)
                neighbors.Add(new List<int>());
            double[] scalingFactors0 = new double[apartment.Rooms.Count + lines.Count + 1]; // != 1 for walls


            // add rooms
            List<Room> roomList = new List<Room>(this.apartment.Rooms);
            Dictionary<Room, double> roomCapacities = new Dictionary<Room, double>();
            for (int i = 0; i < roomList.Count; i++)
            {
                roomToNode[roomList[i]] = i;
                conductivity0[i] = airCoefficient;// * roomList[i].Perimeter() * roomList[i].Height;
                startingTemperature0[i] = insideTemperature;
                scalingFactors0[i] = 1.0; // rooms are aggregates and therefore have no surface area
            }

            // add lines
            int k = roomList.Count;
            foreach (Line line in lines)
            {
                HashSet<Room> associatedRooms = new HashSet<Room>();
                associatedRooms.Add(lineToRoom[line]);
                Wall wall = lineToWall[line];
                foreach (Line line2 in wallToLine[lineToWall[line]])
                {
                    // if line2 and line contain the same abschnitt
                    int q = 0;
                    if (Math.Abs(line2.ClosestPoint(line.From, true).DistanceTo(line.From) - wall.Width) < 0.01) q++;
                    if (Math.Abs(line2.ClosestPoint(line.To, true).DistanceTo(line.To) - wall.Width) < 0.01) q++;
                    if (Math.Abs(line.ClosestPoint(line2.From, true).DistanceTo(line2.From) - wall.Width) < 0.01) q++;
                    if (Math.Abs(line.ClosestPoint(line2.To, true).DistanceTo(line2.To) - wall.Width) < 0.01) q++;
                    if (q < 3) continue;

                    if (lineToRoom.ContainsKey(line2))
                        associatedRooms.Add(lineToRoom[line2]);
                }

                foreach (Room room2 in associatedRooms)
                {
                    neighbors[roomToNode[room2]].Add(k);
                    neighbors[k].Add(roomToNode[room2]);
                }

                conductivity0[k] = lineCapacities[line];
                startingTemperature0[k] = insideTemperature;
                scalingFactors0[k] = 1.0/lineArea[line];
                if (lineIsExterior[line])
                {
                    neighbors[k].Add(conductivity0.Length - 1);
                    neighbors[conductivity0.Length - 1].Add(k);
                }
                k++;
            }

            List<int[]> neighbors0 = new List<int[]>();
            foreach (List<int> l0 in neighbors) neighbors0.Add(l0.ToArray());

            // create super source
            isConstant0[conductivity0.Length - 1] = true;
            conductivity0[conductivity0.Length - 1] = airCoefficient;
            startingTemperature0[conductivity0.Length - 1] = outsideTemperature;
            scalingFactors0[conductivity0.Length - 1] = 1.0; // also an aggregate

            // simulate
            this.simulation = new ThermalSimulation(neighbors0.ToArray(), conductivity0, startingTemperature0, isConstant0, scalingFactors0);
            simulation.SimulateUntil(this.maxtime + 1);

            this.faces = new List<Brep>();
            this.conductivity = new double[apartment.Rooms.Count];
            this.startingTemperature = new double[apartment.Rooms.Count];
            this.isConstant = new bool[apartment.Rooms.Count];

            double[,] results = simulation.Temperatures;

            for (int i = 0; i < roomList.Count; i++)
                faces.Add(Brep.CreatePlanarBreps(roomList[i].Footprint())[0]);

            for (int i = 0; i < roomList.Count; i++)
                this.conductivity[i] = conductivity0[roomToNode[roomList[i]]];

            for (int i = 0; i < roomList.Count; i++)
                this.startingTemperature[i] = startingTemperature0[roomToNode[roomList[i]]];

            for (int i = 0; i < roomList.Count; i++)
                this.isConstant[i] = isConstant0[roomToNode[roomList[i]]];
        }

        private void computeFE()
        {
            double granularity = 0.5;
            double exteriorWallCoeffcient = .21;
            double windowCoeffcient = 1;
            double airCoefficient = 100;
            double insideTemperature = 0;
            double outsideTemperature = 30;

            if (this.simulation == null)
            {

                //compute deltas to enlarge bounding box (for exterior mesh points)
                double maxWidth = 0.0;
                foreach (Wall wall in apartment.Walls)
                    if (wall.Width > maxWidth) maxWidth = wall.Width;
                Vector3d deltaX = new Vector3d(5 * maxWidth, 0, 0);
                Vector3d deltaY = new Vector3d(0, 5 * maxWidth, 0);

                // get analaysis points
                List<Point3d> points = new List<Point3d>();
                foreach (Room room in apartment.Rooms)
                    points.AddRange(room.CyclicVertices);
                this.faces = new List<Brep>();
                this.analysisPoints = new List<Point3d>();
                this.neighbors = new List<int[]>();
                this.faceCurves = new List<PolylineCurve>();

                BoundingBox bbox1 = new BoundingBox(points);
                BoundingBox bbox2 = new BoundingBox(bbox1.Min - deltaX - deltaY, bbox1.Max + deltaX + deltaY);
                Point3d[] corners = bbox2.GetCorners();
                int numX = (int)Math.Ceiling(((corners[2] - corners[0]).X) / granularity);
                int numY = (int)Math.Ceiling(((corners[2] - corners[0]).Y) / granularity);
                for (int i = 0; i < numX; i++)
                {
                    for (int j = 0; j < numY; j++)
                    {
                        Point3d center = new Point3d(corners[0].X + i * granularity + granularity / 2, corners[0].Y + j * granularity + granularity / 2, 0);
                        Point3d[] faceCorners = new Point3d[]
                        {
                            new Point3d(corners[0].X + (i+1)* granularity, corners[0].Y + j * granularity, 0),
                            new Point3d(corners[0].X + (i+1) * granularity, corners[0].Y + (j+1) * granularity, 0),
                            new Point3d(corners[0].X + i * granularity, corners[0].Y + (j+1) * granularity, 0),
                            new Point3d(corners[0].X + i * granularity, corners[0].Y + j * granularity, 0),
                            new Point3d(corners[0].X + (i+1) * granularity, corners[0].Y + j * granularity, 0)
                        };
                        analysisPoints.Add(center);
                        faces.Add(Brep.CreateFromCornerPoints(faceCorners[0], faceCorners[1], faceCorners[2], faceCorners[3], 0.001));
                        faceCurves.Add(new PolylineCurve(faceCorners));

                        List<int> faceNeighbors = new List<int>();
                        if (j < numY - 1) faceNeighbors.Add(i * numY + (j + 1));
                        if (i < numX - 1) faceNeighbors.Add((i + 1) * numY + j);
                        if (j > 0) faceNeighbors.Add(i * numY + (j - 1));
                        if (i > 0) faceNeighbors.Add((i - 1) * numY + j);
                        neighbors.Add(faceNeighbors.ToArray());
                    }
                }

                // assign coefficients and start temp
                conductivity = new double[analysisPoints.Count];
                startingTemperature = new double[analysisPoints.Count];
                scalingFactor = new double[analysisPoints.Count];
                isConstant = new bool[analysisPoints.Count];
                HashSet<int> inside = new HashSet<int>();

                foreach (Room room in apartment.Rooms)
                {
                    PolylineCurve footprint = room.Footprint();
                    for (int i = 0; i < analysisPoints.Count; i++)
                    {
                        PointContainment pc = footprint.Contains(analysisPoints[i]);
                        if (pc != PointContainment.Outside)
                        {
                            inside.Add(i);
                            startingTemperature[i] = insideTemperature;
                            conductivity[i] = airCoefficient;
                            scalingFactor[i] = granularity;
                        }
                    }
                }

                foreach (Wall wall in apartment.Walls)
                {
                    Line wallLine = wall.Axis();
                    for (int i = 0; i < analysisPoints.Count; i++)
                    {
                        CurveIntersections intersections = Intersection.CurveCurve(new LineCurve(wallLine), faceCurves[i], 0.001, 0.001);
                        if (intersections.Count > 0)
                        {
                            inside.Add(i);
                            startingTemperature[i] = insideTemperature;
                            conductivity[i] = exteriorWallCoeffcient; //TODO: Check
                            scalingFactor[i] = granularity;
                        }
                    }
                }

                foreach (Window window in apartment.Windows)
                {
                    Line windowLine = window.AxisOnGround();
                    for (int i = 0; i < analysisPoints.Count; i++)
                    {
                        CurveIntersections intersections = Intersection.CurveCurve(new LineCurve(windowLine), faceCurves[i], 0.001, 0.001);
                        if (intersections.Count > 0)
                        {
                            inside.Add(i);
                            startingTemperature[i] = insideTemperature;
                            conductivity[i] = windowCoeffcient;
                            scalingFactor[i] = granularity;
                        }
                    }
                }

                for (int i = 0; i < analysisPoints.Count; i++)
                {
                    if (!inside.Contains(i))
                    {
                        startingTemperature[i] = outsideTemperature;
                        conductivity[i] = airCoefficient;
                        isConstant[i] = true;
                        scalingFactor[i] = granularity;
                    }
                }
                this.simulation = new ThermalSimulation(neighbors.ToArray(), conductivity, startingTemperature, isConstant, scalingFactor);
            }
            simulation.SimulateUntil(this.maxtime + 1);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.thermalanalysis;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{1e761de6-4d25-4888-8126-fbfa8a294a65}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.tertiary);
            }
        }
    }
}
