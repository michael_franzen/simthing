using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Inner
{
    public class InnerInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Inner";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.thermalinside;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2c709e2e-0e1c-11e6-be09-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}