﻿using CPlan.Evaluation;
using CPlan.Geometry;
using Grasshopper.Kernel;
using LibSimthing;
using Rhino.Geometry;
using System;
using System.Collections.Generic;

namespace WeimarGraphComponent
{
    public class WeimarGraphComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public WeimarGraphComponent()
          : base("Connectivity", "Connectivity",
              "Analyzes the apartment's connectivity.",
              "Carve", " Bauhaus")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "Granularity", "The maximum edge size (>= 0.5) of the constructed grid.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //pManager.AddGeometryParameter("faces", "Surfaces", "The grid surfaces.", GH_ParamAccess.list);
            //("centralitymetric", "Closeness (metric)", "The closeness of each grid element measured metrically.")
            //("centralitysteps", "Closeness (steps)", "The closeness of each grid element measured in grid steps.")
            //("choice", "Betweenness", "The simplified betweenness of each grid element.")
            //("connectivity", "Connectivity", "The number of neighbors of each grid element.")

        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Input
            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;

            double granularity = 1.0;
            DA.GetData<double>("granularity", ref granularity);
            if (granularity < 0.5) granularity = 0.5;

            // TODO: This needs to be improved
            double stepwidth = granularity;
            
            // generate graph
            List<Brep> faces = new List<Brep>();
            List<Line2D> connections = new List<Line2D>();
            List<Point3d> vertices = new List<Point3d>();
            foreach (Room room in apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(footprint);

                if (breps == null) continue;
                foreach (Brep brep in breps)
                {
                    foreach (Mesh mesh in Rhino.Geometry.Mesh.CreateFromBrep(brep, new MeshingParameters() { MaximumEdgeLength = granularity }))
                    {
                        for (int i = 0; i < mesh.Faces.Count; i++)
                        {
                            // add vertex
                            Point3d center = mesh.Faces.GetFaceCenter(i);
                            vertices.Add(center);

                            // add corresponding face
                            Point3f a, b, c, d;
                            mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);
                            faces.Add(Brep.CreateFromCornerPoints(a, b, c, d, 0.001));

                            // get adjacent faces and add connections
                            Point3d adjacentFaceCenter;
                            foreach (int x in mesh.Faces.AdjacentFaces(i))
                            {
                                adjacentFaceCenter = mesh.Faces.GetFaceCenter(x);
                                connections.Add(new Line2D(new OpenTK.Vector2d(center.X, center.Y), new OpenTK.Vector2d(adjacentFaceCenter.X, adjacentFaceCenter.Y)));
                            }
                        }
                    }
                }
            }

            // add connections to doors
            List<Line2D> doorConnections = new List<Line2D>();
            foreach (Door door in apartment.Doors)
            {
                Point3d doorCenter = door.Axis().PointAt(0.5);
                vertices.Add(doorCenter);

                faces.AddRange(Brep.CreatePlanarBreps(door.Footprint()));
                for (int i = 0; i < vertices.Count; i++)
                {
                    Point3d center = vertices[i];
                    if (doorCenter.DistanceTo(center) < stepwidth)
                        doorConnections.Add(new Line2D(new OpenTK.Vector2d(center.X, center.Y), new OpenTK.Vector2d(doorCenter.X, doorCenter.Y)));
                }
            }
            connections.AddRange(doorConnections);

            // graph analysis, sadly indices are getting mixed up here.
            CPlan.Evaluation.GraphAnalysis graphAnalysis = new CPlan.Evaluation.GraphAnalysis(connections);
            graphAnalysis.Calculate("angle");
            ShortestPath results = graphAnalysis.ResultsAngular;

            // get metrics
            float[][] centralityMetric = results.GetNormCentralityMetricArray();
            float[][] centralitySteps = results.GetNormCentralityStepsArray();
            float[][] choice = results.GetNormChoiceArray();
            float[] connectivity = results.GetNormConnectivityArray();

            // find the correct indices and sum up the data for each face

            double[] centralityMetricSummed = new double[vertices.Count];
            double[] centralityStepsSummed = new double[vertices.Count];
            double[] choiceSummed = new double[vertices.Count];
            double[] connectivitySummed = new double[vertices.Count];

            for (int i = 0; i < vertices.Count; i++) {
                Point3d vertex = vertices[i];
                for (int j = 0; j < results.OrigEdges.Length; j++) {
                    Tektosyne.Geometry.LineF testLine = results.OrigEdges[j].ToLineF();
                    Point3d startVertex = new Point3d(testLine.Start.X, testLine.Start.Y, 0);
                    Point3d endVertex = new Point3d(testLine.End.X, testLine.End.Y, 0);

                    if (vertex.EpsilonEquals(startVertex, 0.01) || vertex.EpsilonEquals(endVertex, 0.01))
                    {
                        centralityMetricSummed[i] += centralityMetric[0][j];
                        centralityStepsSummed[i] += centralitySteps[0][j];
                        choiceSummed[i] += choice[0][j];
                        connectivitySummed[i] += connectivity[j];
                    }
                }
            }

            // set output
            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["centralitymetric"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="centralitymetric", Values = new List<double>(centralityMetricSummed) };
            resultObjects["centralitysteps"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="centralitysteps", Values = new List<double>(centralityStepsSummed) };
            resultObjects["choice"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="choice", Values = new List<double>(choiceSummed) };
            resultObjects["connectivity"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="connectivity", Values = new List<double>(connectivitySummed) };
            DA.SetData("Analysis", resultObjects);

            /*
            CPlan.Evaluation.GraphAnalysis graphAnalysis = new CPlan.Evaluation.GraphAnalysis(connections);
            graphAnalysis.Calculate("angle_original");
            ShortestPath results = graphAnalysis.ResultsAngular;

            // get metrics
            float[] connectivity = results.Connectivity.ToArray();
            float[] closeness = results.Closeness.ToArray();
            float[] choice = results.Choice2.ToArray();

            // set output
            resultObjects["faces"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="faces", Values = faces };
            resultObjects["connectivity"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="connectivity", Values = new List<float>(connectivity) };
            resultObjects["closeness"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="closeness", Values = new List<float>(closeness) };
            resultObjects["choice"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="choice", Values = new List<float>(choice) };
            */
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.connectivityanalysis;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{f10bcc59-cbf7-44d9-9a2e-996fa818e435}"); }
        }
        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.secondary);
            }
        }
    }
}
