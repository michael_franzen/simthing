﻿using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace WeimarGraphComponent
{
    public class WeimarGraphComponentInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "WeimarGraph";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.connectivityanalysis;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("e455c260-d0ee-4a73-ac7d-85c5a6cdfbdf");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
