using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Safety
{
    public class SafetyInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Safety";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.safetyresult;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2c76b8b0-0e1c-11e6-b2e0-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}