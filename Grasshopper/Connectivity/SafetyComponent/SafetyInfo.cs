﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace SafetyComponent
{
    public class SafetyInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "SafetyComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.safetyanalysis;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("ee90c507-a0ae-4015-bd17-f3427f4baa44");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
