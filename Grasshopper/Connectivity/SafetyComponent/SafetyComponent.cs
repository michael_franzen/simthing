﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;
using CPlan.Geometry;

namespace SafetyComponent
{
    public class SafetyComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public SafetyComponent()
          : base("SafetyComponent", "Safety",
              "Dedicated to analyze the apartment regarding safety measurements such as exit paths.",
              "Carve", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "Granularity", "The maximum edge size (>= 0.5) of the constructed grid.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {

            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //pManager.AddGeometryParameter("faces", "Surfaces", "The grid faces of the constructed grid.", GH_ParamAccess.list);
            //("exitdistances", "Exit Distance", "The distance to an apartment exit.")
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;

            double granularity = 1.0;
            DA.GetData<double>("granularity", ref granularity);
            if (granularity < 0.5) granularity = 0.5;

            List<Brep> faces = new List<Brep>();
            List<Tuple<int, int>> connections = new List<Tuple<int, int>>();
            List<Point3d> vertices = new List<Point3d>();
            foreach (Room room in apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(footprint);

                if (breps == null) continue;
                foreach (Brep brep in breps)
                {
                    foreach (Mesh mesh in Rhino.Geometry.Mesh.CreateFromBrep(brep, new MeshingParameters() { MaximumEdgeLength = granularity }))
                    {
                        int startCount = vertices.Count;
                        for (int i = 0; i < mesh.Faces.Count; i++)
                        {
                            // add vertex
                            Point3d center = mesh.Faces.GetFaceCenter(i);
                            vertices.Add(center);

                            // add corresponding face
                            Point3f a, b, c, d;
                            mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);
                            faces.Add(Brep.CreateFromCornerPoints(a, b, c, d, 0.001));

                            // get adjacent faces and add connections
                            foreach (int x in mesh.Faces.AdjacentFaces(i))
                                connections.Add(new Tuple<int, int>(startCount + i, startCount + x));
                        }
                    }
                }
            }

            List<int> exitVertices = new List<int>();
            // add connections to doors
            foreach (Door door in apartment.Doors)
            {
                Point3d doorCenter = door.Axis().PointAt(0.5);
                vertices.Add(doorCenter);
                if (apartment.GetRooms(door).Count < 2)
                    exitVertices.Add(vertices.Count - 1);

                faces.AddRange(Brep.CreatePlanarBreps(door.Footprint()));
                for (int i = 0; i < vertices.Count; i++)
                {
                    Point3d center = vertices[i];
                    if (doorCenter.DistanceTo(center) < granularity)
                    {
                        connections.Add(new Tuple<int, int>(i, vertices.Count - 1));
                        connections.Add(new Tuple<int, int>(vertices.Count - 1, i));
                    }
                }
            }

            // generate fwnet graph
            FW_GPU_NET.Graph graph = new FW_GPU_NET.Graph();
            List<FW_GPU_NET.Node> nodes = new List<FW_GPU_NET.Node>();
            foreach (Point3d vertex in vertices)
                nodes.Add(graph.AddNode());

            foreach (Tuple<int, int> edge in connections)
                graph.AddDirectedEdge(nodes[edge.Item1], nodes[edge.Item2], 1);

            graph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_DIJKSTRA_BASIC);

            List<double> distancesToExit = new List<double>();
            for (int i = 0; i < vertices.Count; i++) {
                double minDist = double.MaxValue;
                for (int j = 0; j < exitVertices.Count; j++) {
                    try
                    {
                        float distToExit = graph.GetPathLength(nodes[i], nodes[exitVertices[j]]);
                        if (distToExit < minDist) minDist = distToExit;
                    }
                    catch (NotImplementedException) { }
                }
                if (minDist >= float.MaxValue) minDist = -1;
                distancesToExit.Add(minDist);
            }

            graph.Dispose();

            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["exitdistances"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="exitdistances", Values = distancesToExit };
            DA.SetData("Analysis", resultObjects);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.safetyanalysis;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{f16e7e5d-b2dc-4b08-9559-5537d184b153}"); }
        }
        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.quarternary);
            }
        }
    }
}
