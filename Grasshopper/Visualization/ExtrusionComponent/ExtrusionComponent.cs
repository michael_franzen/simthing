﻿using Grasshopper.Kernel;
using LibSimthing;
using Rhino.Geometry;
using System;
using System.Collections.Generic;

namespace Extrusion
{
    public class ExtrusionComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ExtrusionComponent()
          : base("Extrusion", "Extrusion",
              "Extrudes walls, windows and doors using the simthing library.",
              "Carve", "  Core")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGeometryParameter("walls", "Walls", "The walls as surfaces.", GH_ParamAccess.item);
            pManager.AddGeometryParameter("holes", "Holes", "The holes as surfaces.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Apartment apartment = new Apartment();

            // load JSON
            if (!DA.GetData<Apartment>("Carve", ref apartment))
            {
                return;
            }

            List<Surface> wallSurfaces = new List<Surface>();
            List<Surface> holeSurfaces = new List<Surface>();

            foreach (Wall wall in apartment.Walls) wallSurfaces.Add(wall.Surface());
            foreach (Door door in apartment.Doors) holeSurfaces.Add(door.Surface());
            foreach (Window window in apartment.Windows) holeSurfaces.Add(window.Surface());

            DA.SetDataList("walls", wallSurfaces);
            DA.SetDataList("holes", holeSurfaces);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Extrusion.Properties.Resources.buildGeom;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{ec7523b0-301f-41ee-9031-422138a118cf}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.secondary);
            }
        }
    }
}
