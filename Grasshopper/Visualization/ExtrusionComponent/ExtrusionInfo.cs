﻿using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace Extrusion
{
    public class ExtrusionInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Extrusion";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Extrusion.Properties.Resources.buildGeom;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("c67eac4b-be42-48ac-8c6d-91584c23827d");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
