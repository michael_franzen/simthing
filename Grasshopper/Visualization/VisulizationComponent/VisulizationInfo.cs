﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace VisulizationComponent
{
    public class VisulizationInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Visulization";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.visualize;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("f7badcc4-cb2e-439c-8a34-b6c569163cd0");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
