﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;

namespace VisulizationComponent
{
    public class VisulizationComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public VisulizationComponent()
          : base("VisulizationComponent", "Visualization",
              "Computes the feature's values for usage with a gradient component.",
              "Carve", "  Core")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Result", "Result", "The result of a simthing analysis.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGeometryParameter("Geometry", "Geometry", "The geometry associated with each feature value.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Values", "Values", "The feature values.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Min", "Min", "The feature values' minimum.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Max", "Max", "The feature values' maximum.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Mean", "Mean", "The feature values' mean.", GH_ParamAccess.item);
            pManager.AddNumberParameter("StandardDeviation", "StdDev", "The feature values' standard deviation.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Median", "Median", "The feature values' median.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Q25", "Q25", "The feature values' 25% percentile.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Q75", "Q75", "The feature values' 75% percentile.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Lower Fence", "Lower Fence", "The feature values' lower fence (Q25 - 1.5*IQR).", GH_ParamAccess.item);
            pManager.AddNumberParameter("Upper Fence", "Upper Fence", "The feature values' upper fence (Q75 + 1.5*IQR).", GH_ParamAccess.item);
        }

        protected double percentile(List<double> sortedList, double percentile)
        {
            double index = sortedList.Count * percentile;
            int ceiledIndex = Convert.ToInt32(Math.Ceiling(index));
            int flooredIndex = Convert.ToInt32(Math.Floor(index));

            if (ceiledIndex == flooredIndex)
                return sortedList[flooredIndex];
            else
            {
                double result = sortedList[flooredIndex];
                result += sortedList[ceiledIndex];
                return result / 2.0;
            }

        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            SimthingResultObject feature = new SimthingResultObject();
            if (!DA.GetData<SimthingResultObject>("Result", ref feature)) return;

            DA.SetDataList("Geometry", feature.Geometry);
            DA.SetDataList("Values", feature.Values);

            double min = double.MaxValue;
            double max = double.MinValue;
            double mean = 0;
            double std = 0;
            double median = 0;
            double q25 = 0;
            double q75 = 0;
            double IQR = 0;
            double lowerFence = 0;
            double upperFence = 0;

            foreach (double value in feature.Values)
            {
                if (value < min) min = value;
                if (value > max) max = value;
                mean += value;
            }
            mean /= feature.Values.Count;

            foreach (double value in feature.Values)
            {
                std += Math.Pow(value - mean, 2);
            }
            std = Math.Sqrt(std / feature.Values.Count);

            List<double> featureValuesSorted = new List<double>(feature.Values);
            featureValuesSorted.Sort();

            median = percentile(featureValuesSorted, 0.5);
            q25 = percentile(featureValuesSorted, 0.25);
            q75 = percentile(featureValuesSorted, 0.75);
            IQR = q75 - q25;
            lowerFence = Math.Max(min, q75 - IQR * 1.5);
            upperFence = Math.Min(max, q25 + IQR * 1.5);
            
            DA.SetData("Min", min);
            DA.SetData("Max", max);
            DA.SetData("Mean", mean);
            DA.SetData("StandardDeviation", std);
            DA.SetData("Median", median);
            DA.SetData("Q25", q25);
            DA.SetData("Q75", q75);
            DA.SetData("Lower Fence", lowerFence);
            DA.SetData("Upper Fence", upperFence);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.visualize;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{e9a6ed8e-269b-4afa-97a6-a7153bd8a556}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.secondary);
            }
        }
    }
}
