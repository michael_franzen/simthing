﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;

namespace RoomFunctionsComponent
{
    public class FurnitureComponent : GH_Component
    {
        private Apartment apartment;
        private Brep[,] faces;
        private Dictionary<string, double[,]> values;
        private Dictionary<string, double[,]> convolvedValues;
        private double granularity = 0.2;
        private double kernelsize = 13.0;

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public FurnitureComponent()
          : base("Furniture", "Furniture",
              "Returns a map of each furniture category",
              "Carve", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "granularity", "The maximum edge size (>= 0.2) of the constructed grid.", GH_ParamAccess.item);
            pManager.AddNumberParameter("kernelsize", "kernel size", "The kernel size (metric).", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //pManager.AddGeometryParameter("faces", "Surfaces", "The grid faces of the constructed grid.", GH_ParamAccess.list);
            foreach (string furnitureCategory in Enum.GetNames(typeof(Interior.InteriorCategory)))
            {
                pManager.AddGenericParameter(furnitureCategory, furnitureCategory, String.Format("The analysis result for furniture category {0}.", furnitureCategory), GH_ParamAccess.item);
            }
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Apartment currentApartment = new Apartment();
            double currentGranularity = 0.0;
            double currentKernelSize = 0.0;

            if (!DA.GetData<Apartment>("Carve", ref currentApartment)) return;
            DA.GetData<double>("granularity", ref currentGranularity);
            DA.GetData<double>("kernelsize", ref currentKernelSize);

            if (this.apartment != currentApartment || this.granularity != currentGranularity || this.kernelsize != currentKernelSize)
            {
                this.apartment = currentApartment;
                this.granularity = currentGranularity;
                this.kernelsize = currentKernelSize;
                this.computeFurnitureMaps();

                // set faces
                List<Brep> faceList = new List<Brep>();
                foreach (Brep b in this.faces)
                    faceList.Add(b);
                Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();

                // set values
                foreach (string furnitureCategory in Enum.GetNames(typeof(Interior.InteriorCategory)))
                {
                    List<double> valueList = new List<double>();
                    foreach (double d in this.convolvedValues[furnitureCategory])
                        valueList.Add(d);
                    resultObjects[furnitureCategory] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faceList), Name = furnitureCategory, Values = valueList };
                    DA.SetData(furnitureCategory, resultObjects[furnitureCategory]);
                }
            }
        }

        private void computeFurnitureMaps()
        {
            if (this.granularity < 0.2) this.granularity = 0.2;
            if (this.kernelsize <= 0) this.kernelsize = 1;

            // get analaysis points
            List<Point3d> points = new List<Point3d>();
            foreach (Room room in apartment.Rooms)
                points.AddRange(room.CyclicVertices);

            BoundingBox bbox1 = new BoundingBox(points);
            Point3d[] corners = bbox1.GetCorners();
            int numX = (int)Math.Ceiling(((corners[2] - corners[0]).X) / granularity);
            int numY = (int)Math.Ceiling(((corners[2] - corners[0]).Y) / granularity);

            // construct grid
            this.faces = new Brep[numX, numY];
            for (int i = 0; i < numX; i++)
            {
                for (int j = 0; j < numY; j++)
                {
                    Point3d center = new Point3d(corners[0].X + i * granularity + granularity / 2, corners[0].Y + j * granularity + granularity / 2, 0);
                    Point3d[] faceCorners = new Point3d[]
                    {
                            new Point3d(corners[0].X + (i+1)* granularity, corners[0].Y + j * granularity, 0),
                            new Point3d(corners[0].X + (i+1) * granularity, corners[0].Y + (j+1) * granularity, 0),
                            new Point3d(corners[0].X + i * granularity, corners[0].Y + (j+1) * granularity, 0),
                            new Point3d(corners[0].X + i * granularity, corners[0].Y + j * granularity, 0),
                            new Point3d(corners[0].X + (i+1) * granularity, corners[0].Y + j * granularity, 0)
                    };
                    faces[i, j] = Brep.CreateFromCornerPoints(faceCorners[0], faceCorners[1], faceCorners[2], faceCorners[3], 0.001);
                }
            }

            // initialize array for each furniture category
            this.values = new Dictionary<string, double[,]>();
            foreach (string furnitureCategory in Enum.GetNames(typeof(Interior.InteriorCategory)))
                values[furnitureCategory] = new double[numX, numY];

            // compute values for each furniture category
            foreach (Interior interior in this.apartment.Interiors)
            {
                Point3d pos = interior.Position;
                string category = Enum.GetName(typeof(Interior.InteriorCategory), interior.Category);

                // get position in grid
                int posX = (int)Math.Floor((interior.Position.X - corners[0].X) / granularity);
                int posY = (int)Math.Floor((interior.Position.Y - corners[0].Y) / granularity);
                if (posX < 0 || posX >= numX || posY < 0 || posY >= numY) continue;

                // set value in grid
                values[category][posX, posY] += 1;
            }

            // create kernel
            int kernelSize = (int)(this.kernelsize/this.granularity);
            double[,] kernel = new double[kernelSize, kernelSize];

            // fill kernel
            double sigma = kernelSize / 6.0;
            int kernelRadius = kernelSize / 2;
            double factor = 1.0 / (2.0 * Math.PI * Math.Pow(sigma, 2));
            double total = 0.0;
            for (int kx = -kernelRadius; kx < kernelRadius; kx++)
            {
                for (int ky = -kernelRadius; ky < kernelRadius; ky++)
                {
                    kernel[kx + kernelRadius, ky + kernelRadius] = factor * Math.Exp(-(Math.Pow(kx, 2) + Math.Pow(ky, 2)) / (2 * Math.Pow(sigma, 2)));
                    total += kernel[kx + kernelRadius, ky + kernelRadius];
                }
            }

            // convolve
            this.convolvedValues = new Dictionary<string, double[,]>();
            foreach (string furnitureCategory in Enum.GetNames(typeof(Interior.InteriorCategory)))
            {
                convolvedValues[furnitureCategory] = new double[numX, numY];
                for (int x = 0; x < numX; x++)
                {
                    for (int y = 0; y < numY; y++)
                    {
                        convolvedValues[furnitureCategory][x, y] = 0.0;
                        for (int kx = -kernelRadius; kx < kernelRadius; kx++)
                        {
                            for (int ky = -kernelRadius; ky < kernelRadius; ky++)
                            {
                                int xp = x + kx;
                                int yp = y + ky;

                                if (xp < 0 || xp >= numX || yp < 0 || yp >= numY) continue;
                                convolvedValues[furnitureCategory][x, y] += kernel[kx + kernelRadius, ky + kernelRadius] * this.values[furnitureCategory][xp, yp]; 
                            }
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.furniture;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{065038ae-df26-4cdc-8573-3fc11bef2d78}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.quinary);
            }
        }
    }
}
