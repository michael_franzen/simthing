﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace RoomFunctionsComponent
{
    public class FurnitureInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "RoomFunctions";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.furniture;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("8d73e135-2884-4670-a904-2cff521b3c6d");
            }
        }
        
        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
