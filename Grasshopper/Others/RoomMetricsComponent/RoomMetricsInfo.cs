﻿using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace RoomMetrics
{
    public class RoomMetricsInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "RoomMetrics";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return RoomMetrics.Properties.Resources.roommetrics;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("d42d9dfa-92d9-408c-819c-d05eed5c7c70");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
