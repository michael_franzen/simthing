﻿using Grasshopper.Kernel;
using LibSimthing;
using Rhino.Geometry;
using System;
using System.Collections.Generic;

namespace RoomMetrics
{
    public class RoomMetricsComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public RoomMetricsComponent()
          : base("RoomMetrics", "RoomMetrics",
              "Returns some basic metrics for each room of the apartment.",
              "Carve", "ETH")
        {
        }


        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //pManager.AddGeometryParameter("faces", "Surfaces", "The surface area of each floor.", GH_ParamAccess.item);
            //("floorareas", "Floor Area", "The floor areas of each room.")
            //("windowareas", "Window Area", "The window areas of each room.")
            //("neighboringrooms", "Number of Neighbors", "The number of neighboring (i.e. accessible) rooms for each room.")
            //("perimeter", "Perimeter", "The perimeter of each room.")
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //string serializedRooms = "";
            //
            //// load JSON
            //if (!DA.GetData<string>("rooms", ref serializedRooms)) return;
            //
            //// deserialize apartment model
            //List<Room> rooms = JsonConvert.DeserializeObject<List<Room>>(serializedRooms, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;

            HashSet<Room> rooms = apartment.Rooms;
            List<Brep> faces = new List<Brep>();
            List<double> floorAreas = new List<double>();
            List<double> windowAreas = new List<double>();
            List<double> perimeters = new List<double>();
            List<double> neighbors = new List<double>();
            foreach (Room room in rooms)
            {
                double windowArea = 0;
                double floorArea = 0;
                double perimeter = 0;
                double neighbor = 0;

                foreach (Window window in room.Windows) windowArea += window.Height * window.Length;
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(room.Footprint());
                faces.AddRange(breps);
                if (breps != null) foreach (Brep brep in breps) floorArea += brep.GetArea();
                else continue;


                HashSet<Room> visitedRooms = new HashSet<Room>();
                foreach (Door doors in room.Doors)
                {
                    HashSet<Room> doorRooms = apartment.GetRooms(doors);

                    // dont add apartment entry
                    if (doorRooms.Count < 2) continue;
                    
                    // add every room only once
                    bool addDoor = true;
                    foreach (Room r in doorRooms)
                    {
                        if (visitedRooms.Contains(r) && r != room)
                        {
                            addDoor = false;
                            break;
                        }
                    }
                    if (addDoor) neighbor += 1.0;
                }
                perimeter = room.Footprint().GetLength();

                floorAreas.Add(floorArea);
                windowAreas.Add(windowArea);
                perimeters.Add(perimeter);
                neighbors.Add(neighbor);
            }

            Dictionary<string, SimthingResultObject> resultObjects = new Dictionary<string, SimthingResultObject>();
            resultObjects["floorareas"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="floorareas", Values = floorAreas };
            resultObjects["windowareas"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="windowareas", Values = windowAreas };
            resultObjects["neighboringrooms"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="neighboringrooms", Values = neighbors };
            resultObjects["perimeter"] = new SimthingResultObject() { Geometry = new List<GeometryBase>(faces), Name="perimeter", Values = perimeters };
            DA.SetData("Analysis", resultObjects);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return RoomMetrics.Properties.Resources.roommetrics;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{7e6fb741-dab4-4380-bbba-93c398eeb87e}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.quinary);
            }
        }
    }
}
