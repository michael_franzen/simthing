using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Rooms
{
    public class RoomsInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Rooms";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.ic_near_me_black_24dp_1x;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2ca2f8cf-0e1c-11e6-86b8-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}