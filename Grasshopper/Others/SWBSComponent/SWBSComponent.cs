﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;
using LibSWBS;

namespace SWBSComponent
{
    public class SWBSComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public SWBSComponent()
          : base("SWBS", "SWBS",
              "Schweizer Wohnungsbewertungssystem",
              "Simthing", "ETH")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("simthing", "Simthing", "The simthing data structure.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Analysis", "Analysis", "The analysis results.", GH_ParamAccess.item);
            //for (int i = 1; i <= 16; i++) {
            //    string feature = String.Format("B{0}", i);
            //    pManager.AddNumberParameter(feature, feature, feature, GH_ParamAccess.item);
            //}
            //pManager.AddNumberParameter("total", "total", "total", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("simthing", ref apartment)) return;

            SWBS swbs = new SWBS(apartment);

            DA.SetData("B1", swbs.B1());
            DA.SetData("B2", swbs.B2());
            DA.SetData("B3", swbs.B3());
            DA.SetData("B4", swbs.B4());
            DA.SetData("B5", swbs.B5());
            DA.SetData("B6", swbs.B6());
            DA.SetData("B7", swbs.B7());
            DA.SetData("B8", swbs.B8());
            DA.SetData("B9", swbs.B9());
            DA.SetData("B10", swbs.B10());
            DA.SetData("B11", swbs.B11());
            DA.SetData("B12", swbs.B12());
            DA.SetData("B13", swbs.B13());
            DA.SetData("B14", swbs.B14());
            DA.SetData("B15", swbs.B15());
            DA.SetData("B16", swbs.B16());
            DA.SetData("total", swbs.TotalValue());
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.ic_list_black_24dp_1x;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{880d9cc4-c1c0-4b2b-b25e-288a1d677ed7}"); }
        }
    }
}
