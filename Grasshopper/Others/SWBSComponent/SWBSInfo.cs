﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace SWBSComponent
{
    public class SWBSInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "SWBSComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.ic_list_black_24dp_1x;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("5030cdba-6f39-4c4e-af9f-49b9b6171cd5");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
