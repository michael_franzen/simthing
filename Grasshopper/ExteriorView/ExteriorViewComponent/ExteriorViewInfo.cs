﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace ExteriorView
{
    public class ExteriorViewInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "ExteriorViewComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.extieriorview;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("90076c0b-7968-4976-9887-1acbe514edd2");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
