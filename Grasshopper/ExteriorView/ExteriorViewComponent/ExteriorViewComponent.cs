﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using OpenTK.Input;
using LibExterior3D;
using System.Diagnostics;
using Grasshopper.Kernel.Parameters;

namespace ExteriorView
{
    public class ExteriorViewComponent : GH_Component, IGH_VariableParameterComponent
    {
        private int numStaticInputParameters = 3;
        private int numStaticOutputParameters = 3;

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ExteriorViewComponent()
          : base("ExteriorView", "ExteriorView",
              "Calculates some properties of the exterior view.",
              "Carve", " Technion")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
            pManager.AddNumberParameter("granularity", "Granularity", "The granularity of the analysis grid.", GH_ParamAccess.item);
            pManager.AddNumberParameter("orientation", "Orientation", "The orientation in degrees as a value between 0 and 360.", GH_ParamAccess.item);
            pManager.AddParameter(this.CreateParameter(GH_ParameterSide.Input, numStaticInputParameters));

            // for dynamic input
            this.Params.ParameterSourcesChanged += Params_ParameterSourcesChanged;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Test", "Test", "test", GH_ParamAccess.item);
            pManager.AddGenericParameter("Test2", "Test", "test", GH_ParamAccess.item);
            pManager.AddGenericParameter("Test3", "Test", "test", GH_ParamAccess.item);
            pManager.AddParameter(this.CreateParameter(GH_ParameterSide.Output, numStaticOutputParameters));
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {

            // Debug only
            List<PolylineCurve> polyLines = new List<PolylineCurve>();

            // Input
            Apartment apartment = new Apartment();
            if (!DA.GetData<Apartment>("Carve", ref apartment)) return;


            double granularity = 0.5;
            DA.GetData<double>("granularity", ref granularity);

            double orientation = 0;
            DA.GetData<double>("orientation", ref orientation);
            orientation %= 360;


            // get apartment
            List<List<List<Point3d>>> polygons = getApartmentPolygons(apartment, orientation);
            List<List<Point3d>> apartmentTriangles = triangulate(polygons);
            
            // convert geometries to OpenTK
            List<List<OpenTK.Vector3d>> openTKGeometry = new List<List<OpenTK.Vector3d>>();
            List<List<int>> categories = new List<List<int>>();
            toOpenTK(apartmentTriangles, 1, ref openTKGeometry, ref categories);

            // custom categories
            int numCategories = this.Params.Input.Count - numStaticInputParameters - 1;
            for (int i = 1; i <= numCategories; i++)
            {
                List<Mesh> catn = new List<Mesh>();
                DA.GetDataList<Mesh>(String.Format("catIn{0}", i), catn);
                foreach (Mesh mesh in catn)
                {
                    List<List<Point3d>> categoryTriangles = getMeshTriangles(mesh);
                    toOpenTK(categoryTriangles, i + 1, ref openTKGeometry, ref categories);
                }
            }

            // get grid points
            Transform rotation = Transform.Rotation(Math.PI * orientation / 180.0, Point3d.Origin);
            List<Point3d> gridPoints = new List<Point3d>();
            List<Brep> faces = new List<Brep>();
            foreach (Room room in apartment.Rooms)
            {
                PolylineCurve footprint = room.Footprint();
                Brep[] breps = Rhino.Geometry.Brep.CreatePlanarBreps(footprint);

                if (breps == null) continue;
                foreach (Brep brep in breps)
                {
                    foreach (Mesh mesh in Rhino.Geometry.Mesh.CreateFromBrep(brep, new MeshingParameters() { MaximumEdgeLength = granularity }))
                    {
                        for (int i = 0; i < mesh.Faces.Count; i++)
                        {
                            Point3f a, b, c, d;
                            Point3d center = mesh.Faces.GetFaceCenter(i);
                            mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);

                            List<Point3d> pp = new List<Point3d>() { a, b, c, d, center };
                            pp = new List<Point3d>(rotation.TransformList(pp));
                            a = new Point3f((float)pp[0].X, (float)pp[0].Y, (float)pp[0].Z);
                            b = new Point3f((float)pp[1].X, (float)pp[1].Y, (float)pp[1].Z);
                            c = new Point3f((float)pp[2].X, (float)pp[2].Y, (float)pp[2].Z);
                            d = new Point3f((float)pp[3].X, (float)pp[3].Y, (float)pp[3].Z);
                            center = pp[4];
                            
                            gridPoints.Add(center);
                            faces.Add(Brep.CreateFromCornerPoints(a, b, c, d, 0.001));
                        }
                    }
                }
            }

            List<List<OpenTK.Vector3d>> viewports = new List<List<OpenTK.Vector3d>>();
            foreach (Window window in apartment.Windows)
            {
                Vector3d windowHeightVector = new Vector3d(0, 0, window.Height);
                Line line = window.Lines2D()[0];
                List<Point3d> windowPolygon = new List<Point3d>()
                            {
                                line.From,
                                line.To,
                                line.To + windowHeightVector,
                                line.From + windowHeightVector,
                                line.From
                            };

                List<OpenTK.Vector3d> viewport = new List<OpenTK.Vector3d>();
                foreach (Point3d point in windowPolygon)
                    viewport.Add(new OpenTK.Vector3d(point.X, point.Y, point.Z));

                viewports.Add(viewport);
            }

            List<OpenTK.Vector3d> grid = new List<OpenTK.Vector3d>();
            foreach (Point3d analysisPoint in gridPoints)
                grid.Add(new OpenTK.Vector3d(analysisPoint.X, analysisPoint.Y, 1.6));

            // initialize renderer
            LibExterior3D.ExteriorView analysis = new LibExterior3D.ExteriorView(openTKGeometry, categories);


            SimthingResultObject[] results = new SimthingResultObject[numCategories + 1];
            for (int i = 0; i <= numCategories; i++)
                results[i] = new SimthingResultObject() {
                    Geometry = new List<GeometryBase>(faces),
                    Name = String.Format("Category {0}", i),
                    Values = new List<double>()
                };

            // render all grid points and all windows
            List<double> data = new List<double>();
            foreach (OpenTK.Vector3d from in grid)
            {
                double[] tmpResult = new double[numCategories + 1];
                foreach (List<OpenTK.Vector3d> viewport in viewports)
                {
                    double f1 = OpenTK.Vector3d.CalculateAngle(viewport[0] - from, viewport[1] - from);
                    double f2 = OpenTK.Vector3d.CalculateAngle(viewport[0] - from, viewport[3] - from);

                    // render and sample
                    analysis.glRender(from, viewport);
                    int[] sampled = analysis.clSample(numCategories + 2);

                    // store results
                    for (int i = 1; i <= numCategories; i++)
                        tmpResult[i] += sampled[i+1] * f1 * f2;
                }

                // add to result objects
                for (int i = 0; i <= numCategories; i++)
                    results[i].Values.Add(tmpResult[i]);
            }
            

            for (int i = 1; i <= numCategories; i++)
                DA.SetData(String.Format("catOut{0}", i), results[i]);
        }

        #region Preprocessing

        private void toOpenTK(
            List<List<Point3d>> triangles,
            int category,
            ref List<List<OpenTK.Vector3d>> openTKGeometry,
            ref List<List<int>> categories)
        {
            foreach (List<Point3d> triangle in triangles)
            {
                List<OpenTK.Vector3d> vec = new List<OpenTK.Vector3d>();
                List<int> cat = new List<int>();
                foreach (Point3d point in triangle)
                {
                    vec.Add(new OpenTK.Vector3d(point.X, point.Y, point.Z));
                    cat.Add(category);
                }
                openTKGeometry.Add(vec);
                categories.Add(cat);
            }
        }

        private List<List<Point3d>> getMeshTriangles(Mesh mesh)
        {
            List<List<Point3d>> triangles = new List<List<Point3d>>();
            mesh.Faces.ConvertQuadsToTriangles();
            for (int i = 0; i < mesh.Faces.Count; i++)
            {

                Point3f a, b, c, d;
                Point3d center = mesh.Faces.GetFaceCenter(i);
                mesh.Faces.GetFaceVertices(i, out a, out b, out c, out d);

                triangles.Add(new List<Point3d>() { a, b, c });
            }
            return triangles;
        }

        private List<List<Point3d>> triangulate(List<List<List<Point3d>>> polygons)
        {
            List<List<Point3d>> triangles = new List<List<Point3d>>();
            foreach (List<List<Point3d>> polySet in polygons)
            {
                Plane basePlane = new Plane(polySet[0][0], polySet[0][1] - polySet[0][0], polySet[0][2] - polySet[0][1]);
                Transform rotation = Transform.PlaneToPlane(basePlane, Plane.WorldXY);
                Transform invrotation = Transform.PlaneToPlane(Plane.WorldXY, basePlane);
                
                HashSet<double> X = new HashSet<double>();
                HashSet<double> Y = new HashSet<double>();

                // Intervals of windows / doors
                HashSet<Tuple<double, double>> HolesX = new HashSet<Tuple<double, double>>();
                HashSet<Tuple<double, double>> HolesY = new HashSet<Tuple<double, double>>();
                for (int i = 0; i < polySet.Count; i++)
                {
                    // rotate on XY-Plane
                    List<Point3d> rotatedPoints = new List<Point3d>(rotation.TransformList(polySet[i]));

                    double minX = double.MaxValue;
                    double minY = double.MaxValue;
                    double maxX = double.MinValue;
                    double maxY = double.MinValue;
                    for (int j = 0; j < rotatedPoints.Count; j++)
                    {
                        minX = Math.Min(minX, rotatedPoints[j].X);
                        minY = Math.Min(minY, rotatedPoints[j].Y);
                        maxX = Math.Max(maxX, rotatedPoints[j].X);
                        maxY = Math.Max(maxY, rotatedPoints[j].Y);
                        X.Add(rotatedPoints[j].X);
                        Y.Add(rotatedPoints[j].Y);
                    }

                    if (i > 0)
                    {
                        HolesX.Add(new Tuple<double, double>(minX, maxX));
                        HolesY.Add(new Tuple<double, double>(minY, maxY));
                    }
                }

                // sort x and y values
                List<double> sortedX = new List<double>(X);
                sortedX.Sort();
                List<double> sortedY = new List<double>(Y);
                sortedY.Sort();

                // create grid
                List<List<Point3d>> quads = new List<List<Point3d>>();
                for (int x = 0; x < sortedX.Count - 1; x++)
                {
                    for (int y = 0; y < sortedY.Count - 1; y++)
                    {
                        // check if quad is within window / door boundaries
                        bool isHoleX = false;
                        foreach (Tuple<double, double> holeX in HolesX)
                        {
                            if (sortedX[x] >= holeX.Item1 && sortedX[x+1] <= holeX.Item2)
                            {
                                isHoleX = true;
                                break;
                            }
                        }

                        bool isHoleY = false;
                        foreach (Tuple<double, double> holeY in HolesY)
                        {
                            if (sortedY[y] >= holeY.Item1 && sortedY[y + 1] <= holeY.Item2)
                            {
                                isHoleY = true;
                                break;
                            }
                        }

                        if (!(isHoleX && isHoleY))
                        {
                            // triangle one
                            List<Point3d> triangle1 = new List<Point3d>()
                            {
                                new Point3d(sortedX[x], sortedY[y], 0),
                                new Point3d(sortedX[x + 1], sortedY[y], 0),
                                new Point3d(sortedX[x + 1], sortedY[y + 1], 0)
                            };

                            // triangle two
                            List<Point3d> triangle2 = new List<Point3d>()
                            {
                                new Point3d(sortedX[x+1], sortedY[y+1], 0),
                                new Point3d(sortedX[x], sortedY[y+1], 0),
                                new Point3d(sortedX[x], sortedY[y], 0)
                            };


                            triangles.Add(new List<Point3d>(invrotation.TransformList(triangle1)));
                            triangles.Add(new List<Point3d>(invrotation.TransformList(triangle2)));
                        }
                    }
                }
            }

            return triangles;
        }

        private List<List<List<Point3d>>> getApartmentPolygons(Apartment apartment, double orientation)
        {
            Transform rotation = Transform.Rotation(Math.PI * orientation / 180.0, Point3d.Origin);

            List<List<List<Point3d>>> polygons = new List<List<List<Point3d>>>();
            // Parse apartment
            foreach (Room room in apartment.Rooms)
            {
                Vector3d wallHeightVector = new Vector3d(0, 0, room.Height);

                // construct floor polygon
                // TODO: Add pillars
                List<Point3d> floor = room.CyclicVertices;
                polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(floor)) });


                // construct ceiling polygon
                List<Point3d> ceiling = new List<Point3d>();
                for (int i = 0; i < floor.Count; i++)
                    ceiling.Add(floor[i] + wallHeightVector);
                polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(ceiling)) });

                // construct wall polygons
                for (int i = 0; i < floor.Count - 1; i++)
                {
                    List<List<Point3d>> wallPolygons = new List<List<Point3d>>();

                    List<Point3d> wallPolygon = new List<Point3d>() {
                        floor[i],
                        floor[i + 1],
                        floor[i + 1] + wallHeightVector,
                        floor[i] + wallHeightVector,
                        floor[i]
                    };
                    wallPolygons.Add(new List<Point3d>(rotation.TransformList(wallPolygon)));

                    // add holes
                    PolylineCurve wallPolyline = new PolylineCurve(wallPolygon);

                    // add windows
                    foreach (Window window in room.Windows)
                    {
                        Vector3d windowHeightVector = new Vector3d(0, 0, window.Height);
                        foreach (Line windowLine in window.Lines2D())
                        {
                            PointContainment pc1 = wallPolyline.Contains(windowLine.From);
                            PointContainment pc2 = wallPolyline.Contains(windowLine.To);

                            if (pc1 != PointContainment.Outside && pc2 != PointContainment.Outside)
                            {
                                List<Point3d> windowPolygon = new List<Point3d>()
                                {
                                    windowLine.From,
                                    windowLine.To,
                                    windowLine.To + windowHeightVector,
                                    windowLine.From + windowHeightVector,
                                    windowLine.From
                                };
                                wallPolygons.Add(new List<Point3d>(rotation.TransformList(windowPolygon)));
                            }
                        }
                    }

                    // add doors
                    foreach (Door door in room.Doors)
                    {
                        // don't add exterior doors
                        if (apartment.GetRooms(door).Count < 2)
                            continue;

                        Vector3d doorHeightVector = new Vector3d(0, 0, door.Height);
                        foreach (Line doorLine in door.Lines2D())
                        {
                            PointContainment pc1 = wallPolyline.Contains(doorLine.From);
                            PointContainment pc2 = wallPolyline.Contains(doorLine.To);

                            if (pc1 != PointContainment.Outside && pc2 != PointContainment.Outside)
                            {
                                List<Point3d> doorPolygon = new List<Point3d>()
                                {
                                    doorLine.From,
                                    doorLine.To,
                                    doorLine.To + doorHeightVector,
                                    doorLine.From + doorHeightVector,
                                    doorLine.From
                                };
                                wallPolygons.Add(new List<Point3d>(rotation.TransformList(doorPolygon)));
                            }
                        }
                    }

                    // add wallpolygons to set of polygons
                    polygons.Add(wallPolygons);
                }


                // add door frames
                foreach (Door door in room.Doors)
                {
                    // don't add exterior doors
                    if (apartment.GetRooms(door).Count < 2)
                        continue;

                    List<Line> doorLines = door.Lines2D();
                    Vector3d doorHeight = new Vector3d(0, 0, door.Height);

                    // footprint
                    List<Point3d> doorFootprint = new List<Point3d>()
                    {
                        doorLines[0].From,
                        doorLines[1].From,
                        doorLines[2].From,
                        doorLines[3].From,
                        doorLines[0].From
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorFootprint)) });

                    // ceiling
                    List<Point3d> doorCeiling = new List<Point3d>()
                    {
                        doorLines[0].From + doorHeight,
                        doorLines[1].From + doorHeight,
                        doorLines[2].From + doorHeight,
                        doorLines[3].From + doorHeight,
                        doorLines[0].From + doorHeight
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorCeiling)) });

                    // side reels
                    List<Point3d> doorLeft = new List<Point3d>()
                    {
                        doorLines[1].From,
                        doorLines[1].From + doorHeight,
                        doorLines[2].From + doorHeight,
                        doorLines[2].From,
                        doorLines[1].From
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorLeft)) });

                    List<Point3d> doorRight = new List<Point3d>()
                    {
                        doorLines[3].From,
                        doorLines[3].From + doorHeight,
                        doorLines[0].From + doorHeight,
                        doorLines[0].From,
                        doorLines[3].From
                    };
                    polygons.Add(new List<List<Point3d>>() { new List<Point3d>(rotation.TransformList(doorRight)) });
                }
            }

            return polygons;
        }

        #endregion

        #region Variable Parameters

        public bool CanInsertParameter(GH_ParameterSide side, int index)
        {
            return side == GH_ParameterSide.Input && index >= Params.Input.Count;
        }

        public bool CanRemoveParameter(GH_ParameterSide side, int index)
        {
            return side == GH_ParameterSide.Input && Params.Input.Count > 1 && index > numStaticInputParameters;
        }

        public IGH_Param CreateParameter(GH_ParameterSide side, int index)
        {
            if (side == GH_ParameterSide.Input)
            {
                Param_Mesh ghNewParameter = new Param_Mesh();
                ghNewParameter.NickName = String.Format("Category {0}", Params.Input.Count - numStaticInputParameters + 1);
                ghNewParameter.Name = String.Format("catIn{0}", Params.Input.Count - numStaticInputParameters + 1);
                ghNewParameter.Access = GH_ParamAccess.list;
                if (index > numStaticInputParameters)
                    ghNewParameter.Optional = true;
                return ghNewParameter;
            }
            else
            {
                Param_GenericObject ghNewParameter = new Param_GenericObject();
                ghNewParameter.NickName = String.Format("Category {0}", Params.Output.Count - numStaticOutputParameters + 1);
                ghNewParameter.Name = String.Format("catOut{0}", Params.Output.Count - numStaticOutputParameters + 1);

                return ghNewParameter;
            }
        }

        private void Params_ParameterSourcesChanged(object sender, GH_ParamServerEventArgs e)
        {
            if (e.ParameterSide == GH_ParameterSide.Input)
            {
                if (e.ParameterIndex == this.Params.Input.Count - 1 && e.Parameter.Sources.Count > 0)
                {
                    this.Params.RegisterInputParam(this.CreateParameter(GH_ParameterSide.Input, Params.Input.Count));
                    this.Params.RegisterOutputParam(this.CreateParameter(GH_ParameterSide.Output, Params.Output.Count));
                }
                else if (e.ParameterIndex < this.Params.Input.Count && e.Parameter.Sources.Count == 0)
                {
                    int offset = this.Params.Input.Count - e.ParameterIndex;
                    this.Params.Input.RemoveAt(e.ParameterIndex);
                    this.Params.Output.RemoveAt(this.Params.Output.Count - offset);
                }

                this.VariableParameterMaintenance();
                this.Params.OnParametersChanged();
            }
        }
        
        public bool DestroyParameter(GH_ParameterSide side, int index)
        {
            return true;
        }

        public void VariableParameterMaintenance()
        {
            for (int i = numStaticInputParameters; i < this.Params.Input.Count; i++)
            {
                this.Params.Input[i].NickName = String.Format("Category {0}", i - numStaticInputParameters + 1);
                this.Params.Input[i].Name = String.Format("catIn{0}", i - numStaticInputParameters + 1);
                this.Params.Input[i].Access = GH_ParamAccess.list;
                if (i > numStaticInputParameters)
                    this.Params.Input[i].Optional = true;
            }

            for (int i = numStaticOutputParameters; i < this.Params.Input.Count; i++)
            {
                this.Params.Output[i].NickName = String.Format("Category {0}", i - numStaticOutputParameters + 1);
                this.Params.Output[i].Name = String.Format("catOut{0}", i - numStaticOutputParameters + 1);
                this.Params.Input[i].Access = GH_ParamAccess.list;
            }
        }

        #endregion

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.extieriorview;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{6a5a1fd3-274f-4e85-ba32-da891ebdde9b}"); }
        }
        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.secondary);
            }
        }
    }
}
