﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using Archilogic.Data;
using LibSimthing;
using Archilogic;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace ArchilogicWeb
{
    public class ArchilogicWebComponent : GH_Component
    {
        private string id;
        private Apartment apartment;

        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ArchilogicWebComponent()
          : base("Web", "Web",
              "Downloads an apartment from the archilogic servers.",
              "Carve", "  Core")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("archilogic", "ID", "The apartment ID.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The serialized simthing data structure.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string tmpId = "";

            // load JSON
            if (!DA.GetData<string>("archilogic", ref tmpId))
            {
                return;
            }

            if (tmpId != this.id)
            {
                this.id = tmpId;
                
                // generate id
                string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                char[] idChars = new char[5];
                Random random = new Random();
                for (int i = 0; i < idChars.Length; i++)
                    idChars[i] = chars[random.Next(chars.Length)];

                // generate http request
                HttpWebRequest client = WebRequest.CreateHttp(@"http://spaces.archilogic.com/api/v2");
                client.Method = "POST";
                client.ContentType = "application/json";
                client.Headers["dataType"] = "json";
                
                Dictionary<string, object> dt = new Dictionary<string, object>
                {
                    {"jsonrpc", "2.0" },
                    {"method", "Model.read" },
                    {"id", String.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMddHHmmssfff"), new String(idChars))},
                    {"params", new Dictionary<string, string>
                        {
                            { "resourceName", this.id.Split('/')[1] },
                            { "organizationResourceName", this.id.Split('/')[0] }
                        }
                    }
                };

                // make http request
                byte[] data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(dt));
                client.ContentLength = data.Length;
                using (Stream stream = client.GetRequestStream())
                    stream.Write(data, 0, data.Length);
                
                // get http response
                WebResponse response = (HttpWebResponse)client.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                JObject deserialized = JObject.Parse(responseString);
                ALApartment apartmentAL = JsonConvert.DeserializeObject<ALApartment>(deserialized["result"]["modelStructure"].ToString());

                // TODO: Support more levels.
                ALLevel levelAL = apartmentAL.Children[0];

                List<Point3d> interior_positions = new List<Point3d>();
                List<string> interior_categories = new List<string>();
                apartment = new Apartment() { Id = levelAL.Id };
                foreach (ALSolidObject solidAL in levelAL.Children)
                {
                    if (solidAL.Type == ALSolidObject.SolidObjectType.Wall)
                    {
                        Wall wall = apartment.AddWall(solidAL.X, solidAL.Y, solidAL.Z, solidAL.L, solidAL.W, solidAL.H, solidAL.Ry);
                        foreach (ALVolatileObject volatileAL in solidAL.Children)
                        {
                            if (volatileAL.Type == ALVolatileObject.VolatileObjectType.Window)
                                apartment.AddWindow(wall, volatileAL.X, volatileAL.Z, volatileAL.L, volatileAL.H);

                            if (volatileAL.Type == ALVolatileObject.VolatileObjectType.Door)
                                apartment.AddDoor(wall, volatileAL.X, volatileAL.Z, volatileAL.L, volatileAL.H);
                        }
                    }
                    if (solidAL.Type == ALSolidObject.SolidObjectType.Interior)
                    {
                        Interior.InteriorCategory category = InteriorCategorizer.Category(solidAL.Interior);
                        apartment.AddInterior(category, solidAL.Interior, solidAL.X, solidAL.Y, solidAL.Z, solidAL.Ry);
                        interior_positions.Add(new Point3d(solidAL.X, solidAL.Y, solidAL.Z));
                        interior_categories.Add(Enum.GetName(typeof(Interior.InteriorCategory), category));
                    }
                }
                apartment.ComputeRooms();
            }
            DA.SetData("Carve", apartment);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.webimport;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{3cf700b1-1bd4-45a0-845e-2174349c6aed}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.primary);
            }
        }
    }
}
