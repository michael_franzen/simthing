﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace ArchilogicWeb
{
    public class ArchilogicWebInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "ArchilogicWeb";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.webimport;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("ce6036de-91d0-4a78-9e57-207faaa3b0a0");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
