﻿using Grasshopper.Kernel;
using System;
using System.Drawing;

namespace Archilogic
{
    public class ArchilogicInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Archilogic";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Archilogic.Properties.Resources.fileimport;
            }
        }
        public override string Description
        {
            get
            {
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("e7824556-4f29-46aa-9174-676650f899a4");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
