﻿using Archilogic.Data;
using Grasshopper.Kernel;
using LibSimthing;
using Newtonsoft.Json;
using Rhino.Geometry;
using System;
using System.Collections.Generic;
using System.IO;

namespace Archilogic
{
    public class ArchilogicComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ArchilogicComponent()
          : base("Archilogic", "Archilogic",
              "Parses an Archilogic file",
              "Carve", "  Core")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("archilogic", "File", "The JSON file containing the ArchiLogic data. One apartment per line.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("line", "Line", "The line containing the apartment to be parsed.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The serialized simthing data structure.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string filepath = "";

            // load JSON
            if (!DA.GetData<string>("archilogic", ref filepath))
            {
                return;
            }

            // get file line of apartment
            int line = 0;
            if (!DA.GetData<int>("line", ref line))
            {
                return;
            }

            string[] lines = File.ReadAllLines(filepath);
            ALApartment apartmentAL = JsonConvert.DeserializeObject<ALApartment>(lines[line]);

            // TODO: Support more levels.
            ALLevel levelAL = apartmentAL.Children[0];
            
            Apartment apartment = new Apartment() { Id = levelAL.Id };
            foreach (ALSolidObject solidAL in levelAL.Children)
            {
                if (solidAL.Type ==  ALSolidObject.SolidObjectType.Wall)
                {
                    Wall wall = apartment.AddWall(solidAL.X, solidAL.Y, solidAL.Z, solidAL.L, solidAL.W, solidAL.H, solidAL.Ry);
                    foreach (ALVolatileObject volatileAL in solidAL.Children)
                    {
                        if (volatileAL.Type == ALVolatileObject.VolatileObjectType.Window)
                            apartment.AddWindow(wall, volatileAL.X, volatileAL.Z, volatileAL.L, volatileAL.H);

                        if (volatileAL.Type == ALVolatileObject.VolatileObjectType.Door)
                            apartment.AddDoor(wall, volatileAL.X, volatileAL.Z, volatileAL.L, volatileAL.H);
                    }
                }
                if (solidAL.Type == ALSolidObject.SolidObjectType.Interior)
                {
                    Interior.InteriorCategory category = InteriorCategorizer.Category(solidAL.Interior);
                    apartment.AddInterior(category, solidAL.Interior, solidAL.X, solidAL.Y, solidAL.Z, solidAL.Ry);
                }
            }
            apartment.ComputeRooms();

            DA.SetData("Carve", apartment);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Archilogic.Properties.Resources.fileimport;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{c730b7ba-2d20-4510-bb19-5b6fe4072a96}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.primary);
            }
        }
    }
}
