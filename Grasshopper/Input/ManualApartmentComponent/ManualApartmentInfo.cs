﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace ManualApartmentComponent
{
    public class ManualApartmentInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "ManualApartmentComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.manual;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("57a814f2-25e7-4af5-9c40-4bfbbc0dee25");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
