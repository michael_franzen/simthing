﻿using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using LibSimthing;
using Rhino.Geometry.Intersect;
using System.Collections.Generic;

namespace ManualApartmentComponent
{
    public class ManualApartmentComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public ManualApartmentComponent()
          : base("Manual", "Manual",
              "Takes the walls, windows and doors as Rhino boxes and transforms them into an apartment.",
              "Carve", "  Core")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBoxParameter("walls", "Walls", "The apartment's walls as boxes.", GH_ParamAccess.list);
            pManager.AddBoxParameter("windows", "Windows", "The apartment's windows as boxes.", GH_ParamAccess.list);
            pManager.AddBoxParameter("doors", "Doors", "The apartment's doors as boxes.", GH_ParamAccess.list);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Carve", "Carve", "The simthing data structure.", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<Box> wallboxes = new List<Box>();
            if (!DA.GetDataList<Box>("walls", wallboxes)) return;

            List<Box> windowboxes = new List<Box>();
            if (!DA.GetDataList<Box>("windows", windowboxes)) return;

            List<Box> doorboxes = new List<Box>();
            if (!DA.GetDataList<Box>("doors", doorboxes)) return;
            
            List<Wall> walls = new List<Wall>();
            Apartment apartment = new Apartment();
            foreach (Box box in wallboxes)
            {
                // get windows / doors of this this wall
                List<Box> wall_windowboxes = new List<Box>();
                List<Box> wall_doorboxes = new List<Box>();
                foreach (Box wbox in windowboxes)
                {
                    Curve[] intersectionCurves;
                    Point3d[] intersectionPoints;
                    Intersection.BrepBrep(wbox.ToBrep(), box.ToBrep(), 0.001, out intersectionCurves, out intersectionPoints);
                    if (intersectionCurves.Length > 0 || intersectionPoints.Length > 0)
                        wall_windowboxes.Add(wbox);
                }

                foreach (Box dbox in doorboxes)
                {
                    Curve[] intersectionCurves;
                    Point3d[] intersectionPoints;
                    Intersection.BrepBrep(dbox.ToBrep(), box.ToBrep(), 0.001, out intersectionCurves, out intersectionPoints);
                    if (intersectionCurves.Length > 0 || intersectionPoints.Length > 0)
                        wall_doorboxes.Add(dbox);
                }


                // get position, dimensions and angle of wall
                List<Point3d> corners = (new List<Point3d>(box.GetCorners()));
                BoundingBox wallBoundingBox = new BoundingBox(corners);
                corners.Sort((x, y) => x.DistanceTo(wallBoundingBox.Min).CompareTo(y.DistanceTo(wallBoundingBox.Min)));
                Point3d wall_min3d = corners[0];
                Point3d wall_max3d = corners[corners.Count - 1];
                Point3d wall_min3dw = corners[corners.Count - 2];

                // get angle in degrees
                double angle = Vector3d.VectorAngle(wall_min3dw - wall_min3d, Vector3d.XAxis, Plane.WorldXY)*180/Math.PI;

                // redo such that each dimension is posiive
                Transform rotation = Transform.Rotation((Math.PI / 180.0) * angle, new Vector3d(0, 0, 1), wall_min3d);
                wall_max3d.Transform(rotation);

                Vector3d dim = (wall_max3d - wall_min3d);

                // add wall
                Wall wall = apartment.AddWall(wall_min3d.X, wall_min3d.Y, wall_min3d.Z, dim.X, dim.Y, dim.Z, angle);

                // adjust origin, dimensions of windows
                foreach (Box wb in wall_windowboxes)
                {
                    corners = (new List<Point3d>(wb.GetCorners()));
                    wallBoundingBox = new BoundingBox(corners);
                    corners.Sort((x, y) => x.DistanceTo(wallBoundingBox.Min).CompareTo(y.DistanceTo(wallBoundingBox.Min)));
                    Point3d window_min3d = corners[0];
                    Point3d window_max3d = corners[corners.Count - 1];
                    window_min3d.Transform(rotation);
                    window_max3d.Transform(rotation);

                    Vector3d window_offset = window_min3d - wall_min3d;
                    Vector3d window_dimensions = window_max3d - window_min3d;

                    apartment.AddWindow(wall, window_offset.X, window_offset.Z, window_dimensions.X, window_dimensions.Z);
                }

                foreach (Box db in wall_doorboxes)
                {
                    corners = (new List<Point3d>(db.GetCorners()));
                    wallBoundingBox = new BoundingBox(corners);
                    corners.Sort((x, y) => x.DistanceTo(wallBoundingBox.Min).CompareTo(y.DistanceTo(wallBoundingBox.Min)));
                    Point3d door_min3d = corners[0];
                    Point3d door_max3d = corners[corners.Count - 1];
                    door_min3d.Transform(rotation);
                    door_max3d.Transform(rotation);

                    Vector3d door_offset = door_min3d - wall_min3d;
                    Vector3d door_dimensions = door_max3d - door_min3d;

                    apartment.AddDoor(wall, door_offset.X, door_offset.Z, door_dimensions.X, door_dimensions.Z);
                }
            }
            apartment.ComputeRooms();
            DA.SetData("Carve", apartment);
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Properties.Resources.manual;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{1fe14bdb-5a63-49f0-8ea3-9f96966d2819}"); }
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return (GH_Exposure.primary);
            }
        }
    }
}
