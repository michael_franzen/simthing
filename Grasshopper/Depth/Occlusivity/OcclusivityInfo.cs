using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Occlusivity
{
    public class OcclusivityInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Occlusivity";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.depthocclusivity;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("{2c96c3cf-0e1c-11e6-87da-c48508b0f4c7}");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}