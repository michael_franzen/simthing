﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace WeimarVisibilityGraphComponent
{
    public class TechnionInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "WeimarVisibilityGraphComponent";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.depthanalysis;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("6fa9416a-b933-4292-a500-737647ab9c63");
            }
        }

        public override string AuthorName
        {
            get
            {
                return "Michael Franzen, ETH Zürich";
            }
        }
        public override string AuthorContact
        {
            get
            {
                return "franzen@arch.ethz.ch";
            }
        }
    }
}
