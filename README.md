# Overview

1. [Features](https://bitbucket.org/michael_franzen/simthing/wiki/Features)
3. [Algorithms](https://bitbucket.org/michael_franzen/simthing/wiki/Algorithms)
4. [Data Sources](https://bitbucket.org/michael_franzen/simthing/wiki/Data%20Sources)
5. [Literature](https://bitbucket.org/michael_franzen/simthing/wiki/Literature)